﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSchedule.Data
{
	public class HardcodeStorage : IStorage
	{
		public List<Team> _teams;
		private List<Worker> _workers;

		public void AddTeam(Team team)
		{
			throw new NotImplementedException();
		}

		public void AddVacation(Vacation vacation, Worker worker)
		{
			throw new NotImplementedException();
		}

		public void AddWorker(Worker worker)
		{
			throw new NotImplementedException();
		}

		public void AddWorkerToTeam(Worker worker, Team team)
		{
			throw new NotImplementedException();
		}

		public void DeleteTeam(Team team)
		{
			throw new NotImplementedException();
		}

		public void DeleteVacation(Vacation vacation)
		{
			throw new NotImplementedException();
		}

		public void DeleteWorker(Worker worker)
		{
			throw new NotImplementedException();
		}

		public void DeleteWorkerFromTeam(Worker worker, Team team)
		{
			throw new NotImplementedException();
		}

		public DateTime GetBaseDate()
		{
			return new DateTime(2019, 6, 30);
		}

		public List<Worker> GetFreeWorkers()
		{
			throw new NotImplementedException();
		}

		public List<Team> GetTeams()
		{
			if (_teams == null)
			{
				_teams = new List<Team>();
				var team = new Team("А");
				var w1 = new Worker("А", "В", "Королев", "");
				w1.Vacations.Add(new Vacation(new DateTime(2019, 7, 30), new DateTime(2019, 8, 1)));
				var w2 = new Worker("О", "А", "Штырева", "");
				w2.Vacations.Add(new Vacation(new DateTime(2019, 7, 2), new DateTime(2019, 7, 29)));
				team.Workers.AddRange(new Worker[] { new Worker("Ю", "Д", "Малахов", ""), new Worker("Е", "Д", "Крысанова", "") });
				_teams.Add(team);
				team = new Team("Б");
				team.Workers.AddRange(new Worker[] { new Worker("А", "Д", "Морсаков", ""), new Worker("И", "В", "Стодушная", "") });
				_teams.Add(team);
				team = new Team("В");
				team.Workers.AddRange(new Worker[] { w1, w2 });
				_teams.Add(team);
				team = new Team("Г");
				team.Workers.AddRange(new Worker[] { new Worker("В", "Г", "Стырков", ""), new Worker("Т", "И", "Алексеева", "") });
				_teams.Add(team);
			}
			return _teams;
		}

		public List<Worker> GetWorkers()
		{
			if (_workers == null)
			{
				var teams = GetTeams();
				_workers = new List<Worker>();
				foreach (var team in teams)
				{
					_workers.AddRange(team.Workers);
				}
			}
			return _workers;
		}

		public Team GetWorkerTeam(Worker worker)
		{
			throw new NotImplementedException();
		}

		public void SetBaseDate(DateTime date)
		{
			throw new NotImplementedException();
		}

		public void UpdateTeam(Team team)
		{
			throw new NotImplementedException();
		}

		public void UpdateVacation(Vacation vacation)
		{
			throw new NotImplementedException();
		}

		public void UpdateWorker(Worker worker)
		{
			throw new NotImplementedException();
		}
	}
}
