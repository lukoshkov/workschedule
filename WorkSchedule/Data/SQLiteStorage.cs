﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace WorkSchedule.Data
{
	public class SQLiteStorage : IStorage
	{
		private string _connectionString;
		private List<Department> _departments;
		private List<Team> _teams;
		private List<Worker> _workers;
		private List<Worker> _freeWorkers;
		private List<Vacation> _holidays;

		public SQLiteStorage(string path)
		{
			var csb = new SQLiteConnectionStringBuilder();
			csb.DataSource = path;
			_connectionString = csb.ConnectionString;

			var departmentsTableSQL =
				"CREATE TABLE IF NOT EXISTS [Departments] ("
				+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ "[name] VARCHAR(20),"
				+ "[scheduler_id] INTEGER,"
				+ "[date] INTEGER)";

			var teamsInDepartmentsSql =
				"CREATE TABLE IF NOT EXISTS [TeamsInDepartments] ("
				+ "[team_id] INTEGER, "
				+ "[department_id] INTEGER, "
				+ "FOREIGN KEY(team_id) REFERENCES Teams(id), "
				+ "FOREIGN KEY(department_id) REFERENCES Departments(id))";

			var holidaysTableSQL =
				"CREATE TABLE IF NOT EXISTS [Holidays] ("
				+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ "[first_day] INTEGER, "
				+ "[last_day] INTEGER)";

			var daysOffTableSQL =
				"CREATE TABLE IF NOT EXISTS [DaysOff] ("
				+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ "[first_day] INTEGER, "
				+ "[last_day] INTEGER, "
				+ "[worker_id] INTEGER, "
				+ "FOREIGN KEY(worker_id) REFERENCES Worker(id))";

			if (!System.IO.File.Exists(path))
			{
				SQLiteConnection.CreateFile(path);
				using (var connection = new SQLiteConnection(_connectionString))
				{
					var createTables = new string[]
					{
						departmentsTableSQL,
						teamsInDepartmentsSql,
						holidaysTableSQL,

						"CREATE TABLE IF NOT EXISTS [Workers] ("
						+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
						+ "[first_name] VARCHAR(20), "
						+ "[middle_name] VARCHAR(20), "
						+ "[last_name] VARCHAR(20), "
						+ "[number] VARCHAR(10), "
						+ "[position] VARCHAR(20))",

						"CREATE TABLE IF NOT EXISTS [Teams] ("
						+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
						+ "[name] VARCHAR(20))",

						"CREATE TABLE IF NOT EXISTS [Vacations] ("
						+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
						+ "[first_day] INTEGER, "
						+ "[last_day] INTEGER, "
						+ "[worker_id] INTEGER, "
						+ "FOREIGN KEY(worker_id) REFERENCES Worker(id))",

						"CREATE TABLE IF NOT EXISTS [WorkersInTeams] ("
						+ "[team_id] INTEGER, "
						+ "[worker_id] INTEGER, "
						+ "FOREIGN KEY(team_id) REFERENCES Team(id), "
						+ "FOREIGN KEY(worker_id) REFERENCES Worker(id))",

						"CREATE TABLE IF NOT EXISTS [BaseDate] ("
						+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
						+ "[date] INTEGER)",

						daysOffTableSQL
					};

					connection.Open();
					foreach (var text in createTables)
					{
						var command = new SQLiteCommand(text, connection);
						command.ExecuteNonQuery();
					}
					connection.Close();
				}
			}
			else
			{
				var baseDate = GetBaseDate();
				using (var connection = new SQLiteConnection(_connectionString))
				{
					var sql = "SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'Departments'";
					connection.Open();
					var command = new SQLiteCommand(sql, connection);
					var reader = command.ExecuteReader();
					//while (reader.Read())
					//{
					//	var name = reader.GetString(0);
					//	var x = 1;
					//}
					if (!reader.HasRows)
					{
						reader.Close();
						var transaction = connection.BeginTransaction();
						try
						{
							command = new SQLiteCommand(departmentsTableSQL, connection, transaction);
							command.ExecuteNonQuery();
							command = new SQLiteCommand(teamsInDepartmentsSql, connection, transaction);
							command.ExecuteNonQuery();
							//command = new SQLiteCommand("ALTER TABLE Teams ADD COLUMN [department_id] INTEGER REFERENCES Departments(id)", connection);
							var department = new Department("БЕЗ НАЗВАНИЯ", 1, baseDate);
							InsertDepartmentCommand(department, connection, transaction);
							var teamIDs = new List<long>();
							sql = "SELECT id FROM Teams;";
							command = new SQLiteCommand(sql, connection, transaction);
							reader = command.ExecuteReader();
							while (reader.Read())
							{
								teamIDs.Add(reader.GetInt64(0));
							}
							foreach (var teamID in teamIDs)
							{
								sql = string.Format("INSERT INTO TeamsInDepartments (team_id, department_id) VALUES({0}, {1});", teamID, department.ID);
								command = new SQLiteCommand(sql, connection, transaction);
								command.ExecuteNonQuery();
							}
							transaction.Commit();
						}
						catch (Exception ex)
						{
							transaction.Rollback();
						}
						CleanUpDatabase();
					}
					else
					{
						reader.Close();
					}
					connection.Close();
				}
			}
			Action<SQLiteConnection, SQLiteTransaction> workerColumns = (connection, transaction) =>
			{
				AddColumnIfNotExists("Workers", "position", "VARCHAR(20)", connection, transaction);
			};
			var workersHavePosition = CommandTransactionWrapper(workerColumns);
			Action<SQLiteConnection, SQLiteTransaction> orderColumns = (connection, transaction) =>
			{
				AddColumnIfNotExists("TeamsInDepartments", "order", "INTEGER", connection, transaction);
				AddColumnIfNotExists("WorkersInTeams", "order", "INTEGER", connection, transaction);
			};
			CommandTransactionWrapper(orderColumns);
			Action<SQLiteConnection, SQLiteTransaction> holidays = (connection, transaction) =>
			{
				var command = new SQLiteCommand(holidaysTableSQL, connection, transaction);
				command.ExecuteNonQuery();
			};
			CommandTransactionWrapper(holidays);
			Action<SQLiteConnection, SQLiteTransaction> daysOff = (connection, transaction) =>
			{
				var command = new SQLiteCommand(daysOffTableSQL, connection, transaction);
				command.ExecuteNonQuery();
			};
			CommandTransactionWrapper(daysOff);
		}

		public void AddDepartment(Department department)
		{
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					InsertDepartmentCommand(department, connection, transaction);
					if (_departments != null)
					{
						_departments.Add(department);
					}
					transaction.Commit();
				}
				catch
				{
					transaction.Rollback();
				}
				connection.Close();
			}
		}

		public void AddHoliday(Vacation holiday)
		{
			var text = string.Format("INSERT INTO Holidays (first_day, last_day) VALUES({0}, {1});",
				ConvertDate(holiday.FirstDay), ConvertDate(holiday.LastDay));
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				holiday.ID = connection.LastInsertRowId;
				connection.Close();
			}
			if (_holidays != null && !_holidays.Contains(holiday))
			{
				_holidays.Add(holiday);
			}
		}

		public void AddTeam(Department department, Team team)
		{
			var text = string.Format("INSERT INTO Teams (name) VALUES('{0}');", team.Name);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					var command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
					team.ID = connection.LastInsertRowId;
					text = string.Format("INSERT INTO TeamsInDepartments (team_id, department_id) VALUES({0}, {1});", team.ID, department.ID);
					command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();
					transaction.Commit();
				}
				catch
				{
					transaction.Rollback();
				}
				connection.Close();
			}
			if (department != null)
			{
				department.Teams.Add(team);
			}
			if (_teams != null)
			{
				_teams.Add(team);
			}
		}

		public void AddVacation(Vacation vacation, Worker worker)
		{
			var text = string.Format("INSERT INTO Vacations (first_day, last_day, worker_id) VALUES({0}, {1}, {2});",
				ConvertDate(vacation.FirstDay), ConvertDate(vacation.LastDay), worker.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				vacation.ID = connection.LastInsertRowId;
				vacation.WorkerID = worker.ID;
				connection.Close();
			}
			if (!worker.Vacations.Contains(vacation))
			{
				worker.Vacations.Add(vacation);
			}
		}

		public void AddDayOff(Vacation dayOff, Worker worker)
		{
			var text = string.Format("INSERT INTO DaysOff (first_day, last_day, worker_id) VALUES({0}, {1}, {2});",
				ConvertDate(dayOff.FirstDay), ConvertDate(dayOff.LastDay), worker.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				dayOff.ID = connection.LastInsertRowId;
				dayOff.WorkerID = worker.ID;
				connection.Close();
			}
			if (!worker.DaysOff.Contains(dayOff))
			{
				worker.DaysOff.Add(dayOff);
			}
		}

		public void AddWorker(Worker worker)
		{
			var text = string.Format("INSERT INTO Workers (first_name, middle_name, last_name, number, position) VALUES('{0}', '{1}', '{2}', '{3}', '{4}');",
				worker.FirstName, worker.MiddleName, worker.LastName, worker.Number, worker.Position);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
				worker.ID = connection.LastInsertRowId;
				connection.Close();
			}
			if (_workers != null)
			{
				_workers.Add(worker);
			}
			if (_freeWorkers != null)
			{
				_freeWorkers.Add(worker);
			}
		}

		public void AddWorkerToTeam(Worker worker, Team team)
		{
			var text = string.Format("INSERT INTO WorkersInTeams (team_id, worker_id) VALUES({0}, {1});", team.ID, worker.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
			if (!team.Workers.Contains(worker))
			{
				team.Workers.Add(worker);
			}
			if (_freeWorkers != null)
			{
				_freeWorkers.Remove(worker);
			}
		}

		public void DeleteDepartment(Department department)
		{
			var text = string.Format("DELETE FROM Departments WHERE id = {0};", department.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					var command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();
					text = string.Format("DELETE FROM Teams WHERE id IN (SELECT team_id FROM TeamsInDepartments WHERE department_id = {0});", department.ID);
					command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();
					text = string.Format("DELETE FROM TeamsInDepartments WHERE department_id = {0};", department.ID);
					command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();
					transaction.Commit();
				}
				catch
				{
					transaction.Rollback();
				}
				connection.Close();
			}
			if (_departments != null)
			{
				_departments.Remove(department);
			}
		}

		public void DeleteHoliday(Vacation holiday)
		{
			var text = string.Format("DELETE FROM Holidays WHERE id = {0};", holiday.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
			if (_holidays != null)
			{
				_holidays.Remove(holiday);
			}
		}

		public void DeleteTeam(Team team)
		{
			var text = string.Format("DELETE FROM Teams WHERE id = {0};", team.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					var command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();

					text = string.Format("DELETE FROM TeamsInDepartments WHERE team_id = {0};", team.ID);
					command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();

					transaction.Commit();
				}
				catch
				{
					transaction.Rollback();
				}
				connection.Close();
			}
			var department = GetTeamDepartment(team);
			if (department != null)
			{
				department.Teams.Remove(team);
			}
			if (_teams != null)
			{
				_teams.Remove(team);
			}
		}

		public void DeleteVacation(Vacation vacation)
		{
			var text = string.Format("DELETE FROM Vacations WHERE id = {0};", vacation.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
			if (_workers != null)
			{
				var worker = _workers.Find(w => w.Vacations.Contains(vacation));
				if (worker != null)
				{
					worker.Vacations.Remove(vacation);
				}
			}
		}

		public void DeleteDayOff(Vacation dayOff)
		{
			var text = string.Format("DELETE FROM DaysOff WHERE id = {0};", dayOff.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
			if (_workers != null)
			{
				var worker = _workers.Find(w => w.DaysOff.Contains(dayOff));
				if (worker != null)
				{
					worker.DaysOff.Remove(dayOff);
				}
			}
		}

		public void DeleteWorker(Worker worker)
		{
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					var text = string.Format("DELETE FROM Vacations WHERE worker_id = {0};", worker.ID);
					var command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();

					text = string.Format("DELETE FROM Workers WHERE id = {0};", worker.ID);
					command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();

					transaction.Commit();
				}
				catch
				{
					transaction.Rollback();
				}
				connection.Close();
			}
			if (_workers != null)
			{
				_workers.Remove(worker);
			}
			if (_freeWorkers != null)
			{
				_freeWorkers.Remove(worker);
			}
			if (_teams != null)
			{
				var team = _teams.Find(t => t.Workers.Contains(worker));
				if (team != null)
				{
					team.Workers.Remove(worker);
				}
			}
		}

		public void DeleteWorkerFromTeam(Worker worker, Team team)
		{
			var text = string.Format("DELETE FROM WorkersInTeams WHERE team_id = {0} AND worker_id = {1};", team.ID, worker.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
			if (team.Workers.Contains(worker))
			{
				team.Workers.Remove(worker);
			}
			if (_freeWorkers != null && !_freeWorkers.Contains(worker))
			{
				if (_teams != null && _teams.Find(t => t.Workers.Contains(worker)) == null)
				{
					_freeWorkers.Add(worker);
				}
			}
		}

		public void SetBaseDate(DateTime date)
		{
			var text = string.Format("INSERT INTO BaseDate (date) VALUES({0});", ConvertDate(date));
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
		}

		public DateTime GetBaseDate()
		{
			var date = new DateTime(2019, 6, 30);
			var text = "SELECT id, date FROM BaseDate;";
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				var reader = command.ExecuteReader();
				while (reader.Read())
				{
					date = ConvertDate(reader.GetInt64(1));
				}
				connection.Close();
			}
			return date;
		}

		public List<Department> GetDepartments()
		{
			if (_departments == null)
			{
				_departments = new List<Department>();
				var text = "SELECT id, name, scheduler_id, date FROM Departments;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var department = new Department(reader.GetString(1), reader.GetInt64(2), ConvertDate(reader.GetInt64(3)));
						department.ID = reader.GetInt64(0);
						_departments.Add(department);
					}
					connection.Close();
				}
				GetTeams();
				text = "SELECT team_id, department_id, [order] FROM TeamsInDepartments ORDER BY 2, 3 DESC, 1;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var teamID = reader.GetInt64(0);
						var departmentID = reader.GetInt64(1);
						Team team = _teams.Find(t => t.ID == teamID);
						Department department = _departments.Find(d => d.ID == departmentID);
						if (team != null && department != null)
						{
							if (reader.IsDBNull(2))
							{
								department.Teams.Add(team);
							}
							else
							{
								department.Teams.Insert(0, team);
							}
						}
					}
					connection.Close();
				}
			}
			return _departments;
		}

		public List<Worker> GetFreeWorkers()
		{
			if (_freeWorkers == null)
			{
				GetTeams();
				//_freeWorkers = _workers.Where(w => _teams.Find(t => t.Workers.Contains(w)) == null).ToList();
			}
			return _freeWorkers;
		}

		public List<Team> GetTeams()
		{
			if (_teams == null)
			{
				_teams = new List<Team>();
				var text = "SELECT * FROM Teams;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var team = new Team(reader.GetString(1));
						team.ID = reader.GetInt64(0);
						_teams.Add(team);
					}
					connection.Close();
				}

				if (_workers == null)
				{
					GetWorkers();
				}
				_freeWorkers = new List<Worker>(_workers);

				text = "SELECT team_id, worker_id, [order] FROM WorkersInTeams ORDER BY 1, 3 DESC, 2;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var teamID = reader.GetInt64(0);
						var workerID = reader.GetInt64(1);
						Team team = _teams.Find(t => t.ID == teamID);
						Worker worker = _workers.Find(w => w.ID == workerID);
						if (team != null && worker != null)
						{
							if (reader.IsDBNull(2))
							{
								team.Workers.Add(worker);
							}
							else
							{
								team.Workers.Insert(0, worker);
							}
							_freeWorkers.Remove(worker);
						}
					}
					connection.Close();
				}
			}
			return _teams;
		}

		public List<Worker> GetWorkers()
		{
			if (_workers == null)
			{
				_workers = new List<Worker>();
				var text = "SELECT * FROM Workers;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var worker = new Worker(reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
						worker.Position = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
						worker.ID = reader.GetInt64(0);
						_workers.Add(worker);
					}
					connection.Close();
				}

				text = "SELECT id, first_day, last_day, worker_id FROM Vacations;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var start = ConvertDate(reader.GetInt64(1));
						var end = ConvertDate(reader.GetInt64(2));
						var vacation = new Vacation(start, end);
						vacation.ID = reader.GetInt64(0);
						vacation.WorkerID = reader.GetInt64(3);
						var worker = _workers.Find(w => w.ID == vacation.WorkerID);
						if (worker != null)
						{
							worker.Vacations.Add(vacation);
						}
					}
					connection.Close();
				}

				text = "SELECT id, first_day, last_day, worker_id FROM DaysOff;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var start = ConvertDate(reader.GetInt64(1));
						var end = ConvertDate(reader.GetInt64(2));
						var dayOff = new Vacation(start, end);
						dayOff.ID = reader.GetInt64(0);
						dayOff.WorkerID = reader.GetInt64(3);
						var worker = _workers.Find(w => w.ID == dayOff.WorkerID);
						if (worker != null)
						{
							worker.DaysOff.Add(dayOff);
						}
					}
					connection.Close();
				}
			}
			return _workers;
		}

		public List<Vacation> GetHolidays()
		{
			if (_holidays == null)
			{
				_holidays = new List<Vacation>();
				var text = "SELECT id, first_day, last_day FROM Holidays;";
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var command = new SQLiteCommand(text, connection);
					var reader = command.ExecuteReader();
					while (reader.Read())
					{
						var start = ConvertDate(reader.GetInt64(1));
						var end = ConvertDate(reader.GetInt64(2));
						var holiday = new Vacation(start, end);
						holiday.ID = reader.GetInt64(0);
						_holidays.Add(holiday);
					}
					connection.Close();
				}
			}
			return _holidays;
		}

		public void UpdateDepartment(Department department)
		{
			var text = string.Format("UPDATE Departments SET name = '{0}', scheduler_id = {1}, date = {2} WHERE id = {3};",
				department.Name, department.SchedulerID, ConvertDate(department.BaseDate), department.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
		}

		public void UpdateDepartmentTeamsOrder(Department department, IEnumerable<Team> teams)
		{
			CommandTransactionWrapper((c, t) => SaveDepartmentTeamsOrder(department, teams, c, t));
			department.Teams.Clear();
			department.Teams.AddRange(teams);
		}

		public void UpdateHoliday(Vacation holiday)
		{
			var text = string.Format("UPDATE Holidays SET first_day = {0}, last_day = {1} WHERE id = {2};",
				ConvertDate(holiday.FirstDay), ConvertDate(holiday.LastDay), holiday.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
		}

		public void UpdateTeam(Team team)
		{
			var text = string.Format("UPDATE Teams SET name = '{0}' WHERE id = {1};", team.Name, team.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
		}

		public void UpdateTeamWorkersOrder(Team team, IEnumerable<Worker> workers)
		{
			CommandTransactionWrapper((c, t) => SaveTeamWorkersOrder(team, workers, c, t));
			team.Workers.Clear();
			team.Workers.AddRange(workers);
		}

		public void UpdateVacation(Vacation vacation)
		{
			var text = string.Format("UPDATE Vacations SET first_day = {0}, last_day = {1} WHERE id = {2};",
				ConvertDate(vacation.FirstDay), ConvertDate(vacation.LastDay), vacation.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
		}

		public void UpdateDayOff(Vacation dayOff)
		{
			var text = string.Format("UPDATE DaysOff SET first_day = {0}, last_day = {1} WHERE id = {2};",
				ConvertDate(dayOff.FirstDay), ConvertDate(dayOff.LastDay), dayOff.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
		}

		public void UpdateWorker(Worker worker)
		{
			var text = string.Format("UPDATE Workers SET first_name = '{0}', middle_name = '{1}', last_name = '{2}', number = '{3}', position = '{4}' WHERE id = {5};",
				worker.FirstName, worker.MiddleName, worker.LastName, worker.Number, worker.Position, worker.ID);
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var command = new SQLiteCommand(text, connection);
				command.ExecuteNonQuery();
				connection.Close();
			}
		}

		private static DateTime ConvertDate(long dt)
		{
			return DateTime.FromBinary(dt);
		}

		private static long ConvertDate(DateTime dt)
		{
			return dt.ToBinary();
		}

		public Team GetWorkerTeam(Worker worker)
		{
			return _teams.Find(t => t.Workers.Exists(w => w.ID == worker.ID));
		}

		public Department GetTeamDepartment(Team team)
		{
			return team == null ? null : _departments.Find(d => d.Teams.Exists(t => t.ID == team.ID));
		}

		private void CleanUpDatabase()
		{
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					var text = "DELETE FROM WorkersInTeams WHERE worker_id NOT IN (SELECT id FROM Workers) OR team_id NOT IN (SELECT id FROM Teams);";
					var command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();

					text = "DELETE FROM Vacations WHERE worker_id NOT IN (SELECT id FROM Workers);";
					command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();

					text = "DELETE FROM DaysOff WHERE worker_id NOT IN (SELECT id FROM Workers);";
					command = new SQLiteCommand(text, connection, transaction);
					command.ExecuteNonQuery();

					transaction.Commit();
				}
				catch
				{
					transaction.Rollback();
				}
				connection.Close();
			}
		}

		#region SQL

		private void InsertDepartmentCommand(Department department, SQLiteConnection connection, SQLiteTransaction transaction)
		{
			var text = string.Format("INSERT INTO Departments (name, scheduler_id, date) VALUES('{0}', {1}, {2});",
				department.Name, department.SchedulerID, ConvertDate(department.BaseDate));
			var command = new SQLiteCommand(text, connection, transaction);
			command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
			department.ID = connection.LastInsertRowId;
		}

		private List<string> GetColumnNamesCommand(string tableName, SQLiteConnection connection, SQLiteTransaction transaction)
		{
			var columns = new List<string>();
			var text = string.Format("PRAGMA table_info({0});", tableName);
			var command = new SQLiteCommand(text, connection, transaction);
			var reader = command.ExecuteReader();
			while (reader.Read())
			{
				var name = reader.GetString(1);
				columns.Add(name);
			}
			return columns;
		}

		private bool CommandTransactionWrapper(Action<SQLiteConnection, SQLiteTransaction> command)
		{
			var noError = true;
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					command(connection, transaction);
					transaction.Commit();
				}
				catch (Exception ex)
				{
					transaction.Rollback();
					noError = false;
				}
				connection.Close();
			}
			return noError;
		}

		private void AddColumnIfNotExists(string tableName, string columnName, string columnType, SQLiteConnection connection, SQLiteTransaction transaction)
		{
			var columns = GetColumnNamesCommand(tableName, connection, transaction);
			if (!columns.Contains(columnName))
			{
				var text = string.Format("ALTER TABLE {0} ADD COLUMN [{1}] {2}", tableName, columnName, columnType);
				var command = new SQLiteCommand(text, connection, transaction);
				command.ExecuteNonQuery();
			}
		}

		private void SaveDepartmentTeamsOrder(Department department, IEnumerable<Team> teams, SQLiteConnection connection, SQLiteTransaction transaction)
		{
			var i = 1;
			foreach (var team in teams)
			{
				var text = string.Format("UPDATE TeamsInDepartments SET [order] = {0} WHERE department_id = {1} AND team_id = {2};",
					i, department.ID, team.ID);
				var command = new SQLiteCommand(text, connection, transaction);
				command.ExecuteNonQuery();
				i++;
			}
		}

		private void SaveTeamWorkersOrder(Team team, IEnumerable<Worker> workers, SQLiteConnection connection, SQLiteTransaction transaction)
		{
			var i = 1;
			foreach (var worker in workers)
			{
				var text = string.Format("UPDATE WorkersInTeams SET [order] = {0} WHERE team_id = {1} AND worker_id = {2};",
					i, team.ID, worker.ID);
				var command = new SQLiteCommand(text, connection, transaction);
				command.ExecuteNonQuery();
				i++;
			}
		}

		#endregion SQL
	}
}
