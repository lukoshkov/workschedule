﻿using System;
using System.Collections.Generic;

namespace WorkSchedule.Data
{
	public interface IStorage
	{
		List<Department> GetDepartments();
		List<Team> GetTeams();
		List<Worker> GetWorkers();
		List<Worker> GetFreeWorkers();
		List<Vacation> GetHolidays();
		DateTime GetBaseDate();
		Team GetWorkerTeam(Worker worker);
		Department GetTeamDepartment(Team team);

		void SetBaseDate(DateTime date);

		void AddDepartment(Department department);
		void UpdateDepartment(Department department);
		void DeleteDepartment(Department department);
		void UpdateDepartmentTeamsOrder(Department department, IEnumerable<Team> teams);

		void AddTeam(Department department, Team team);
		void UpdateTeam(Team team);
		void DeleteTeam(Team team);

		void AddWorker(Worker worker);
		void UpdateWorker(Worker worker);
		void DeleteWorker(Worker worker);

		void AddWorkerToTeam(Worker worker, Team team);
		void DeleteWorkerFromTeam(Worker worker, Team team);
		void UpdateTeamWorkersOrder(Team team, IEnumerable<Worker> workers);

		void AddVacation(Vacation vacation, Worker worker);
		void UpdateVacation(Vacation vacation);
		void DeleteVacation(Vacation vacation);

		void AddDayOff(Vacation dayOff, Worker worker);
		void UpdateDayOff(Vacation dayOff);
		void DeleteDayOff(Vacation dayOff);

		void AddHoliday(Vacation holiday);
		void UpdateHoliday(Vacation holiday);
		void DeleteHoliday(Vacation holiday);
	}
}
