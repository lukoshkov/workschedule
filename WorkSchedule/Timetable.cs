﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkSchedule.Schedulers;

namespace WorkSchedule
{
	public class Timetable
	{
		public Worker[] Workers { get; private set; }
		public Team[] Teams { get; private set; }
		public float[,] Table { get; private set; }
		public DateTime StartDate { get; set; }
		public DateTime PeriodFirstDay { get; set; }
		public DateTime PeriodLastDay { get; set; }
		public float[] FullHours { get; private set; }
		public float[] NightHours { get; private set; }
		public SchedulerOptions Options { get; private set; }

		public Timetable(
			IEnumerable<Worker> workers, IEnumerable<Team> teams, float[,] table, 
			DateTime startDate, DateTime periodFirstDay, DateTime periodLastDay, 
			float[] fullHours, float[] nightHours, SchedulerOptions options)
		{
			Workers = workers.ToArray();
			Teams = teams.ToArray();
			Table = table;
			StartDate = startDate;
			PeriodFirstDay = periodFirstDay;
			PeriodLastDay = periodLastDay;
			FullHours = fullHours;
			NightHours = nightHours;
			Options = options;
		}
	}
}
