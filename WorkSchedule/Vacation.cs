﻿using System;

namespace WorkSchedule
{
	public class Vacation
	{
		private DateTime _start;
		private DateTime _end;

		public long ID { get; set; }
		public long WorkerID { get; set; }

		public DateTime FirstDay
		{
			get { return _start; }
			set { _start = value.Date; }
		}

		public DateTime LastDay
		{
			get { return _end; }
			set { _end = value.Date; }
		}

		public Vacation(DateTime firstDay, DateTime lastDay)
		{
			SetInterval(firstDay, lastDay);
		}

		public void SetInterval(DateTime firstDay, DateTime lastDay)
		{
			FirstDay = firstDay;
			LastDay = lastDay;
		}

		public bool IsInRange(DateTime day)
		{
			day = day.Date;
			return day >= FirstDay && day <= LastDay;
		}

		public override string ToString()
		{
			if (FirstDay == LastDay)
			{
				return string.Format("{0:dd.MM.yyyy}", FirstDay);
			}
			return string.Format("{0:dd.MM.yyyy} - {1:dd.MM.yyyy}", FirstDay, LastDay);
		}
	}
}
