﻿using System;
using System.Collections.Generic;

namespace WorkSchedule.Schedulers
{
	public abstract class SchedulerBase : IScheduler
	{
		public const float WorkerFreeDay = 0;
		public const float WorkerVacation = -1;
		public const float WorkerDayOff = -2;
		public const float WorkerHoliday = -3;


		public string Title { get; protected set; }

		protected SchedulerBase(string title)
		{
			Title = title;
		}

		public static DateTime Min(DateTime a, DateTime b)
		{
			return a < b ? a : b;
		}

		public static DateTime Max(DateTime a, DateTime b)
		{
			return a > b ? a : b;
		}

		public static DateTime GetMonthStart(DateTime date)
		{
			return new DateTime(date.Year, date.Month, 1);
		}

		public static DateTime GetMonthEnd(DateTime date)
		{
			DateTime start, end;
			GetMonthBorders(date, out start, out end);
			return end;
		}

		public static void GetMonthBorders(DateTime date, out DateTime start, out DateTime end)
		{
			start = GetMonthStart(date);
			end = start.AddMonths(1).AddDays(-1);
		}

		public static bool HasVacation(Worker worker, DateTime day, List<Vacation> holidays)
		{
			return worker.HasVacation(day) || IsHoliday(day, holidays);
		}

		public static bool IsHoliday(DateTime day, List<Vacation> holidays)
		{
			foreach (var holiday in holidays)
			{
				if (holiday.IsInRange(day))
				{
					return true;
				}
			}
			return false;
		}

		public Timetable CreateWorkersTimetable(Department department, DateTime calcStart, DateTime calcEnd, SchedulerOptions options, List<Vacation> holidays)
		{
			var startDate = options.HasFlag(SchedulerOptions.AddPrefixDay) ? calcStart.AddDays(-1) : calcStart;
			var endDate = options.HasFlag(SchedulerOptions.AddPostfixDay) ? calcEnd.AddDays(1) : calcEnd;
			return CreateWorkersTimetable(department, startDate, endDate, calcStart, calcEnd, options, holidays);
		}

		public virtual Timetable CreateWorkersTimetable(Department department, DateTime startDate, DateTime endDate, DateTime calcStart, DateTime calcEnd, SchedulerOptions options, List<Vacation> holidays)
		{
			startDate = startDate.Date;
			endDate = endDate.Date;
			calcStart = calcStart.Date;
			calcEnd = calcEnd.Date;

			var teams = CreateTeamsTable(department, startDate, endDate);
			if (teams == null)
			{
				return null;
			}

			var colCount = teams.GetLength(1);
			var workers = new List<Worker>();
			var teamNames = new List<Team>();
			var workersTeams = new List<int>();
			for (int i = 0; i < department.Teams.Count; i++)
			{
				foreach (var worker in department.Teams[i].Workers)
				{
					workers.Add(worker);
					teamNames.Add(department.Teams[i]);
					workersTeams.Add(i);
				}
			}

			var fullHours = new float[workers.Count];
			var nightHours = new float[workers.Count];
			var hours = new float[workers.Count, colCount];
			var shifts = new float[workers.Count, colCount];

			for (int wi = 0; wi < workers.Count; wi++)
			{
				var ti = workersTeams[wi];
				for (int c = 0; c < colCount; c++)
				{
					var currDay = startDate.AddDays(c);
					float nightH;
					SetHoursAndShift(teams[ti, c], workers[wi], holidays, currDay, out hours[wi, c], out nightH, out shifts[wi, c]);
					
					if (currDay >= calcStart && currDay <= calcEnd)
					{
						fullHours[wi] += Math.Max(0, hours[wi, c]);
						nightHours[wi] += Math.Max(0, nightH);
					}
				}
			}
			var table = options.HasFlag(SchedulerOptions.ShowHours) ? hours : shifts;
			var timetable = new Timetable(workers, teamNames, table, startDate, calcStart, calcEnd, fullHours, nightHours, options);
			return timetable;
		}

		protected virtual bool SetWorkFreeDayHoursAndShift(WorkingShift currShift, Worker worker, List<Vacation> holidays, DateTime currDay, out float hours, out float shift)
		{
			//if (currShift == WorkingShift.DayOff)
			//{
			//	hours = shift = WorkerFreeDay;
			//	return true;
			//}
			var isHoliday = IsHoliday(currDay, holidays);
			var isVacation = worker.HasVacation(currDay);
			var isDayOff = worker.HasDayOff(currDay);
			//hours = shift = WorkerFreeDay;
			//if (isDayOff)
			//{
			//	if (currShift != WorkingShift.DayOff)
			//	{
			//		hours = shift = WorkerDayOff;
			//	}
			//}
			//else if (isHoliday)
			//{
			//	hours = shift = WorkerHoliday;
			//}
			//else if (isVacation)
			//{
			//	hours = shift = WorkerVacation;
			//}
			//else
			//{
			//	return false;
			//}
			//return true;
			hours = shift =
				isDayOff ? WorkerDayOff :
				isHoliday ? WorkerHoliday :
				isVacation ? WorkerVacation : WorkerFreeDay;
			if (currShift == WorkingShift.DayOff && isDayOff)
			{
				hours = shift = WorkerFreeDay;
				return true;
			}
			return isDayOff || isVacation || isHoliday;
		}

		public abstract WorkingShift[,] CreateTeamsTable(Department department, DateTime startDate, DateTime endDate);

		protected abstract void SetHoursAndShift(WorkingShift currShift, Worker worker, List<Vacation> holidays, DateTime currDay, out float hours, out float nightHours, out float shift);
	}
}
