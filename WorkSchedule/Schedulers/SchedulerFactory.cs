﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSchedule.Schedulers
{
	public static class SchedulerFactory
	{
		public static readonly long InvalidID = -1;
		public static readonly long[] IDs = new long[] { 1, 2 };

		public static IScheduler CreateSchedulerByID(long id)
		{
			switch (id)
			{
				case 1:
					return new TwoShift12CicleScheduler();
				case 2:
					return new TwoShiftThreeWorkingDaysScheduler();
				default:
					return null;
			}
		}

		public static long GetSchedulerID(IScheduler scheduler)
		{
			if (scheduler is TwoShift12CicleScheduler)
			{
				return 1;
			}
			if (scheduler is TwoShiftThreeWorkingDaysScheduler)
			{
				return 2;
			}
			return 0;
		}
	}
}
