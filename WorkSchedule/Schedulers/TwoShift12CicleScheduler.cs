﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkSchedule.Schedulers
{
	public class TwoShift12CicleScheduler : SchedulerBase
	{
		public TwoShift12CicleScheduler()
			: base("12-4-8")
		{
		}

		public override WorkingShift[,] CreateTeamsTable(Department department, DateTime startDate, DateTime endDate)
		{
			if (department == null || department.Teams.Count == 0)
			{
				return null;
			}
			startDate = startDate.Date;
			endDate = endDate.Date;
			int colCount = (endDate - startDate).Days + 1;
			var table = new WorkingShift[department.Teams.Count, colCount];
			var interval = department.Teams.Count;
			var days = (startDate - department.BaseDate).Days;
			int turn = (days % interval + interval) % interval;
			int secondStartTurn = (turn + interval - 1) % interval;
			int secondEndTurn = (turn + interval - 2) % interval;
			for (int c = 0; c < colCount; c++)
			{
				table[turn, c] = WorkingShift.First;
				table[secondStartTurn, c] = WorkingShift.SecondStart;
				table[secondEndTurn, c] = WorkingShift.SecondEnd;
				secondEndTurn = secondStartTurn;
				secondStartTurn = turn;
				turn = (turn + 1) % interval;
			}
			return table;
		}

		protected override void SetHoursAndShift(WorkingShift currShift, Worker worker, List<Vacation> holidays, DateTime currDay, out float hours, out float nightHours, out float shift)
		{
			hours = 0;
			nightHours = 0;
			shift = 0;
			var nextDay = currDay.AddDays(1);
			var prevDay = currDay.AddDays(-1);
			////if (!worker.HasVacation(dt))
			//var hasDayOff = worker.HasDayOff(currDay);
			//var hasVacation = HasVacation(worker, currDay, holidays);
			//if (!hasDayOff && !hasVacation)
			if (!SetWorkFreeDayHoursAndShift(currShift, worker, holidays, currDay, out hours, out shift))
			{
				int night = 0;
				switch (currShift)
				{
					case WorkingShift.DayOff:
						break;
					case WorkingShift.First:
						hours = 12;
						shift = 1;
						break;
					case WorkingShift.SecondStart:
						//if (worker.HasDayOff(nextDay))
						//{
						//	hours = shift = WorkerDayOff;
						//	break;
						//}
						////if (!worker.HasVacation(dt.AddDays(1)))
						//if (!HasVacation(worker, nextDay, holidays))
						//{
						//	hours = 4;
						//	shift = 2;
						//	night += 2;
						//}
						if (!SetWorkFreeDayHoursAndShift(currShift, worker, holidays, nextDay, out hours, out shift))
						{
							hours = 4;
							shift = 2;
							night += 2;
						}
						break;
					case WorkingShift.SecondEnd:
						//if (worker.HasDayOff(prevDay))
						//{
						//	hours = shift = WorkerDayOff;
						//	break;
						//}
						////if (!worker.HasVacation(dt.AddDays(-1)))
						//if (!HasVacation(worker, prevDay, holidays))
						//{
						//	hours = 8;
						//	night += 6;
						//}
						if (!SetWorkFreeDayHoursAndShift(currShift, worker, holidays, prevDay, out hours, out shift))
						{
							hours = 8;
							night += 6;
						}
						break;
					case WorkingShift.Vacation:
						hours = shift = WorkerVacation;
						break;
				}
				nightHours = night;
			}
			//else if (hasDayOff)
			//{
			//	if (currShift != WorkingShift.DayOff)
			//	{
			//		hours = shift = WorkerDayOff;
			//	}
			//}
			//else
			//{
			//	hours = shift = WorkerVacation;
			//}
		}

		//private int[] GetTeamsTurns(DateTime day)
		//{
		//	var interval = Teams.Count;
		//	var days = (day - BaseDate).Days;
		//	int turn1 = (days % interval + interval) % interval;
		//	int turn2 = (turn1 + interval - 1) % interval;
		//	var turns = new int[interval];
		//	turns[turn1] = 1;
		//	turns[turn2] = 2;
		//	return turns;
		//}
	}
}
