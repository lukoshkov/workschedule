﻿using System;
using System.Collections.Generic;

namespace WorkSchedule.Schedulers
{
	public interface IScheduler
	{
		string Title { get; }
		
		Timetable CreateWorkersTimetable(Department department, DateTime calcStart, DateTime calcEnd, SchedulerOptions options, List<Vacation> holidays);
	}
}
