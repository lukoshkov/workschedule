﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSchedule.Schedulers
{
	public class TwoShiftThreeWorkingDaysScheduler : SchedulerBase
	{
		public TwoShiftThreeWorkingDaysScheduler()
			: base("Г3")
		{ }

		public override WorkingShift[,] CreateTeamsTable(Department department, DateTime startDate, DateTime endDate)
		{
			if (department == null || department.Teams.Count == 0)
			{
				return null;
			}
			startDate = startDate.Date;
			endDate = endDate.Date;
			int colCount = (endDate - startDate).Days + 1;
			var table = new WorkingShift[department.Teams.Count, colCount];
			var pairCount = department.Teams.Count / 2 + department.Teams.Count % 2;
			var interval = pairCount * 3;
			var days = (startDate - department.BaseDate).Days;
			days = days < 0 ? days + interval * (1 - days / interval) : days;

			for (int c = 0; c < colCount; c++)
			{
				int pairTurn = SafeMod(days / 3, pairCount);
				int teamTurn = SafeMod(days / 3 / pairCount, 2);
				var turn1 = pairTurn * 2 + teamTurn;
				var turn2 = pairTurn * 2 + (teamTurn + 1) % 2;
				if (turn1 < department.Teams.Count)
				{
					table[turn1, c] = WorkingShift.First;
				}
				if (turn2 < department.Teams.Count)
				{
					if (SafeMod((days - 1) / 3, pairCount) != pairTurn)
					{
						table[turn2, c] = WorkingShift.SecondStart;
					}
					else
					{
						if (c + 1 < colCount && SafeMod((days + 1) / 3, pairCount) != pairTurn)
						{
							table[turn2, c + 1] = WorkingShift.SecondEnd;
						}
						table[turn2, c] = WorkingShift.SecondEndAndStart;
					}
				}
				days++;
			}
			return table;
		}

		protected override void SetHoursAndShift(WorkingShift currShift, Worker worker, List<Vacation> holidays, DateTime currDay, out float hours, out float nightHours, out float shift)
		{
			hours = 0;
			nightHours = 0;
			shift = 0;
			var nextDay = currDay.AddDays(1);
			var prevDay = currDay.AddDays(-1);
			////if (!worker.HasVacation(dt))
			//var hasDayOff = worker.HasDayOff(currDay);
			//var hasVacation = HasVacation(worker, currDay, holidays);
			//if (!hasDayOff && !hasVacation)
			if (!SetWorkFreeDayHoursAndShift(currShift, worker, holidays, currDay, out hours, out shift))
			{
				float night = 0;
				switch (currShift)
				{
					case WorkingShift.DayOff:
						break;
					case WorkingShift.First:
						hours = 11.33f;
						shift = 1;
						break;
					case WorkingShift.SecondStart:
						//if (worker.HasDayOff(nextDay))
						//{
						//	hours = shift = WorkerDayOff;
						//	break;
						//}
						////if (!worker.HasVacation(dt.AddDays(1)))
						//if (!HasVacation(worker, nextDay, holidays))
						//{
						//	hours = 4.33f;
						//	shift = 2;
						//	night += 1.5f;
						//}
						if (!SetWorkFreeDayHoursAndShift(currShift, worker, holidays, nextDay, out hours, out shift))
						{
							hours = 4.33f;
							shift = 2;
							night += 1.5f;
						}
						break;
					case WorkingShift.SecondEnd:
						//if (worker.HasDayOff(prevDay))
						//{
						//	hours = shift = WorkerDayOff;
						//	break;
						//}
						////if (!worker.HasVacation(dt.AddDays(-1)))
						//if (!HasVacation(worker, prevDay, holidays))
						//{
						//	hours = 7;
						//	night += 5.83f;
						//}
						if (!SetWorkFreeDayHoursAndShift(currShift, worker, holidays, prevDay, out hours, out shift))
						{
							hours = 7;
							night += 5.83f;
						}
						break;
					case WorkingShift.SecondEndAndStart:
						if (worker.HasDayOff(nextDay) && worker.HasDayOff(prevDay))
						{
							hours = shift = WorkerDayOff;
							break;
						}
						if (IsHoliday(nextDay, holidays) && IsHoliday(prevDay, holidays))
						{
							hours = shift = WorkerHoliday;
							break;
						}
						if (worker.HasVacation(nextDay) && worker.HasVacation(prevDay))
						{
							hours = shift = WorkerHoliday;
							break;
						}
						//if (!worker.HasVacation(dt.AddDays(1)))
						if (!HasVacation(worker, nextDay, holidays))
						{
							hours = 4.33f;
							shift = 2;
							night += 1.5f;
						}
						//if (!worker.HasVacation(dt.AddDays(-1)))
						if (!HasVacation(worker, prevDay, holidays))
						{
							hours += 7;
							night += 5.83f;
						}
						break;
					case WorkingShift.Vacation:
						hours = shift = WorkerVacation;
						break;
				}
				nightHours = night;
			}
			//else if (hasDayOff)
			//{
			//	if (currShift != WorkingShift.DayOff)
			//	{
			//		hours = shift = WorkerDayOff;
			//	}
			//}
			//else
			//{
			//	hours = shift = WorkerVacation;
			//}
		}

		private static int SafeMod(int left, int right)
		{
			return (left % right + right) % right;
		}
	}
}
