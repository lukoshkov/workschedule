﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSchedule.Schedulers
{
	[Flags]
	public enum SchedulerOptions
	{
		None = 0,

		AddPrefixDay = 1 << 0,
		AddPostfixDay = 1 << 1,
		AddPaddingDays = AddPrefixDay | AddPostfixDay,
		ShowHours = 1 << 2,
		HasIndexColumn = 1 << 3,
		HasNumberColumn = 1 << 4,
		HasPositionColumn = 1 << 5,
		HasTeamColumn = 1 << 6,
		HasTeamRows = 1 << 7,

		All = ~0
	}
}
