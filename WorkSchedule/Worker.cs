﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSchedule
{
	public class Worker
	{
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string Number { get; set; }
		public string Position { get; set; }
		public long ID { get; set; }
		public List<Vacation> Vacations { get; private set; }
		public List<Vacation> DaysOff { get; private set; }

		public Worker(string firstName, string middleName, string lastName, string number)
		{
			FirstName = firstName;
			MiddleName = middleName;
			LastName = lastName;
			Number = number;
			Position = string.Empty;
			Vacations = new List<Vacation>();
			DaysOff = new List<Vacation>();
		}

		public bool HasVacation(DateTime day)
		{
			return HasInterval(day, Vacations);
		}

		public bool HasDayOff(DateTime day)
		{
			return HasInterval(day, DaysOff);
		}

		private bool HasInterval(DateTime day, List<Vacation> intervals)
		{
			foreach (var interval in intervals)
			{
				if (interval.IsInRange(day))
				{
					return true;
				}
			}
			return false;
		}

		public string GetShortName()
		{
			return string.Format("{0} {1}.{2}.", LastName,
				string.IsNullOrEmpty(FirstName) ? '_' : FirstName[0],
				string.IsNullOrEmpty(MiddleName) ? '_' : MiddleName[0]);
		}

		public string GetFullName()
		{
			return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
		}

		public override string ToString()
		{
			return GetShortName();
		}
	}
}
