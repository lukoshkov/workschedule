﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using WorkSchedule.Commands;

namespace WorkSchedule.Helpers
{
	public class ListViewController<T> : INotifyPropertyChanged
		where T : class
	{
		private ObservableCollection<T> _source;
		private int _selectedIndex;
		private ICommand _moveUpCommand;
		private ICommand _moveDownCommand;

		#region Properties

		public ObservableCollection<T> Source
		{
			get { return _source; }
			set
			{
				_source = value;
				NotifyPropertyChanged(nameof(Source));
			}
		}

		public int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				_selectedIndex = value;
				NotifyPropertyChanged(nameof(SelectedIndex));
			}
		}

		public ICommand MoveUpCommand
		{
			get
			{
				if (_moveUpCommand == null)
				{
					_moveUpCommand = new RelayCommand(p => MoveUpExecute(p));
				}
				return _moveUpCommand;
			}
		}

		public ICommand MoveDownCommand
		{
			get
			{
				if (_moveDownCommand == null)
				{
					_moveDownCommand = new RelayCommand(p => MoveDownExecute(p));
				}
				return _moveDownCommand;
			}
		}

		#endregion Properties

		public ListViewController(IEnumerable<T> source)
		{
			_selectedIndex = -1;
			UpdateSource(source);
		}

		public void UpdateSource(IEnumerable<T> source)
		{
			if (source == null)
			{
				Source = new ObservableCollection<T>();
			}
			else
			{
				Source = new ObservableCollection<T>(source);
			}
		}

		private void MoveUpExecute(object param)
		{
			var index = Source.IndexOf(param as T);
			var length = Source.Count;
			Source.Move(index, (index - 1 + length) % length);
		}

		private void MoveDownExecute(object param)
		{
			var index = Source.IndexOf(param as T);
			var length = Source.Count;
			Source.Move(index, (index + 1) % length);
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
