﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorkSchedule.Data;
using WorkSchedule.Export;
using WorkSchedule.Helpers;
using WorkSchedule.Schedulers;

namespace WorkSchedule
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private IScheduler[] _schedulers;
		private IStorage _storage;
		private List<Worker> _workers;
		private List<Worker> _freeWorkers;
		private List<Team> _teams;
		private ObservableCollection<Department> _departments;
		private Department _workerDepartment;
		private List<Vacation> _holidays;

		private ListViewController<Team> _teamsListController;
		private ListViewController<Worker> _workersListController;

		private readonly string _titleIsEmpty = "Поле названия не может быть пустым.";
		private readonly string _baseDateNotSet = "Поле даты не может быть пустым.";
		private readonly string _schedulerNotSelected = "Не выбран тип графика работы.";
		private readonly string _workerNotSelected = "Выберите сотрудника из списка.";
		private readonly string _vacationNotSelected = "Выберите отпуск из списка.";
		private readonly string _teamNotSelected = "Выберите смену из списка.";
		private readonly string _departmentNotSelected = "Выберите участок из списка.";
		private readonly string _intervalNotSetted = "Поля \"Начало периода\" и \"Конец периода\" должны быть заполнены.";
		private readonly string _intervalReversed = "Значение поля \"Начало периода\" должно быть меньше значения поля \"Конец периода\".";
		private readonly string _teamListIsEmpty = "Список смен пуст.";
		private readonly string _error = "Ошибка";
		private readonly string _databasePath = "WorkSchedule.db";

		public ListViewController<Team> TeamsListController
		{
			get
			{
				if (_teamsListController == null)
				{
					_teamsListController = new ListViewController<Team>(null);
				}
				return _teamsListController;
			}
		}

		public ListViewController<Worker> WorkersListController
		{
			get
			{
				if (_workersListController == null)
				{
					_workersListController = new ListViewController<Worker>(null);
				}
				return _workersListController;
			}
		}

		public MainWindow()
		{
			InitializeComponent();
			var x = SchedulerOptions.All;
			var y = x.HasFlag(SchedulerOptions.None);
			_storage = new SQLiteStorage(_databasePath);
			//_teams = _storage.GetTeams();
			_workers = _storage.GetWorkers();
			_freeWorkers = _storage.GetFreeWorkers();
			_holidays = _storage.GetHolidays();

			_schedulers = SchedulerFactory.IDs.Select(id => SchedulerFactory.CreateSchedulerByID(id)).ToArray();
			//_scheduler.Teams.AddRange(_teams);
			//_scheduler.BaseDate = _storage.GetBaseDate();
			_departments = new ObservableCollection<Department>(_storage.GetDepartments());
			UpdateWorkersFilter();
			WorkersFilter.SelectedIndex = 0;

			UpdateDepartments();
			DepartmentsList.SelectionChanged += DepartmentsList_SelectionChanged;
			DepartmentSchedulerSelector.ItemsSource = _schedulers.Select(s => s.Title);

			//TeamList.ItemsSource = _teams;
			TeamList.SelectionChanged += TeamList_SelectionChanged;

			FreeWorkersList.ItemsSource = _freeWorkers;

			WorkersList.ItemsSource = _workers;
			WorkersList.SelectionChanged += WorkersList_SelectionChanged;

			WorkerView.Department.SelectionChanged += (s, e) =>
			{
				var departmentIndex = WorkerView.Department.SelectedIndex - 1;
				_workerDepartment = departmentIndex < 0 ? null : _departments[departmentIndex];
				UpdateWorkerViewTeams();
			};
			WorkerVacationsList.SelectionChanged += WorkerVacationsList_SelectionChanged;
			WorkerDaysOffList.SelectionChanged += WorkerDaysOffList_SelectionChanged;

			StartDate.SelectedDate = DateTime.Now;
			BaseDate.SelectedDate = _departments[0].BaseDate;

			UpdateWorkerViewDepartments();
			UpdateWorkerViewTeams();
			//PopulateDB("test.db");
			
			HolidaysList.ItemsSource = _holidays;
			HolidaysList.SelectionChanged += HolidaysList_SelectionChanged;
		}

		void PopulateDB(string path)
		{
			var storage = new SQLiteStorage(path);
			var department = new Department("БЕЗ НАЗВАНИЯ", SchedulerFactory.IDs[0], DateTime.Now.Date);
			storage.AddDepartment(department);
			var teams = new List<Team>()
			{
				new Team("Г"), new Team("Б"), new Team("А"), new Team("В")
			};
			foreach (var team in teams)
			{
				storage.AddTeam(department, team);
			}
			var workers = new List<Worker>()
			{
				new Worker("Ю", "Д", "Малахов", "44453"),
				new Worker("Е", "Д", "Крысанова", "37860"),
				new Worker("А", "Д", "Морсаков", "45182"),
				new Worker("И", "В", "Стодушная", "20620"),
				new Worker("А", "В", "Королев", "33973"),
				new Worker("О", "А", "Штырева", "84413"),
				new Worker("В", "Г", "Стырков", "63000"),
				new Worker("Т", "И", "Алексеева", "2767")
			};
			foreach (var worker in workers)
			{
				storage.AddWorker(worker);
			}
			for (int i = 0; i < teams.Count; i++)
			{
				storage.AddWorkerToTeam(workers[2*i], teams[i]);
				storage.AddWorkerToTeam(workers[2*i + 1], teams[i]);
			}
		}

		private void WorkerVacationsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			TimeIntervalsList_SelectionChanged(WorkerVacationsList, VacationInterval);
		}

		private void WorkerDaysOffList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			TimeIntervalsList_SelectionChanged(WorkerDaysOffList, DayOffInterval);
		}

		private void HolidaysList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			TimeIntervalsList_SelectionChanged(HolidaysList, HolidayInterval);
		}

		private void TimeIntervalsList_SelectionChanged(ListView source, Views.TimeIntervalView intervalView)
		{
			var interval = source.SelectedItem as Vacation;
			if (interval != null)
			{
				intervalView.StartDate.SelectedDate = interval.FirstDay;
				intervalView.EndDate.SelectedDate = interval.LastDay;
			}
			else
			{
				intervalView.StartDate.SelectedDate = null;
				intervalView.EndDate.SelectedDate = null;
			}
		}

		private void DepartmentsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var department = DepartmentsList.SelectedItem as Department;
			if (department != null)
			{
				DepartmentName.Text = department.Name;
				DepartmentBaseDate.SelectedDate = department.BaseDate;
				DepartmentSchedulerSelector.SelectedItem = department.Scheduler?.Title;
				_teams = department.Teams;
			}
			else
			{
				DepartmentName.Text = string.Empty;
				DepartmentBaseDate.SelectedDate = null;
				DepartmentSchedulerSelector.SelectedItem = null;
				_teams = null;
			}
			UpdateTeams();
		}
		
		private void TeamList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var team = TeamList.SelectedItem as Team;
			if (team != null)
			{
				WorkersListController.UpdateSource(team.Workers);
				//TeamWorkersList.ItemsSource = team.Workers;
				TeamName.Text = team.Name;
			}
			else
			{
				WorkersListController.UpdateSource(null);
				//TeamWorkersList.ItemsSource = null;
				TeamName.Text = string.Empty;
			}
		}

		private void WorkersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker != null)
			{
				WorkerView.FirstName.Text = worker.FirstName;
				WorkerView.LastName.Text = worker.LastName;
				WorkerView.MiddleName.Text = worker.MiddleName;
				WorkerView.Number.Text = worker.Number;
				WorkerView.Position.Text = worker.Position;
				WorkerVacationsList.ItemsSource = worker.Vacations;
				WorkerDaysOffList.ItemsSource = worker.DaysOff;
				var team = _storage.GetWorkerTeam(worker);
				var department = _storage.GetTeamDepartment(team);
				if (department == null)
				{
					WorkerView.Department.SelectedIndex = 0;
				}
				else
				{
					WorkerView.Department.SelectedValue = department.Name;
				}
				if (team == null)
				{
					WorkerView.Team.SelectedIndex = 0;
				}
				else
				{
					WorkerView.Team.SelectedValue = team.Name;
				}
			}
			else
			{
				WorkerVacationsList.ItemsSource = null;
				WorkerDaysOffList.ItemsSource = null;
			}
		}

		private void WorkersFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			UpdateWorkers();
		}

		private void CreatePlanButton_Click(object sender, RoutedEventArgs e)
		{
			var department = DepartmentSelector.SelectedItem as Department;
			if (department == null)
			{
				MessageBox.Show(_departmentNotSelected, _error);
			}
			else if (department.Teams.Count == 0)
			{
				MessageBox.Show(_teamListIsEmpty, _error, MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else if (StartDate.SelectedDate.HasValue && EndDate.SelectedDate.HasValue)
			{
				var start = StartDate.SelectedDate.Value;
				var end = EndDate.SelectedDate.Value;
				if (start > end)
				{
					MessageBox.Show(_intervalReversed, _error);
					return;
				}
				var options = AddNeighbors.IsChecked == true ? SchedulerOptions.AddPaddingDays : SchedulerOptions.None;
				if (TableFormatSelector.SelectedIndex == 0)
				{
					options |= SchedulerOptions.ShowHours;
				}
				if (TableColumnsSelector.SelectedIndex == 0)
				{
					options |= SchedulerOptions.HasTeamColumn;
				}
				else
				{
					options = options | SchedulerOptions.HasIndexColumn | SchedulerOptions.HasPositionColumn | SchedulerOptions.HasNumberColumn | SchedulerOptions.HasTeamRows;
				}
				var tables = new List<Timetable>();
				try
				{
					if (SplitIntoMonths.IsChecked == true)
					{
						var dt = start;
						DateTime mStart, mEnd;
						while (dt <= end)
						{
							SchedulerBase.GetMonthBorders(dt, out mStart, out mEnd);
							var pStart = SchedulerBase.Max(start, mStart);
							var pEnd = SchedulerBase.Min(end, mEnd);
							tables.Add(department.CreateWorkersTimetable(pStart, pEnd, options, _holidays));
							dt = mEnd.AddDays(1);
						}
					}
					else
					{
						tables.Add(department.CreateWorkersTimetable(start, end, options, _holidays));
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(string.Format("Произошла ошибка при расчёте графика.\nДетали:\n{0}\n{1}", ex.Message, ex.StackTrace),
						_error, MessageBoxButton.OK, MessageBoxImage.Error);
					return;
				}
				var dialog = new Microsoft.Win32.SaveFileDialog();
				dialog.DefaultExt = ".csv";
				dialog.Filter = "Файлы значений |*.csv";
				dialog.InitialDirectory = Environment.CurrentDirectory;
				if (dialog.ShowDialog() == true)
				{
					try
					{
						var csv = new CSVExporter(dialog.FileName);
						csv.Export(tables);
					}
					catch (Exception ex)
					{
						MessageBox.Show(string.Format("Произошла ошибка при сохранении файла.\nДетали:\n{0}\n{1}", ex.Message, ex.StackTrace),
							_error, MessageBoxButton.OK, MessageBoxImage.Error);
					}
					try
					{
						var proc = new System.Diagnostics.Process();
						proc.StartInfo = new System.Diagnostics.ProcessStartInfo(dialog.FileName);
						proc.Start();
					}
					catch (Exception ex) { }
				}
			}
			else
			{
				MessageBox.Show(_intervalNotSetted, _error);
			}
		}

		private void SelectMonthButton_Click(object sender, RoutedEventArgs e)
		{
			if (StartDate.SelectedDate.HasValue)
			{
				DateTime start, end;
				SchedulerBase.GetMonthBorders(StartDate.SelectedDate.Value, out start, out end);
				StartDate.SelectedDate = start;
				EndDate.SelectedDate = end;
			}
		}

		private void EditBaseDateButton_Click(object sender, RoutedEventArgs e)
		{
			var date = BaseDate.SelectedDate;
			if (date.HasValue)
			{
				if (MessageBox.Show("Вы уверены, что хотите изменить дату?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					_storage.SetBaseDate(date.Value);
					_departments[0].BaseDate = date.Value;
				}
				else
				{
					BaseDate.SelectedDate = _departments[0].BaseDate;
				}
			}
			else
			{
				MessageBox.Show("Поле даты не должно быть пустым.");
				BaseDate.SelectedDate = _departments[0].BaseDate;
			}
		}

		private void CreateWorkerButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = new Worker(WorkerView.FirstName.Text, WorkerView.MiddleName.Text, WorkerView.LastName.Text, WorkerView.Number.Text);
			worker.Position = WorkerView.Position.Text;
			_storage.AddWorker(worker);
			EditWorkerTeam(worker);
			UpdateWorkers();
		}

		private void EditWorkerButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker != null)
			{
				worker.FirstName = WorkerView.FirstName.Text;
				worker.LastName = WorkerView.LastName.Text;
				worker.MiddleName = WorkerView.MiddleName.Text;
				worker.Number = WorkerView.Number.Text;
				worker.Position = WorkerView.Position.Text;
				_storage.UpdateWorker(worker);
				EditWorkerTeam(worker);
				UpdateWorkers();
			}
			else
			{
				MessageBox.Show(_workerNotSelected);
			}
		}

		private void DeleteWorkerButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker != null)
			{
				_storage.DeleteWorker(worker);
				UpdateWorkers();
			}
			else
			{
				MessageBox.Show(_workerNotSelected);
			}
		}

		private void AddVacationButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker == null)
			{
				MessageBox.Show(_workerNotSelected);
				return;
			}
			var vacation = AddTimeIntervalButtonClick(VacationInterval);
			if (vacation == null) return;
			_storage.AddVacation(vacation, worker);
			UpdateVacations();
		}

		private void EditVacationButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker == null)
			{
				MessageBox.Show(_workerNotSelected);
				return;
			}
			var vacation = EditTimeIntervalButtonClick(WorkerVacationsList, VacationInterval);
			if (vacation == null) return;
			_storage.UpdateVacation(vacation);
			UpdateVacations();
		}

		private void DeleteVacationButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker == null)
			{
				MessageBox.Show(_workerNotSelected);
				return;
			}
			var vacation = DeleteTimeIntervalButtonClick(WorkerVacationsList);
			if (vacation == null) return;
			_storage.DeleteVacation(vacation);
			UpdateVacations();
		}

		private void SetDayLongVacationButton_Click(object sender, RoutedEventArgs e)
		{
			SetDayLongTimeIntervalClick(VacationInterval);
		}

		private void AddDayOffButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker == null)
			{
				MessageBox.Show(_workerNotSelected);
				return;
			}
			var dayOff = AddTimeIntervalButtonClick(DayOffInterval);
			if (dayOff == null) return;
			_storage.AddDayOff(dayOff, worker);
			UpdateDaysOff();
		}

		private void EditDayOffButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker == null)
			{
				MessageBox.Show(_workerNotSelected);
				return;
			}
			var dayOff = EditTimeIntervalButtonClick(WorkerDaysOffList, DayOffInterval);
			if (dayOff == null) return;
			_storage.UpdateDayOff(dayOff);
			UpdateDaysOff();
		}

		private void DeleteDayOffButton_Click(object sender, RoutedEventArgs e)
		{
			var worker = WorkersList.SelectedItem as Worker;
			if (worker == null)
			{
				MessageBox.Show(_workerNotSelected);
				return;
			}
			var dayOff = DeleteTimeIntervalButtonClick(WorkerDaysOffList);
			if (dayOff == null) return;
			_storage.DeleteDayOff(dayOff);
			UpdateDaysOff();
		}

		private void SetDayLongDayOffButton_Click(object sender, RoutedEventArgs e)
		{
			SetDayLongTimeIntervalClick(DayOffInterval);
		}

		private void AddHolidayButton_Click(object sender, RoutedEventArgs e)
		{
			var holiday = AddTimeIntervalButtonClick(HolidayInterval);
			if (holiday == null) return;
			_storage.AddHoliday(holiday);
			UpdateHolidays();
		}

		private void EditHolidayButton_Click(object sender, RoutedEventArgs e)
		{
			var holiday = EditTimeIntervalButtonClick(HolidaysList, HolidayInterval);
			if (holiday == null) return;
			_storage.UpdateHoliday(holiday);
			UpdateHolidays();
		}

		private void DeleteHolidayButton_Click(object sender, RoutedEventArgs e)
		{
			var holiday = DeleteTimeIntervalButtonClick(HolidaysList);
			if (holiday == null) return;
			_holidays.Remove(holiday);
			_storage.DeleteHoliday(holiday);
			UpdateHolidays();
		}

		private void SetDayLongHolidayButton_Click(object sender, RoutedEventArgs e)
		{
			SetDayLongTimeIntervalClick(HolidayInterval);
		}

		private Vacation AddTimeIntervalButtonClick(Views.TimeIntervalView intervalView)
		{
			string message;
			Vacation interval;
			if (!TryParseTimeInterval(intervalView, out interval, out message))
			{
				MessageBox.Show(message);
				return null;
			}
			return interval;
		}

		private Vacation EditTimeIntervalButtonClick(ListView intervals, Views.TimeIntervalView intervalView)
		{
			var selected = intervals.SelectedItem as Vacation;
			if (selected == null)
			{
				MessageBox.Show(_vacationNotSelected);
				return null;
			}
			string message;
			Vacation interval;
			if (!TryParseTimeInterval(intervalView, out interval, out message))
			{
				MessageBox.Show(message);
				return null;
			}
			selected.SetInterval(interval.FirstDay, interval.LastDay);
			return selected;
		}

		private Vacation DeleteTimeIntervalButtonClick(ListView intervals)
		{
			var selected = intervals.SelectedItem as Vacation;
			if (selected == null)
			{
				MessageBox.Show(_vacationNotSelected);
				return null;
			}
			return selected;
		}

		private void SetDayLongTimeIntervalClick(Views.TimeIntervalView intervalView)
		{
			intervalView.EndDate.SelectedDate = intervalView.StartDate.SelectedDate;
		}

		private void AddDepartmentButton_Click(object sender, RoutedEventArgs e)
		{
			Department department = null;
			string error;
			if (!ParseDepartmentInfo(ref department, out error))
			{
				MessageBox.Show(error, _error);
				return;
			}
			_storage.AddDepartment(department);
			UpdateDepartments();
			DepartmentsList.SelectedItem = department;
			UpdateWorkerViewDepartments();
		}

		private void DeleteDepartmentButton_Click(object sender, RoutedEventArgs e)
		{
			var department = DepartmentsList.SelectedItem as Department;
			if (department == null)
			{
				MessageBox.Show(_departmentNotSelected, _error);
				return;
			}
			_storage.DeleteDepartment(department);
			UpdateDepartments();
			UpdateWorkerViewDepartments();
		}

		private void EditDepartmentButton_Click(object sender, RoutedEventArgs e)
		{
			var department = DepartmentsList.SelectedItem as Department;
			if (department == null)
			{
				MessageBox.Show(_departmentNotSelected, _error);
				return;
			}
			string error;
			if (!ParseDepartmentInfo(ref department, out error))
			{
				MessageBox.Show(error, _error);
				return;
			}
			_storage.UpdateDepartment(department);
			UpdateDepartments();
			DepartmentsList.SelectedItem = department;
			UpdateWorkerViewDepartments();
		}

		private bool ParseDepartmentInfo(ref Department department, out string error)
		{
			DateTime date;
			string name;
			long schedulerID;
			if (ParseDepartmentInfo(out name, out date, out schedulerID, out error))
			{
				if (department == null)
				{
					department = new Department(name, schedulerID, date);
				}
				else
				{
					department.Name = name;
					department.SchedulerID = schedulerID;
					department.BaseDate = date;
				}
				return true;
			}
			return false;
		}

		private bool ParseDepartmentInfo(out string name, out DateTime baseDate, out long schedulerID, out string error)
		{
			name = DepartmentName.Text;
			var date = DepartmentBaseDate.SelectedDate;
			schedulerID = GetSelectedSchedulerID();
			var sb = new StringBuilder();
			if (string.IsNullOrWhiteSpace(name))
			{
				sb.AppendLine(_titleIsEmpty);
			}
			if (date.HasValue)
			{
				baseDate = date.Value;
			}
			else
			{
				baseDate = DateTime.MinValue;
				sb.AppendLine(_baseDateNotSet);
			}
			if (schedulerID == SchedulerFactory.InvalidID)
			{
				sb.AppendLine(_schedulerNotSelected);
			}
			error = sb.ToString();
			return string.IsNullOrEmpty(error);
		}

		private void AddTeamButton_Click(object sender, RoutedEventArgs e)
		{
			var department = DepartmentsList.SelectedItem as Department;
			if (department != null)
			{
				var team = new Team(TeamName.Text);
				_storage.AddTeam(department, team);
				UpdateTeams();
				UpdateWorkerViewTeams();
			}
			else
			{
				MessageBox.Show(_departmentNotSelected);
			}
		}

		private void DeleteTeamButton_Click(object sender, RoutedEventArgs e)
		{
			var team = TeamList.SelectedItem as Team;
			if (team == null)
			{
				MessageBox.Show(_teamNotSelected);
				return;
			}
			_storage.DeleteTeam(team);
			UpdateTeams();
			UpdateWorkerViewTeams();
		}

		private void EditTeamButton_Click(object sender, RoutedEventArgs e)
		{
			var team = TeamList.SelectedItem as Team;
			if (team == null)
			{
				MessageBox.Show(_teamNotSelected);
				return;
			}
			team.Name = TeamName.Text;
			_storage.UpdateTeam(team);
			UpdateTeams();
			UpdateWorkerViewTeams();
		}

		private void SaveTeamsOrderButton_Click(object sender, RoutedEventArgs e)
		{
			var department = DepartmentsList.SelectedItem as Department;
			if (department != null)
			{
				_storage.UpdateDepartmentTeamsOrder(department, TeamsListController.Source);
			}
			else
			{
				MessageBox.Show(_departmentNotSelected);
			}
		}

		private void SaveWorkersOrderButton_Click(object sender, RoutedEventArgs e)
		{
			var team = TeamList.SelectedItem as Team;
			if (team != null)
			{
				_storage.UpdateTeamWorkersOrder(team, WorkersListController.Source);
			}
			else
			{
				MessageBox.Show(_teamNotSelected);
			}
		}

		private void DeleteWorkerFromTeamButton_Click(object sender, RoutedEventArgs e)
		{
			var team = TeamList.SelectedItem as Team;
			if (team != null)
			{
				var worker = TeamWorkersList.SelectedItem as Worker;
				if (worker != null)
				{
					_storage.DeleteWorkerFromTeam(worker, team);
					UpdateTeamWorkers();
				}
				else
				{
					MessageBox.Show(_workerNotSelected);
				}
			}
			else
			{
				MessageBox.Show(_teamNotSelected);
			}
		}

		private void AddWorkerToTeamButton_Click(object sender, RoutedEventArgs e)
		{
			var team = TeamList.SelectedItem as Team;
			if (team != null)
			{
				var worker = FreeWorkersList.SelectedItem as Worker;
				if (worker != null)
				{
					_storage.AddWorkerToTeam(worker, team);
					UpdateTeamWorkers();
				}
				else
				{
					MessageBox.Show(_workerNotSelected);
				}
			}
			else
			{
				MessageBox.Show(_teamNotSelected);
			}
		}

		private void EditWorkerTeam(Worker worker)
		{
			var oldTeam = _storage.GetWorkerTeam(worker);
			var teamIndex = WorkerView.Team.SelectedIndex;
			var departmentIndex = WorkerView.Department.SelectedIndex;
			var department = departmentIndex <= 0 ? null : _departments[departmentIndex - 1];
			var team = department == null || teamIndex == 0 || teamIndex > department.Teams.Count ? null : department.Teams[teamIndex - 1];

			if (oldTeam != null)
			{
				if (team == null || team.ID != oldTeam.ID)
				{
					_storage.DeleteWorkerFromTeam(worker, oldTeam);
					if (team != null)
					{
						_storage.AddWorkerToTeam(worker, team);
					}
				}
			}
			else
			{
				if (team != null)
				{
					_storage.AddWorkerToTeam(worker, team);
				}
			}
			UpdateTeams();
		}

		private long GetSelectedSchedulerID()
		{
			var index = DepartmentSchedulerSelector.SelectedIndex;
			if (index < 0 || index >= _schedulers.Length)
			{
				return SchedulerFactory.InvalidID;
			}
			return SchedulerFactory.IDs[index];
		}

		private void UpdateDepartments()
		{
			_departments = new ObservableCollection<Department>(_storage.GetDepartments());
			//DepartmentsList.ItemsSource = null;
			DepartmentsList.ItemsSource = _departments;
			//DepartmentSelector.ItemsSource = null;
			DepartmentSelector.ItemsSource = _departments;
			UpdateWorkersFilter();
		}

		private void UpdateTeams()
		{
			TeamsListController.UpdateSource(_teams);
			return;
			TeamList.ItemsSource = null;
			TeamList.ItemsSource = _teams;
		}

		private void UpdateWorkers()
		{
			var index = WorkersFilter.SelectedIndex;
			if (index <= 0)
			{
				_workers = _storage.GetWorkers();
			}
			else if (index == 1)
			{
				_workers = _freeWorkers;
			}
			else
			{
				_workers = _departments[index - 2].GetWorkers();
			}
			WorkersList.ItemsSource = null;
			WorkersList.ItemsSource = _workers;
			UpdateTeamWorkers();
		}

		private void UpdateVacations()
		{
			WorkerVacationsList.ItemsSource = null;
			var worker = WorkersList.SelectedItem as Worker;
			if (worker != null)
			{
				WorkerVacationsList.ItemsSource = worker.Vacations;
			}
		}

		private void UpdateDaysOff()
		{
			WorkerDaysOffList.ItemsSource = null;
			var worker = WorkersList.SelectedItem as Worker;
			if (worker != null)
			{
				WorkerDaysOffList.ItemsSource = worker.DaysOff;
			}
		}

		private void UpdateTeamWorkers()
		{
			var index = TeamList.SelectedIndex;
			//TeamList.ItemsSource = null;
			//TeamWorkersList.ItemsSource = null;
			FreeWorkersList.ItemsSource = null;
			//TeamList.ItemsSource = _teams;
			//TeamList.SelectedIndex = index;
			FreeWorkersList.ItemsSource = _freeWorkers;
			var team = TeamList.SelectedItem as Team;
			if (team != null)
			{
				WorkersListController.UpdateSource(team.Workers);
				//TeamWorkersList.ItemsSource = team.Workers;
			}
		}

		private void UpdateWorkerViewDepartments()
		{
			WorkerView.Department.ItemsSource = null;
			var departmentNames = _departments.Select(t => t.Name).ToList();
			departmentNames.Insert(0, "<Без участка>");
			WorkerView.Department.ItemsSource = departmentNames;
		}

		private void UpdateWorkerViewTeams()
		{
			WorkerView.Team.ItemsSource = null;
			if (_workerDepartment != null)
			{
				var teamNames = _workerDepartment.Teams.Select(t => t.Name).ToList();
				teamNames.Insert(0, "<Без смены>");
				WorkerView.Team.ItemsSource = teamNames;
			}
		}

		private void UpdateWorkersFilter()
		{
			var filters = new List<string>()
			{
				"Всех",
				"Нераспределённых"
			};
			filters.AddRange(_departments.Select(d => d.Name));
			WorkersFilter.ItemsSource = filters;
			WorkersFilter.SelectedIndex = 0;
		}

		private void UpdateHolidays()
		{
			HolidaysList.ItemsSource = null;
			HolidaysList.ItemsSource = _holidays;
		}

		private bool TryParseTimeInterval(Views.TimeIntervalView intervalView, out Vacation interval, out string errorMessage)
		{
			var start = intervalView.StartDate.SelectedDate;
			var end = intervalView.EndDate.SelectedDate;
			if (IsTimeIntervalValid(start, end, out errorMessage))
			{
				interval = new Vacation(start.Value, end.Value);
				return true;
			}
			interval = null;
			return false;
		}

		private bool IsTimeIntervalValid(DateTime? start, DateTime? end, out string errorMessage)
		{
			if (!start.HasValue || !end.HasValue)
			{
				errorMessage = _intervalNotSetted;
				return false;
			}
			if (start.Value > end.Value)
			{
				errorMessage = _intervalReversed;
				return false;
			}
			errorMessage = string.Empty;
			return true;
		}
	}
}
