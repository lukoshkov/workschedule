﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkSchedule
{
	public class Team
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public List<Worker> Workers { get; private set; }

		public Team(string name)
		{
			Name = name;
			Workers = new List<Worker>();
		}

		public string GetNamesList()
		{
			var sb = new StringBuilder();
			for (int i = 0; i < Workers.Count; i++)
			{
				sb.Append(Workers[i].GetShortName());
				if (i < Workers.Count - 1)
				{
					sb.Append(", ");
				}
			}
			return sb.ToString();
		}

		public override string ToString()
		{
			return Name;
			var sb = new StringBuilder();
			sb.Append(Name);
			if (Workers.Count > 0)
			{
				sb.Append(": ");
				sb.Append(Workers[0].GetShortName());
			}
			if (Workers.Count > 1)
			{
				sb.Append(" и ");
				sb.Append(Workers.Count - 1);
				sb.Append(" чел.");
			}
			return sb.ToString();
		}
	}
}
