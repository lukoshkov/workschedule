﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSchedule
{
	public enum WorkingShift
	{
		DayOff,
		First,
		SecondStart,
		SecondEnd,
		SecondEndAndStart,
		Vacation = -1
	}
}
