﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WorkSchedule.Schedulers;

namespace WorkSchedule.Export
{
	public class CSVExporter : IExporter
	{
		private string _path;

		public CSVExporter(string filepath)
		{
			_path = filepath;
		}

		public void Export(IEnumerable<Timetable> tables)
		{
			var append = false;
			foreach (var table in tables)
			{
				Export(table, append);
				append = true;
			}
		}

		public void Export(Timetable table)
		{
			Export(table, false);
		}

		public void Export(Timetable table, bool append)
		{
			using (var file = new StreamWriter(new FileStream(_path, append ? FileMode.Append : FileMode.Create), Encoding.GetEncoding(1251)))
			{
				if (append)
				{
					file.WriteLine("\n");
				}
				var preTableColumns = 1;
				var preWorkerColumns = "";
				var columnNames = new StringBuilder();
				var hasIndex = table.Options.HasFlag(SchedulerOptions.HasIndexColumn);
				var hasTeam = table.Options.HasFlag(SchedulerOptions.HasTeamColumn);
				var hasTeamRows = table.Options.HasFlag(SchedulerOptions.HasTeamRows);
				var hasPosition = table.Options.HasFlag(SchedulerOptions.HasPositionColumn);
				var hasNumber = table.Options.HasFlag(SchedulerOptions.HasNumberColumn);
				if (hasIndex)
				{
					preTableColumns++;
					preWorkerColumns += ';';
					columnNames.Append("№;");
				}
				if (hasTeam)
				{
					preTableColumns++;
					preWorkerColumns += ';';
					columnNames.Append("СМ;");
				}
				columnNames.Append(hasNumber ? "ФИО" : "Ф.И.О., таб. №");
				if (hasPosition)
				{
					preTableColumns++;
					columnNames.Append(";Должность");
				}
				if (hasNumber)
				{
					preTableColumns++;
					columnNames.Append(";Таб. №");
				}
				var colCount = table.Table.GetLength(1);
				var dt = table.StartDate;
				file.Write(preWorkerColumns);
				file.WriteLine("{0:dd.MM.yyyy} - {1:dd.MM.yyyy}\n", table.PeriodFirstDay, table.PeriodLastDay);

				for (int j = 0; j < preTableColumns - 1; j++)
				{
					file.Write(';');
				}
				for (int j = 0; j < colCount; j++)
				{
					file.Write(string.Format(";{0:ddd}", dt));
					dt = dt.AddDays(1);
				}
				file.WriteLine();

				dt = table.StartDate;
				file.Write(columnNames.ToString());
				for (int j = 0; j < colCount; j++)
				{
					file.Write(';');
					file.Write(dt.Day);
					dt = dt.AddDays(1);
				}
				file.WriteLine(";Итого, ч;Ночные");

				Team lastTeam = null;
				for (int i = 0; i < table.Table.GetLength(0); i++)
				{
					if (hasTeamRows && table.Teams[i] != lastTeam)
					{
						if (lastTeam != null)
						{
							file.WriteLine();
						}
						lastTeam = table.Teams[i];
						file.Write(preWorkerColumns);
						file.WriteLine(lastTeam.Name);
					}
					if (hasIndex) file.Write(string.Format("{0};", i + 1));
					if (hasTeam) file.Write(string.Format("{0};", table.Teams[i].Name));
					var w = table.Workers[i];
					file.Write(hasNumber ? w.GetShortName() : string.Format("{0} таб. №{1}", w.GetShortName(), w.Number));
					if (hasPosition) file.Write(string.Format(";{0}", table.Workers[i].Position));
					if (hasNumber) file.Write(string.Format(";{0}", table.Workers[i].Number));
					for (int j = 0; j < colCount; j++)
					{
						var val = table.Table[i, j];
						file.Write(string.Format(";{0}", val > 0 ? val.ToString("F") : 
							val == SchedulerBase.WorkerFreeDay ? "" : 
							val == SchedulerBase.WorkerVacation ? "О" : 
							val == SchedulerBase.WorkerDayOff ?"B" : "П"));
					}
					file.WriteLine(";{0:F};{1:F}", table.FullHours[i], table.NightHours[i]);
				}
			}
		}

		public void ExportTable(int[,] table)
		{
			using (var file = new StreamWriter(_path))
			{
				var colCount = table.GetLength(1);
				for (int i = 0; i < table.GetLength(0); i++)
				{
					for (int j = 0; j < colCount; j++)
					{
						file.Write(table[i, j]);
						if (j < colCount - 1)
						{
							file.Write(';');
						}
					}
					file.WriteLine();
				}
			}
		}
	}
}
