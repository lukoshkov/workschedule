﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSchedule.Export
{
	public interface IExporter
	{
		void ExportTable(int[,] table);
		void Export(Timetable table);
		void Export(IEnumerable<Timetable> tables);
	}
}
