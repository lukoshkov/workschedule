﻿using System;
using System.Collections.Generic;
using WorkSchedule.Schedulers;

namespace WorkSchedule
{
	public class Department
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public long SchedulerID { get; set; }
		public DateTime BaseDate { get; set; }
		public List<Team> Teams { get; private set; }

		public IScheduler Scheduler
		{
			get
			{
				return SchedulerFactory.CreateSchedulerByID(SchedulerID);
			}
		}

		public Department(string name, long schedulerID, DateTime baseDate)
		{
			Name = name;
			SchedulerID = schedulerID;
			BaseDate = baseDate;
			Teams = new List<Team>();
		}

		public List<Worker> GetWorkers()
		{
			var workers = new List<Worker>();
			foreach (var team in Teams)
			{
				workers.AddRange(team.Workers);
			}
			return workers;
		}

		public Timetable CreateWorkersTimetable(DateTime calcStart, DateTime calcEnd, SchedulerOptions options, List<Vacation> holidays)
		{
			var scheduler = Scheduler;
			return scheduler == null ? null : Scheduler.CreateWorkersTimetable(this, calcStart, calcEnd, options, holidays);
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
