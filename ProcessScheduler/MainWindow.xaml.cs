﻿using ProcessScheduler.Commands;
using ProcessScheduler.Events;
using ProcessScheduler.Helpers;
using ProcessScheduler.Models;
using ProcessScheduler.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace ProcessScheduler
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private ListViewController<TimelineViewModel> _timelinesListController;
		private ListViewController<StepViewModel> _stepsListController;
		private List<ProcessTimeline> _timelines;
		private List<ProcessStep> _steps;
		private Version _version = System.Reflection.Assembly.GetCallingAssembly().GetName().Version;

		private Models.Editing.EditorHistory _history = new Models.Editing.EditorHistory();

		#region Properties

		public bool IsTimelinesListChanged { get; private set; }
		public PropertyNotifier<TimeSpan> TotalDuration { get; private set; } = new PropertyNotifier<TimeSpan>();

		public ListViewController<TimelineViewModel> TimelinesListController
		{
			get
			{
				if (_timelinesListController == null)
				{
					_timelinesListController = new ListViewController<TimelineViewModel>(null);
				}
				return _timelinesListController;
			}
		}

		public ListViewController<StepViewModel> StepsListController
		{
			get
			{
				if (_stepsListController == null)
				{
					_stepsListController = new ListViewController<StepViewModel>(null, false, false);
				}
				return _stepsListController;
			}
		}
		
		public SchedulerSettingsViewModel SchedulerSettingsVM { get; set; } = new SchedulerSettingsViewModel();

		#endregion Properties

		#region Commands

		private ICommand _applyOrderCommand;
		private ICommand _resetOrderCommand;
		private ICommand _appendTimelinesCommand;
		private ICommand _insertTimelinesCommand;

		public ICommand ApplyOrderCommand
		{
			get
			{
				if (_applyOrderCommand == null)
				{
					_applyOrderCommand = new RelayCommand(o => ApplyTimelinesOrder(), o => IsTimelinesListChanged);
				}
				return _applyOrderCommand;
			}
		}

		public ICommand ResetOrderCommand
		{
			get
			{
				if (_resetOrderCommand == null)
				{
					_resetOrderCommand = new RelayCommand(o => UpdateTimelines(_timelines), o => IsTimelinesListChanged);
				}
				return _resetOrderCommand;
			}
		}

		public ICommand AppendTimelinesCommand
		{
			get
			{
				if (_appendTimelinesCommand == null)
				{
					_appendTimelinesCommand = new RelayCommand(o => AppendTimeline(), o => !TimelinesListController.IsEmpty);
				}
				return _appendTimelinesCommand;
			}
		}

		public ICommand InsertTimelinesCommand
		{
			get
			{
				if (_insertTimelinesCommand == null)
				{
					_insertTimelinesCommand = new RelayCommand(o => InsertTimeline(), o => !TimelinesListController.IsEmpty && TimelinesListController.SelectedIndex > -1);
				}
				return _insertTimelinesCommand;
			}
		}

		#endregion Commands

		public MainWindow()
		{
			InitializeComponent();
			_timelines = new List<ProcessTimeline>();
			//var csvFiles = System.IO.Directory.GetFiles("timelines", "*.csv");
			//var timelines = LoadTimelines(csvFiles);
			//UpdateTimelines(timelines);
			//CreateSchedule();
			TimelinesListController.PropertyChanged += TimelinesListController_PropertyChanged;
			////var timer = new System.Windows.Threading.DispatcherTimer();
			////timer.Interval = TimeSpan.FromSeconds(1);
			////timer.Tick += (s, e) =>
			////{
			////	if (_steps == null) return;
			////	Scheduler.Move(_steps, TimeSpan.FromMinutes(1));
			////	UpdateListViews();
			////};
			////timer.Start();
			//SchedulerSettingsVM.NextStepsStrategy = Scheduler.Strategy.UniformMove;
			//SchedulerSettingsVM.PrevStepsStrategy = Scheduler.Strategy.UniformMove;
			//SchedulerSettingsVM.NextTimelinesStrategy = Scheduler.Strategy.UniformMove;
			//SchedulerSettingsVM.PrevTimelinesStrategy = Scheduler.Strategy.UniformMove;
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);
			var dialog = MessageBox.Show("Сохранить график перед выходом?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
			switch (dialog)
			{
				case MessageBoxResult.Yes:
					e.Cancel = !SaveProjectDialog();
					break;
				case MessageBoxResult.No:
					break;
				default:
					e.Cancel = true;
					break;
			}
		}

		private List<ProcessTimeline> LoadTimelines(string[] csvFiles)
		{
			var timelines = new List<ProcessTimeline>();
			foreach (var csv in csvFiles)
			{
				ProcessTimeline tl;
				if (Models.Storing.TimelineCsvParser.TryParse(csv, out tl))
				{
					tl.Title = System.IO.Path.GetFileNameWithoutExtension(csv);
					timelines.Add(tl);
				}
			}
			return timelines;
		}

		private void UpdateTimelines(List<ProcessTimeline> timelines)
		{
			_timelines = timelines;
			_steps = _timelines.SelectMany(t => t.Steps).ToList();
			TimelinesListController.UpdateSource(_timelines.Select(t => new TimelineViewModel(t)));
			TimelinesListController.Source.CollectionChanged += OnTimelinesListChanged;
			IsTimelinesListChanged = false;
			TotalDuration.Value = GetTotalDuration();
		}

		private TimeSpan GetTotalDuration()
		{
			if (_timelines == null || _timelines.Count == 0)
			{
				return TimeSpan.Zero;
			}
			return _timelines[_timelines.Count - 1].EndTime - _timelines[0].StartTime;
		}

		private void CreateSchedule()
		{
			var start = DateTime.Now;
			Scheduler.CreateSchedule(_steps, start.AddSeconds(-start.Second));
			TotalDuration.Value = GetTotalDuration();
		}

		private void OnTimelinesListChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			IsTimelinesListChanged = IsTimelinesListChanged
				|| e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Move
				|| e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove;
		}

		private void ApplyTimelinesOrder()
		{
			int minCount = Math.Min(_timelines.Count, TimelinesListController.Source.Count);
			for (int i = 0; i < minCount; i++)
			{
				var timeline = TimelinesListController.Source[i].Timeline;
				if (!ReferenceEquals(_timelines[i], timeline))
				{
					var time = _timelines[i].StartTime;
					for (int j = i; j < TimelinesListController.Source.Count; j++)
					{
						timeline = TimelinesListController.Source[j].Timeline;
						var move = time - timeline.StartTime;
						Scheduler.Move(timeline.Steps, move);
						time = timeline.EndTime;
					}
					break;
				}
			}
			_timelines = TimelinesListController.Source.Select(vm => vm.Timeline).ToList();
			UpdateListViews();
			IsTimelinesListChanged = false;
			//UpdateTimelines(_timelines);
		}

		private void InsertTimeline()
		{
			var index = TimelinesListController.SelectedIndex;
			if (index > -1)
			{
				var timeline = TimelinesListController.Source[index];
				var timelines = LoadTimelinesDialog();
				if (timelines.Count == 0) return;
				if (IsTimelinesListChanged) ApplyTimelinesOrder();

				Scheduler.CreateSchedule(timelines, timeline.StartTime);
				var move = timelines[timelines.Count - 1].EndTime - timelines[0].StartTime;
				Scheduler.Move(_timelines, move, index, _timelines.Count);
				_timelines.InsertRange(index, timelines);
				UpdateTimelines(_timelines);
			}
		}

		private void AppendTimeline()
		{
			var timelines = LoadTimelinesDialog();
			if (timelines.Count == 0) return;
			if (IsTimelinesListChanged) ApplyTimelinesOrder();

			var dt = _timelines.Count > 0 ? _timelines[_timelines.Count - 1].EndTime : DateTime.Now;
			Scheduler.CreateSchedule(timelines, dt);
			_timelines.AddRange(timelines);
			UpdateTimelines(_timelines);
		}

		private void UpdateListViews()
		{
			var index = TimelinesListController.SelectedIndex;
			var timelines = TimelinesListController.Source;
			TimelinesListController.Source = null;
			TimelinesListController.Source = timelines;
			TimelinesListController.SelectedIndex = index;
			TotalDuration.Value = GetTotalDuration();
			return;

			StepsListController.ForEach(vm => vm.UpdateView());
			TimelinesListController.ForEach(vm => vm.UpdateView());
		}

		private void TimelinesListController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(ListViewController<ProcessTimeline>.SelectedIndex))
			{
				var index = TimelinesListController.SelectedIndex;
				if (index > -1)
				{
					var timelineVM = TimelinesListController.Source[index];
					var timeline = timelineVM.Timeline;
					var vmList = timeline.Steps.Select(s => new StepViewModel(s)).ToList();
					foreach (var item in vmList)
					{
						item.ChangesApplied += StepViewModel_ChangesApplied;
					}
					StepsListController.UpdateSource(vmList);
				}
				else
				{
					StepsListController.UpdateSource(null);
				}
			}
		}

		//private void StepViewModel_ChangesApplied(object sender, ProcessStepChangedEventArgs e)
		//{
		//	var svm = sender as StepViewModel;
		//	if (svm != null)
		//	{
		//		var step = svm.Step;
		//		var index = _steps.IndexOf(step);
		//		Scheduler.ResolveCollisions(_steps, index, _strategies[BackwardStrategyIndex], _strategies[ForwardStrategyIndex], e.OldValue);
		//		UpdateListViews();
		//	}
		//}

		private void StepViewModel_ChangesApplied(object sender, ProcessStepChangedEventArgs e)
		{
			if (IsTimelinesListChanged)
			{
				MessageBox.Show("Порядок испытаний был изменен. Необходимо сохранить либо восстановить порядок.", "", MessageBoxButton.OK, MessageBoxImage.Information);
				return;
			}
			var svm = sender as StepViewModel;
			if (svm != null)
			{
				var step = svm.Step;
				var timelineIndex = _timelines.FindIndex(t => t.Steps.Contains(step));
				var timeline = _timelines[timelineIndex];
				var steps = timeline.Steps;
				var stepIndex = steps.IndexOf(step);
				var lastStepIndex = steps.Count - 1;
				var oldStart = (stepIndex == 0 ? e.OldValue : steps[0]).StartTime;
				var oldEnd = (stepIndex == lastStepIndex ? e.OldValue : steps[lastStepIndex]).EndTime;

				var stepsNextStrat = SchedulerSettingsVM.NextStepsStrategy;
				var stepsPrevStrat = SchedulerSettingsVM.PrevStepsStrategy;
				var timelinesNextStrat = SchedulerSettingsVM.NextTimelinesStrategy;
				var timelinesPrevStrat = SchedulerSettingsVM.PrevTimelinesStrategy;
				Scheduler.ResolveCollisions(steps, stepIndex, stepsPrevStrat, stepsNextStrat, e.OldValue);
				Scheduler.ResolveCollisions(_timelines, timelineIndex, timelinesPrevStrat, timelinesNextStrat, oldStart, oldEnd);
				UpdateListViews();
			}
		}

		private List<ProcessTimeline> LoadTimelinesDialog()
		{
			var dialog = new Microsoft.Win32.OpenFileDialog();
			dialog.Filter = "CSV (с разделителями)|*.csv";
			dialog.CheckFileExists = true;
			dialog.Multiselect = true;
			if (dialog.ShowDialog() == true)
			{
				return LoadTimelines(dialog.FileNames);
			}
			return new List<ProcessTimeline>();
		}

		private bool SaveProjectDialog(bool inShortFormat = false)
		{
			var dialog = new Microsoft.Win32.SaveFileDialog();
			dialog.Filter = "CSV (с разделителями)|*.csv";
			if (dialog.ShowDialog() == true)
			{
				try
				{
					if (inShortFormat)
					{
						Models.Storing.TimelineCsvStorage.ExportShort(dialog.FileName, _timelines);
					}
					else
					{
						Models.Storing.TimelineCsvStorage.Export(dialog.FileName, _timelines);
					}
					return true;
				}
				catch (Exception ex)
				{
					MessageBox.Show(string.Format("Произошла ошибка при сохранении файла.\nДетали:\n{0}", ex.Message));
				}
			}
			return false;
		}

		private void MenuItemOpen_Click(object sender, RoutedEventArgs e)
		{
			var timelines = LoadTimelinesDialog();
			UpdateTimelines(timelines);
			CreateSchedule();
		}

		private void MenuItemOpenProject_Click(object sender, RoutedEventArgs e)
		{
			var dialog = new Microsoft.Win32.OpenFileDialog();
			dialog.Filter = "CSV (с разделителями)|*.csv";
			dialog.CheckFileExists = true;
			dialog.Multiselect = false;
			if (dialog.ShowDialog() == true)
			{
				var timelines = Models.Storing.TimelineCsvStorage.Load(dialog.FileName);
				UpdateTimelines(timelines);
			}
		}

		private void MenuItemSave_Click(object sender, RoutedEventArgs e)
		{
			SaveProjectDialog(true);
		}

		private void MenuItemSaveProject_Click(object sender, RoutedEventArgs e)
		{
			SaveProjectDialog();
		}

		private void MenuItemRecalculate_Click(object sender, RoutedEventArgs e)
		{
			var message = "Вы уверены что хотите пересчитать график относительно настоящего момента времени?\nВсе внесенные изменения будут утеряны!";
			var dialog = MessageBox.Show(message, "", MessageBoxButton.YesNo, MessageBoxImage.Question);
			if (dialog == MessageBoxResult.Yes)
			{
				CreateSchedule();
			}
		}

		private void MenuItemAbout_Click(object sender, RoutedEventArgs e)
		{
			var message = string.Format("Планировщик процессов.\nВерсия: {0}.{1}.{2}", _version.Major, _version.Minor, _version.Build);
			MessageBox.Show(message, "О программе", MessageBoxButton.OK, MessageBoxImage.Information);
		}
	}
}
