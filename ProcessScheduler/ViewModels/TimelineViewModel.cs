﻿using ProcessScheduler.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessScheduler.ViewModels
{
	public class TimelineViewModel : INotifyPropertyChanged
	{
		private ProcessTimeline _timeline;

		#region Properties

		public bool IsReadOnly
		{
			get { return true; }
		}

		public bool IsChanged
		{
			get { return false; }
		}

		public string Title
		{
			get { return _timeline.Title; }
		}

		public DateTime StartTime
		{
			get { return _timeline.StartTime; }
			set
			{
				//_timeline.StartTime = value;
				//NotifyTimePropertiesChanged();
			}
		}

		public DateTime EndTime
		{
			get { return _timeline.EndTime; }
			set
			{
				//_timeline.EndTime = value;
				//NotifyTimePropertiesChanged();
			}
		}

		public TimeSpan Duration
		{
			get { return _timeline.DurationSum; }
			set
			{
				//_timeline.Duration = value;
				//NotifyTimePropertiesChanged();
			}
		}

		public ProcessTimeline Timeline
		{
			get { return _timeline; }
			set
			{
				_timeline = value == null ? new ProcessTimeline("") : value;
				ResetExecute(null);
				NotifyPropertyChanged(nameof(Timeline));
			}
		}

		#endregion Properties

		public TimelineViewModel(ProcessTimeline timeline)
		{
			Timeline = timeline;
		}

		public void UpdateView()
		{
			ResetExecute(null);
		}

		private void ResetExecute(object obj)
		{
			NotifyPropertyChanged(nameof(Title));
			NotifyTimePropertiesChanged();
		}

		private void NotifyTimePropertiesChanged()
		{
			System.Diagnostics.Debug.WriteLine("{0}\t{1}\t{2}\t{3}", Title, StartTime, EndTime, Duration);
			NotifyPropertyChanged(nameof(StartTime));
			NotifyPropertyChanged(nameof(EndTime));
			NotifyPropertyChanged(nameof(Duration));
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
