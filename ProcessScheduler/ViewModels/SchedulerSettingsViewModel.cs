﻿using ProcessScheduler.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessScheduler.ViewModels
{
	public class SchedulerSettingsViewModel : INotifyPropertyChanged
	{
		private static readonly string[] _strategiesTitles = new string[]
		{
			"сдвигать до устранения конфликтов",
			"сдвигать равномерно",
			"сдвигать до совпадения границ"
		};

		private static readonly Scheduler.Strategy[] _strategies = new Scheduler.Strategy[]
		{
			Scheduler.Strategy.ResolveCollisions,
			Scheduler.Strategy.UniformMove,
			Scheduler.Strategy.BackToBack
		};

		private int _prevStepsStrategyIndex;
		private int _nextStepsStrategyIndex;
		private int _prevTimelinesStrategyIndex;
		private int _nextTimelinesStrategyIndex;

		public string[] SchedulerStrategies
		{
			get { return _strategiesTitles; }
		}

		public int PrevStepsStrategyIndex
		{
			get { return _prevStepsStrategyIndex; }
			set
			{
				_prevStepsStrategyIndex = value;
				NotifyPropertyChanged(nameof(PrevStepsStrategyIndex));
			}
		}

		public int NextStepsStrategyIndex
		{
			get { return _nextStepsStrategyIndex; }
			set
			{
				_nextStepsStrategyIndex = value;
				NotifyPropertyChanged(nameof(NextStepsStrategyIndex));
			}
		}

		public int PrevTimelinesStrategyIndex
		{
			get { return _prevTimelinesStrategyIndex; }
			set
			{
				_prevTimelinesStrategyIndex = value;
				NotifyPropertyChanged(nameof(PrevTimelinesStrategyIndex));
			}
		}

		public int NextTimelinesStrategyIndex
		{
			get { return _nextTimelinesStrategyIndex; }
			set
			{
				_nextTimelinesStrategyIndex = value;
				NotifyPropertyChanged(nameof(NextTimelinesStrategyIndex));
			}
		}

		public Scheduler.Strategy PrevStepsStrategy
		{
			get { return _strategies[_prevStepsStrategyIndex]; }
			set
			{
				PrevStepsStrategyIndex = Array.IndexOf(_strategies, value);
				NotifyPropertyChanged(nameof(PrevStepsStrategy));
			}
		}

		public Scheduler.Strategy NextStepsStrategy
		{
			get { return _strategies[_nextStepsStrategyIndex]; }
			set
			{
				NextStepsStrategyIndex = Array.IndexOf(_strategies, value);
				NotifyPropertyChanged(nameof(NextStepsStrategy));
			}
		}

		public Scheduler.Strategy PrevTimelinesStrategy
		{
			get { return _strategies[_prevTimelinesStrategyIndex]; }
			set
			{
				PrevTimelinesStrategyIndex = Array.IndexOf(_strategies, value);
				NotifyPropertyChanged(nameof(PrevTimelinesStrategy));
			}
		}

		public Scheduler.Strategy NextTimelinesStrategy
		{
			get { return _strategies[_nextTimelinesStrategyIndex]; }
			set
			{
				NextTimelinesStrategyIndex = Array.IndexOf(_strategies, value);
				NotifyPropertyChanged(nameof(NextTimelinesStrategy));
			}
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
