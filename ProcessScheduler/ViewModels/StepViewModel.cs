﻿using ProcessScheduler.Commands;
using ProcessScheduler.Events;
using ProcessScheduler.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ProcessScheduler.ViewModels
{
	public class StepViewModel : INotifyPropertyChanged
	{
		private bool _isChanged;
		private ProcessStep _stepBase;
		private ProcessStep _stepView;
		private ICommand _applyCommand;
		private ICommand _resetCommand;

		#region Properties

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool IsChanged
		{
			get { return _isChanged; }
			set
			{
				_isChanged = value;
				NotifyPropertyChanged(nameof(IsChanged));
			}
		}

		public string Title
		{
			get { return _stepView.Description; }
		}

		public DateTime StartTime
		{
			get { return _stepView.StartTime; }
			set
			{
				_stepView.StartTime = value;
				IsChanged = true;
				NotifyTimePropertiesChanged();
			}
		}

		public DateTime EndTime
		{
			get { return _stepView.EndTime; }
			set
			{
				_stepView.EndTime = value;
				IsChanged = true;
				NotifyTimePropertiesChanged();
			}
		}

		public TimeSpan Duration
		{
			get { return _stepView.Duration; }
			set
			{
				_stepView.Duration = value;
				IsChanged = true;
				NotifyTimePropertiesChanged();
			}
		}

		public ProcessStep Step
		{
			get { return _stepBase; }
			set
			{
				_stepBase = value == null ? new ProcessStep() : value;
				ResetExecute(null);
				NotifyPropertyChanged(nameof(Step));
			}
		}

		#endregion Properties

		#region Commands

		public ICommand ApplyCommand
		{
			get
			{
				if (_applyCommand == null)
				{
					_applyCommand = new RelayCommand(p => ApplyExecute(p));
				}
				return _applyCommand;
			}
		}

		public ICommand ResetCommand
		{
			get
			{
				if (_resetCommand == null)
				{
					_resetCommand = new RelayCommand(p => ResetExecute(p));
				}
				return _resetCommand;
			}
		}

		#endregion Commands

		public event EventHandler<ProcessStepChangedEventArgs> ChangesApplied;

		public StepViewModel(ProcessStep step)
		{
			Step = step;
		}

		public void UpdateView()
		{
			ResetExecute(null);
		}

		private void ApplyExecute(object obj)
		{
			var old = _stepBase.Copy();
			_stepBase.Duration = _stepView.Duration;
			_stepBase.Description = _stepView.Description;
			_stepBase.StartTime = _stepView.StartTime;
			_stepBase.EndTime = _stepView.EndTime;
			IsChanged = false;
			ChangesApplied?.Invoke(this, new ProcessStepChangedEventArgs(old, _stepBase));
		}

		private void ResetExecute(object obj)
		{
			_stepView = _stepBase.Copy();
			IsChanged = false;
			NotifyPropertyChanged(nameof(Title));
			NotifyTimePropertiesChanged();
		}

		private void NotifyTimePropertiesChanged()
		{
			NotifyPropertyChanged(nameof(StartTime));
			NotifyPropertyChanged(nameof(EndTime));
			NotifyPropertyChanged(nameof(Duration));
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
