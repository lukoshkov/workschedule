﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;

namespace ProcessScheduler
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		internal class NativeMethods
		{
			public const int HWND_BROADCAST = 0xffff;
			public static readonly int WM_SHOWME = RegisterWindowMessage("WM_SHOWME");
			[DllImport("user32")]
			public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
			[DllImport("user32")]
			public static extern int RegisterWindowMessage(string message);
		}

		private Int32 m_Message;
		private Mutex m_Mutex;

		private IntPtr HandleMessages(IntPtr handle, int message, IntPtr wParameter, IntPtr lParameter, ref bool handled)
		{
			if (message == m_Message)
			{
				if (MainWindow.WindowState == WindowState.Minimized)
					MainWindow.WindowState = WindowState.Normal;

				var topmost = MainWindow.Topmost;

				MainWindow.Topmost = true;
				MainWindow.Topmost = topmost;
			}

			return IntPtr.Zero;
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			var assembly = Assembly.GetExecutingAssembly();
			var mutexName = string.Format(CultureInfo.InvariantCulture, "Local\\{{{0}}}{{{1}}}", assembly.GetType().GUID, assembly.GetName().Name);

			bool mutexCreated;
			m_Mutex = new Mutex(true, mutexName, out mutexCreated);
			m_Message = NativeMethods.RegisterWindowMessage(mutexName);

			if (!mutexCreated)
			{
				m_Mutex = null;
				NativeMethods.PostMessage((IntPtr)NativeMethods.HWND_BROADCAST, m_Message, IntPtr.Zero, IntPtr.Zero);
				Current.Shutdown();
				return;
			}

			base.OnStartup(e);

			MainWindow window = new MainWindow();
			MainWindow = window;
			window.Show();

			HwndSource.FromHwnd((new WindowInteropHelper(window)).Handle).AddHook(new HwndSourceHook(HandleMessages));
		}

		private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			var message = string.Format("Необработанное исключение: {0}\n\n{1}", e.Exception.Message, e.Exception.StackTrace);
			MessageBox.Show(message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
			try
			{
				var dir = "Errors";
				if (!System.IO.Directory.Exists(dir))
				{
					System.IO.Directory.CreateDirectory(dir);
				}
				var file = System.IO.Path.Combine(dir, string.Format("error-{0:yyyy-MM-dd-hh-mm-ss}.txt", DateTime.Now));
				using (var writer = new System.IO.StreamWriter(file))
				{
					writer.Write(message);
				}
			}
			catch (Exception)
			{
			}
			e.Handled = true;
		}
	}
}
