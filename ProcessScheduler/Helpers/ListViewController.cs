﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using ProcessScheduler.Commands;

namespace ProcessScheduler.Helpers
{
	public class ListViewController<T> : INotifyPropertyChanged
		where T : class
	{
		private ObservableCollection<T> _source;
		private int _selectedIndex;
		private ICommand _moveUpCommand;
		private ICommand _moveDownCommand;
		private ICommand _removeCommand;

		#region Properties

		public bool AllowMoveItems { get; set; }
		public bool AllowRemoveItems { get; set; }

		public bool IsEmpty
		{
			get { return Source != null && Source.Count == 0; }
		}

		public ObservableCollection<T> Source
		{
			get { return _source; }
			set
			{
				_source = value;
				NotifyPropertyChanged(nameof(Source));
				NotifyPropertyChanged(nameof(IsEmpty));
			}
		}

		public int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				_selectedIndex = value;
				NotifyPropertyChanged(nameof(SelectedIndex));
			}
		}

		public ICommand MoveUpCommand
		{
			get
			{
				if (_moveUpCommand == null)
				{
					_moveUpCommand = new RelayCommand(p => MoveUpExecute(p), p => AllowMoveItems);
				}
				return _moveUpCommand;
			}
		}

		public ICommand MoveDownCommand
		{
			get
			{
				if (_moveDownCommand == null)
				{
					_moveDownCommand = new RelayCommand(p => MoveDownExecute(p), p => AllowMoveItems);
				}
				return _moveDownCommand;
			}
		}

		public ICommand RemoveCommand
		{
			get
			{
				if (_removeCommand == null)
				{
					_removeCommand = new RelayCommand(p => RemoveExecute(p), p => AllowRemoveItems);
				}
				return _removeCommand;
			}
		}

		#endregion Properties

		public ListViewController(IEnumerable<T> source, bool allowMoveItems = true, bool allowRemoveItems = true)
		{
			AllowMoveItems = allowMoveItems;
			AllowRemoveItems = allowRemoveItems;
			_selectedIndex = -1;
			UpdateSource(source);
		}

		public void ForEach(Action<T> action)
		{
			if (action != null && _source != null)
			{
				foreach (var item in _source)
				{
					action(item);
				}
			}
		}

		public void UpdateSource(IEnumerable<T> source)
		{
			if (source == null)
			{
				Source = new ObservableCollection<T>();
			}
			else
			{
				Source = new ObservableCollection<T>(source);
			}
		}

		private void MoveUpExecute(object param)
		{
			var index = Source.IndexOf(param as T);
			var length = Source.Count;
			Source.Move(index, (index - 1 + length) % length);
		}

		private void MoveDownExecute(object param)
		{
			var index = Source.IndexOf(param as T);
			var length = Source.Count;
			Source.Move(index, (index + 1) % length);
		}

		private void RemoveExecute(object param)
		{
			Source.Remove(param as T);
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
