﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessScheduler.Helpers
{
	public class PropertyNotifier<T> : INotifyPropertyChanged
	{
		private T _value;

		public T Value
		{
			get { return _value; }
			set
			{
				if (EqualityComparer<T>.Default.Equals(_value, value) == false)
				{
					_value = value;
					NotifyPropertyChanged(nameof(Value));
				}
			}
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
