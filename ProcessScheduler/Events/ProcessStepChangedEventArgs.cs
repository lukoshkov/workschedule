﻿using ProcessScheduler.Models;
using System;

namespace ProcessScheduler.Events
{
	public class ProcessStepChangedEventArgs : EventArgs
	{
		public ProcessStep OldValue { get; protected set; }
		public ProcessStep NewValue { get; protected set; }

		public ProcessStepChangedEventArgs(ProcessStep oldValue, ProcessStep newValue)
		{
			OldValue = oldValue;
			NewValue = newValue;
		}
	}
}
