﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessScheduler.Models
{
	public class ProcessStep
	{
		private TimeSpan _duration;
		private DateTime _startTime;
		private DateTime _endTime;

		public TimeSpan CheckInterval { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }

		public TimeSpan Duration
		{
			get { return _duration; }
			set
			{
				_duration = value;
				_endTime = _startTime.Add(Duration);
			}
		}

		public DateTime StartTime
		{
			get { return _startTime; }
			set
			{
				_startTime = value;
				_endTime = _startTime.Add(Duration);
			}
		}

		public DateTime EndTime
		{
			get { return _endTime; }
			set
			{
				_endTime = value;
				_startTime = _endTime.Subtract(Duration);
			}
		}

		public ProcessStep()
		{

		}

		public ProcessStep(TimeSpan duration, string title, string description)
			: this(duration, title, description, TimeSpan.Zero)
		{ }

		public ProcessStep(TimeSpan duration, string title, string description, TimeSpan checkInterval)
		{
			Duration = duration;
			Title = title;
			Description = description;
			CheckInterval = checkInterval;
		}

		public ProcessStep Copy()
		{
			var copy = new ProcessStep(Duration, Title, Description, CheckInterval);
			copy.StartTime = StartTime;
			return copy;
		}

		public override string ToString()
		{
			return string.Format("{0} - {1} ({2})", Duration.ToString(), Title, Description);
			//return string.Format("{0} - {1} ({2})", TimeConverter.ToString(Duration), Title, Description);
		}
	}
}
