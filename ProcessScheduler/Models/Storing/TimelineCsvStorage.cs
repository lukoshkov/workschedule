﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessScheduler.Models.Storing
{
	public static class TimelineCsvStorage
	{
		private static readonly string[] Separators = new string[] { " - ", "  " };

		public static List<ProcessTimeline> Load(string path)
		{
			var lines = TimelineCsvParser.GetCsvFields(path);
			var timelines = new List<ProcessTimeline>();
			ProcessTimeline timeline = null;
			TimeSpan duration;
			DateTime start, end;

			foreach (var fields in lines)
			{
				if (!string.IsNullOrEmpty(fields[0]))
				{
					if (timeline != null)
					{
						timelines.Add(timeline);
					}
					if (TryParse(fields[1], out duration, out start, out end))
					{
						timeline = new ProcessTimeline(fields[0]);
						//timeline.Duration = duration;
					}
				}
				else
				{
					if (timeline != null)
					{
						if (TryParse(fields[3], out duration, out start, out end))
						{
							var step = new ProcessStep(duration, "", fields[2], duration);
							step.StartTime = start;
							timeline.Steps.Add(step);
						}
					}
				}
			}
			if (timeline != null)
			{
				timelines.Add(timeline);
			}

			return timelines;
		}

		public static List<ProcessTimeline> LoadOld(string path)
		{
			var lines = TimelineCsvParser.GetCsvFields(path);
			var timelines = new List<ProcessTimeline>();
			ProcessTimeline timeline = null;
			TimeSpan duration;
			DateTime start, end;

			foreach (var fields in lines)
			{
				if (!string.IsNullOrEmpty(fields[0]))
				{
					if (timeline != null)
					{
						timelines.Add(timeline);
					}
					if (TimeSpan.TryParse(fields[1], out duration) && DateTime.TryParse(fields[2], out start) && DateTime.TryParse(fields[3], out end))
					{
						timeline = new ProcessTimeline(fields[0]);
						//timeline.Duration = duration;
					}
				}
				else
				{
					if (timeline != null)
					{
						if (TimeSpan.TryParse(fields[5], out duration) && DateTime.TryParse(fields[6], out start) && DateTime.TryParse(fields[7], out end))
						{
							var step = new ProcessStep(duration, "", fields[4], duration);
							step.StartTime = start;
							timeline.Steps.Add(step);
						}
					}
				}
			}
			if (timeline != null)
			{
				timelines.Add(timeline);
			}

			return timelines;
		}

		public static void Export(string path, List<ProcessTimeline> timelines)
		{
			using (var writer = new StreamWriter(path, false, TimelineCsvParser.ExcelCsvEncoding))
			{
				foreach (var timeline in timelines)
				{
					writer.WriteLine("\"{0}\";{1}", timeline.Title, ToString(timeline.Duration, timeline.StartTime, timeline.EndTime));
					foreach (var step in timeline.Steps)
					{
						writer.WriteLine(";;\"{0}\";{1}", step.Description, ToString(step.Duration, step.StartTime, step.EndTime));
					}
				}
			}
		}

		public static void ExportOld(string path, List<ProcessTimeline> timelines)
		{
			using (var writer = new StreamWriter(path, false, TimelineCsvParser.ExcelCsvEncoding))
			{
				foreach (var timeline in timelines)
				{
					writer.WriteLine("\"{0}\";{1};{2};{3}", timeline.Title, timeline.Duration, timeline.StartTime, timeline.EndTime);
					foreach (var step in timeline.Steps)
					{
						writer.WriteLine(";;;;\"{0}\";{1};{2};{3}", step.Description, step.Duration, step.StartTime, step.EndTime);
					}
				}
			}
		}

		public static void ExportShort(string path, List<ProcessTimeline> timelines)
		{
			using (var writer = new StreamWriter(path, false, TimelineCsvParser.ExcelCsvEncoding))
			{
				foreach (var timeline in timelines)
				{
					var timeBuilder = new StringBuilder();
					timeBuilder.AppendFormat("{0:dd.MM.yyyy} г.", timeline.StartTime);
					if (timeline.StartTime.Date != timeline.EndTime.Date)
					{
						timeBuilder.AppendFormat(" - {0:dd.MM.yyyy} г.", timeline.EndTime);
					}
					writer.WriteLine("\"{0}\";{1}", timeline.Title, timeBuilder.ToString());
				}
			}
		}

		private static bool TryParse(string value, out TimeSpan duration, out DateTime start, out DateTime end)
		{
			duration = TimeSpan.Zero;
			start = DateTime.Now;
			end = start;
			try
			{
				var index0 = value.IndexOf(Separators[0]);
				var index1 = index0 + Separators[0].Length;
				var index2 = value.IndexOf(Separators[1]);

				var sStr = value.Substring(0, index0);
				var eStr = value.Substring(index1, index2 - index1);
				var dStr = value.Substring(index2 + Separators[1].Length);

				return TimeSpan.TryParse(dStr, out duration)
					&& DateTime.TryParse(sStr, out start)
					&& DateTime.TryParse(eStr, out end);
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private static string ToString(TimeSpan duration, DateTime start, DateTime end)
		{
			return string.Format("{0:HH:mm ddd dd.MM.yyyy}{1}{2:HH:mm ddd dd.MM.yyyy}{3}{4}", start, Separators[0], end, Separators[1], duration);
		}
	}
}
