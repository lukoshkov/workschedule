﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessScheduler.Models
{
	public class ProcessTimeline
	{
		public Guid Guid { get; set; }
		public string Title { get; set; }
		public List<ProcessStep> Steps { get; private set; }

		public TimeSpan DurationSum
		{
			get
			{
				var sum = TimeSpan.Zero;
				foreach (var item in Steps)
				{
					sum += item.Duration;
				}
				return sum;
			}
		}

		public TimeSpan Duration
		{
			get
			{
				return EndTime - StartTime;
			}
		}

		public DateTime StartTime
		{
			get
			{
				if (Steps.Count == 0) return DateTime.MinValue;
				return Steps[0].StartTime;
			}
		}

		public DateTime EndTime
		{
			get
			{
				if (Steps.Count == 0) return DateTime.MinValue;
				return Steps[Steps.Count - 1].EndTime;
			}
		}

		public ProcessTimeline(string title)
			: this(title, Guid.NewGuid())
		{
		}

		public ProcessTimeline(string title, Guid guid)
		{
			Title = title;
			Guid = guid;
			Steps = new List<ProcessStep>();
		}

		public override string ToString()
		{
			return Title;
		}
	}
}
