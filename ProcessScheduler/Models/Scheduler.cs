﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessScheduler.Models
{
	public static class Scheduler
	{
		public enum Strategy
		{
			ResolveCollisions,
			UniformMove,
			BackToBack
		}

		#region TimelineScheduling

		public static void CreateSchedule(List<ProcessTimeline> timelines, DateTime startTime)
		{
			var dt = startTime;
			for (int i = 0; i < timelines.Count; i++)
			{
				var timeline = timelines[i];
				var steps = timeline.Steps;
				CreateSchedule(steps, dt);
				dt = timeline.EndTime;
			}
		}

		public static void ResolveCollisions(List<ProcessTimeline> timelines, int startIndex, Strategy prevStrat, Strategy nextStrat, DateTime oldStart, DateTime oldEnd)
		{
			if (startIndex > 0)
			{
				var prevIndex = startIndex - 1;
				switch (prevStrat)
				{
					case Strategy.UniformMove:
						var move = timelines[startIndex].StartTime - oldStart;
						Move(timelines, move, 0, startIndex);
						break;
					case Strategy.BackToBack:
						ResolveCollisionsBackward(timelines, startIndex, true);
						break;
					case Strategy.ResolveCollisions:
					default:
						ResolveCollisionsBackward(timelines, startIndex, false);
						break;
				}
			}
			if (startIndex < timelines.Count - 1)
			{
				var nextIndex = startIndex + 1;
				switch (nextStrat)
				{
					case Strategy.UniformMove:
						var move = timelines[startIndex].EndTime - oldEnd;
						Move(timelines, move, nextIndex, timelines.Count);
						break;
					case Strategy.BackToBack:
						ResolveCollisionsForward(timelines, startIndex, true);
						break;
					case Strategy.ResolveCollisions:
					default:
						ResolveCollisionsForward(timelines, startIndex, false);
						break;
				}
			}
		}

		public static void Move(List<ProcessTimeline> timelines, TimeSpan moveTime, int startIndex, int count)
		{
			var endIndex = Math.Min(timelines.Count, startIndex + count);
			for (int i = startIndex; i < endIndex; i++)
			{
				Move(timelines[i].Steps, moveTime);
			}
		}

		private static void ResolveCollisionsForward(List<ProcessTimeline> timelines, int startIndex, bool backToBack)
		{
			for (int i = startIndex + 1; i < timelines.Count; i++)
			{
				var prev = timelines[i - 1];
				var curr = timelines[i];
				var dTime = curr.StartTime - prev.EndTime;
				if (dTime < TimeSpan.Zero)
				{
					Move(curr.Steps, -dTime);
				}
				else if (backToBack)
				{
					Move(curr.Steps, -dTime);
				}
				else
				{
					break;
				}
			}
		}

		private static void ResolveCollisionsBackward(List<ProcessTimeline> timelines, int startIndex, bool backToBack)
		{
			for (int i = startIndex; i > 0; i--)
			{
				var next = timelines[i];
				var curr = timelines[i - 1];
				var dTime = next.StartTime - curr.EndTime;
				if (dTime < TimeSpan.Zero)
				{
					Move(curr.Steps, dTime);
				}
				else if (backToBack)
				{
					Move(curr.Steps, dTime);
				}
				else
				{
					break;
				}
			}
		}

		#endregion

		#region StepScheduling

		public static void ResolveCollisions(List<ProcessStep> steps, int startIndex, Strategy prevStrat, Strategy nextStrat, ProcessStep oldValue)
		{
			if (startIndex > 0)
			{
				var prevIndex = startIndex - 1;
				switch (prevStrat)
				{
					case Strategy.UniformMove:
						var move = steps[startIndex].StartTime - oldValue.StartTime;
						Move(steps, move, 0, startIndex);
						break;
					case Strategy.BackToBack:
						CreateScheduleBackward(steps, steps[startIndex].StartTime, prevIndex, startIndex);
						break;
					case Strategy.ResolveCollisions:
					default:
						ResolveCollisionsBackward(steps, startIndex);
						break;
				}
			}
			if (startIndex < steps.Count - 1)
			{
				var nextIndex = startIndex + 1;
				switch (nextStrat)
				{
					case Strategy.UniformMove:
						var move = steps[startIndex].EndTime - oldValue.EndTime;
						Move(steps, move, nextIndex, steps.Count);
						break;
					case Strategy.BackToBack:
						CreateSchedule(steps, steps[startIndex].EndTime, nextIndex, steps.Count);
						break;
					case Strategy.ResolveCollisions:
					default:
						ResolveCollisionsForward(steps, startIndex);
						break;
				}
			}
		}

		public static void CreateSchedule(List<ProcessStep> steps, DateTime startTime)
		{
			CreateSchedule(steps, startTime, 0, steps.Count);
		}

		public static void CreateSchedule(List<ProcessStep> steps, DateTime startTime, int startIndex, int count)
		{
			var dt = startTime;
			var endIndex = Math.Min(steps.Count, startIndex + count);
			for (int i = startIndex; i < endIndex; i++)
			{
				var step = steps[i];
				step.StartTime = dt;
				dt = step.EndTime;
			}
		}

		public static void CreateScheduleBackward(List<ProcessStep> steps, DateTime endTime, int startIndex, int count)
		{
			var dt = endTime;
			var endIndex = Math.Max(0, startIndex - count + 1);
			for (int i = startIndex; i >= endIndex; i--)
			{
				var step = steps[i];
				step.EndTime = dt;
				dt = step.StartTime;
			}
		}

		public static void Move(List<ProcessStep> steps, TimeSpan moveTime)
		{
			Move(steps, moveTime, 0, steps.Count);
		}

		public static void Move(List<ProcessStep> steps, TimeSpan moveTime, int startIndex, int count)
		{
			var endIndex = Math.Min(steps.Count, startIndex + count);
			for (int i = startIndex; i < endIndex; i++)
			{
				var step = steps[i];
				step.StartTime += moveTime;
			}
		}

		public static void ResolveCollisionsForward(List<ProcessStep> steps, int startIndex)
		{
			for (int i = startIndex + 1; i < steps.Count; i++)
			{
				var prev = steps[i - 1];
				var curr = steps[i];
				var dTime = curr.StartTime - prev.EndTime;
				if (dTime < TimeSpan.Zero)
				{
					curr.StartTime -= dTime;
				}
				else
				{
					break;
				}
			}
		}

		public static void ResolveCollisionsBackward(List<ProcessStep> steps, int startIndex)
		{
			for (int i = startIndex; i > 0; i--)
			{
				var next = steps[i];
				var curr = steps[i - 1];
				var dTime = next.StartTime - curr.EndTime;
				if (dTime < TimeSpan.Zero)
				{
					curr.StartTime += dTime;
				}
				else
				{
					break;
				}
			}
		}

		#endregion
	}
}
