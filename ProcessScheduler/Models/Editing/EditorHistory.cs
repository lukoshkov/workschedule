﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessScheduler.Models.Editing
{
	public class EditorHistory
	{
		private Stack<ProcessTimeline[]> _undoStack;
		private Stack<ProcessTimeline[]> _redoStack;

		public int UndoCount
		{
			get { return _undoStack.Count; }
		}

		public int RedoCount
		{
			get { return _redoStack.Count; }
		}

		public EditorHistory()
		{
			_undoStack = new Stack<ProcessTimeline[]>();
			_redoStack = new Stack<ProcessTimeline[]>();
		}

		public void Clear()
		{
			_undoStack.Clear();
			_redoStack.Clear();
		}

		public void StoreProjectState(List<ProcessTimeline> timelines)
		{
			_redoStack.Clear();
			var state = GetState(timelines);
			_undoStack.Push(state);
		}

		public void UndoProjectChanges(ref List<ProcessTimeline> timelines)
		{
			UndoOrRedoProjectChanges(ref timelines, _undoStack, _redoStack);
		}

		public void RedoProjectChanges(ref List<ProcessTimeline> timelines)
		{
			UndoOrRedoProjectChanges(ref timelines, _redoStack, _undoStack);
		}

		private void UndoOrRedoProjectChanges(ref List<ProcessTimeline> timelines, Stack<ProcessTimeline[]> firstStack, Stack<ProcessTimeline[]> secondStack)
		{
			if (firstStack.Count > 0)
			{
				var state = GetState(timelines);
				secondStack.Push(state);
				state = firstStack.Pop();
				RestoreTimelines(state, ref timelines);
			}
		}

		private ProcessTimeline[] GetState(List<ProcessTimeline> timelines)
		{
			var state = new ProcessTimeline[timelines.Count];

			for (int i = 0; i < timelines.Count; i++)
			{
				var timeline = timelines[i];
				var copy = new ProcessTimeline(timeline.Title, timeline.Guid);
				copy.Steps.AddRange(timeline.Steps.Select(s => s.Copy()));
				state[i] = copy;
			}

			return state;
		}

		private void RestoreTimelines(ProcessTimeline[] state, ref List<ProcessTimeline> timelines)
		{
			timelines = state.ToList();
		}
	}
}
