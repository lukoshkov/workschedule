﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessTimer.Helpers
{
	public delegate T Func<T>();
	public delegate TResult Func<in T, out TResult>(T obj);
}
