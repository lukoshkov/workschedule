﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ProcessTimer.Core;

namespace ProcessTimer.UserControls
{
	public partial class ProductEdit : UserControl
	{
		private Product _product;
		private TestCategory _category;
		private List<TestCategory> _categories;

		public Product Product
		{
			get
			{
				return _product;
			}
			set
			{
				_product = value == null ? new Product("") : value;
				UpdateFields();
			}
		}

		private TestCategory Category
		{
			get
			{
				return _category;
			}
			set
			{
				_category = value == null ? new TestCategory("") : value;
				tbCategoryTitle.Text = _category.Title;
			}
		}

		public ProductEdit()
		{
			InitializeComponent();
			Product = null;
			SetButtonsEnabled();
		}

		public Product GetNew()
		{
			_product = new Product("");
			ApplyChanges();
			return _product;
		}

		public void ApplyChanges()
		{
			_product.Title = tbProductTitle.Text;
			_product.TestCategories.Clear();
			_product.TestCategories.AddRange(_categories);
		}

		public void CancelChanges()
		{
			UpdateFields();
		}

		public void Clear()
		{
			Product = null;
		}

		private void buttonAdd_Click(object sender, EventArgs e)
		{
			var category = GetNewCategory();
			_categories.Add(category);
			UpdateCategories();
		}

		private void buttonEdit_Click(object sender, EventArgs e)
		{
			var category = listBoxCategories.SelectedItem as TestCategory;
			if (category != null)
			{
				ApplyCategoryChanges();
				UpdateCategories();
			}
		}

		private void buttonDelete_Click(object sender, EventArgs e)
		{
			var category = listBoxCategories.SelectedItem as TestCategory;
			if (category != null)
			{
				_categories.Remove(category);
				UpdateCategories();
			}
		}

		private void UpdateFields()
		{
			tbProductTitle.Text = _product.Title;
			_categories = new List<TestCategory>(_product.TestCategories);
			UpdateCategories();
		}

		private void UpdateCategories()
		{
			listBoxCategories.DataSource = null;
			listBoxCategories.DataSource = _categories;
			listBoxCategories.DisplayMember = nameof(TestCategory.Title);
			listBoxCategories.SelectedIndex = -1;
		}

		private void SetButtonsEnabled()
		{
			buttonEditCat.Enabled = listBoxCategories.SelectedIndex > -1;
			buttonDeleteCat.Enabled = buttonEditCat.Enabled;
		}

		private TestCategory GetNewCategory()
		{
			_category = new TestCategory("");
			ApplyCategoryChanges();
			return _category;
		}

		private void ApplyCategoryChanges()
		{
			_category.Title = tbCategoryTitle.Text;
		}

		private void listBoxCategories_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetButtonsEnabled();
			Category = listBoxCategories.SelectedItem as TestCategory;
		}
	}
}
