﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ProcessTimer.Core;

namespace ProcessTimer.UserControls
{
	public partial class TimelineEdit : UserControl
	{
		private ProcessTimeline _timeline;
		private List<ProcessStep> _steps;

		public ProcessTimeline Timeline
		{
			get
			{
				return _timeline;
			}

			set
			{
				_timeline = value == null ? new ProcessTimeline("") : value;
				UpdateFields();
			}
		}

		public TimelineEdit()
		{
			InitializeComponent();
			Timeline = null;
			SetButtonsEnabled();
		}

		public ProcessTimeline GetNew()
		{
			_timeline = new ProcessTimeline("");
			ApplyChanges();
			return _timeline;
		}

		public void ApplyChanges()
		{
			_timeline.Title = tbTitle.Text;
			_timeline.Steps.Clear();
			_timeline.Steps.AddRange(_steps);
		}

		public void CancelChanges()
		{
			UpdateFields();
		}

		public void Clear()
		{
			Timeline = null;
		}

		public void UpdateFields(ProcessTimeline timeline)
		{
			tbTitle.Text = timeline.Title;
			_steps = new List<ProcessStep>(timeline.Steps);
			UpdateSteps();
		}

		private void buttonAdd_Click(object sender, EventArgs e)
		{
			var step = stepEdit1.GetNew();
			_steps.Add(step);
			UpdateSteps();
		}

		private void buttonEdit_Click(object sender, EventArgs e)
		{
			var step = listBoxSteps.SelectedItem as ProcessStep;
			if (step != null)
			{
				stepEdit1.ApplyChanges();
				UpdateSteps();
			}
		}

		private void buttonDelete_Click(object sender, EventArgs e)
		{
			var step = listBoxSteps.SelectedItem as ProcessStep;
			if (step != null)
			{
				_steps.Remove(step);
				UpdateSteps();
			}
		}

		private void UpdateFields()
		{
			UpdateFields(_timeline);
		}

		private void UpdateSteps()
		{
			listBoxSteps.DataSource = null;
			listBoxSteps.DataSource = _steps;
			listBoxSteps.SelectedIndex = -1;
		}

		private void SetButtonsEnabled()
		{
			buttonEdit.Enabled = listBoxSteps.SelectedIndex > -1;
			buttonDelete.Enabled = buttonEdit.Enabled;
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetButtonsEnabled();
			stepEdit1.Step = listBoxSteps.SelectedItem as ProcessStep;
		}
	}
}
