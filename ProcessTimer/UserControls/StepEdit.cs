﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ProcessTimer.Core;

namespace ProcessTimer.UserControls
{
	public partial class StepEdit : UserControl
	{
		private ProcessStep _step;

		public ProcessStep Step
		{
			get
			{
				return _step;
			}
			set
			{
				_step = value == null ? new ProcessStep() : value;
				UpdateFields();
			}
		}

		public StepEdit()
		{
			InitializeComponent();
			Step = null;
		}

		public ProcessStep GetNew()
		{
			_step = new ProcessStep();
			ApplyChanges();
			return _step;
		}

		public void ApplyChanges()
		{
			_step.Title = tbTitle.Text;
			_step.Description = tbDescription.Text;
			_step.Duration = GetProcessLength(numericHours, dtMinutes);
			_step.CheckInterval = GetProcessLength(numericHoursPeriod, dtMinutesPeriod);
		}

		public void CancelChanges()
		{
			UpdateFields();
		}

		public void Clear()
		{
			Step = null;
		}

		private void numericHours_ValueChanged(object sender, EventArgs e)
		{
			UpdateDuration();
		}

		private void dtMinutes_ValueChanged(object sender, EventArgs e)
		{
			UpdateDuration();
		}

		private void UpdateFields()
		{
			tbTitle.Text = _step.Title;
			tbDescription.Text = _step.Description;
			SetDurationFields(_step.Duration, numericHours, dtMinutes);
			SetDurationFields(_step.CheckInterval, numericHoursPeriod, dtMinutesPeriod);
		}

		private void SetDurationFields(TimeSpan ts, NumericUpDown numericHours, DateTimePicker dtMinutes)
		{
			numericHours.Value = (int)ts.TotalHours;
			dtMinutes.Value = DateTime.Now.Date.AddMinutes(ts.Minutes);
		}

		private void UpdateDuration()
		{
			UpdateDuration(numericHours, dtMinutes, tbDuration);
			UpdateDuration(numericHoursPeriod, dtMinutesPeriod, tbDurationPeriod);
		}

		private void UpdateDuration(NumericUpDown numericHours, DateTimePicker dtMinutes, TextBox tbDuration)
		{
			var length = GetProcessLength(numericHours, dtMinutes);
			var sb = new StringBuilder();
			if (length.TotalDays >= 1)
			{
				sb.AppendFormat("{0} сут. ", (int)length.TotalDays);
			}
			sb.AppendFormat("{0} ч. {1} м.", length.Hours, length.Minutes);
			tbDuration.Text = sb.ToString();
		}

		private TimeSpan GetProcessLength(NumericUpDown numericHours, DateTimePicker dtMinutes)
		{
			return new TimeSpan((int)numericHours.Value, dtMinutes.Value.Minute, 0);
		}
	}
}
