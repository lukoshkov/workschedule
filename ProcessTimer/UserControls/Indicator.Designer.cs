﻿namespace ProcessTimer.UserControls
{
	partial class Indicator
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.borderPanel1 = new ProcessTimer.UserControls.BorderPanel();
			this.labelText = new System.Windows.Forms.Label();
			this.borderPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// borderPanel1
			// 
			this.borderPanel1.BorderColor = System.Drawing.Color.Black;
			this.borderPanel1.BorderWidth = 3;
			this.borderPanel1.Controls.Add(this.labelText);
			this.borderPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.borderPanel1.Location = new System.Drawing.Point(0, 0);
			this.borderPanel1.Name = "borderPanel1";
			this.borderPanel1.Size = new System.Drawing.Size(50, 50);
			this.borderPanel1.TabIndex = 0;
			this.borderPanel1.Click += new System.EventHandler(this.Control_Click);
			// 
			// labelText
			// 
			this.labelText.AutoSize = true;
			this.labelText.BackColor = System.Drawing.Color.Transparent;
			this.labelText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelText.Location = new System.Drawing.Point(5, 17);
			this.labelText.Margin = new System.Windows.Forms.Padding(0);
			this.labelText.Name = "labelText";
			this.labelText.Size = new System.Drawing.Size(39, 17);
			this.labelText.TabIndex = 0;
			this.labelText.Text = "Text";
			this.labelText.Click += new System.EventHandler(this.Control_Click);
			// 
			// Indicator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.borderPanel1);
			this.Name = "Indicator";
			this.Size = new System.Drawing.Size(50, 50);
			this.borderPanel1.ResumeLayout(false);
			this.borderPanel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private BorderPanel borderPanel1;
		private System.Windows.Forms.Label labelText;
	}
}
