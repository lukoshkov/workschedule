﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ProcessTimer.Helpers;
using ProcessTimer.UserControls.TimerViews;

namespace ProcessTimer.UserControls
{
	public partial class TimerView : UserControl, ITimerView
	{
		protected static readonly Color[] _colors = { Color.Gray, Color.SteelBlue, Color.Green, Color.Red, Color.Orange, Color.MediumSeaGreen };
		protected static readonly string[] _statusMsgs = { "", "", "Завершено", "Остановлено", "Требуется проверка", "Завершено" };

		protected TimerState _state = TimerState.None;
		protected Core.ProcessState _processState;
		protected int _number;

		public int Number
		{
			get { return _number; }
			set
			{
				_number = value;
				label1.Text = string.Format("№ {0}", _number);
			}
		}

		public TimerState State
		{
			get { return _state; }
			protected set
			{
				if (_state != value)
				{
					var oldState = _state;
					_state = value;
					labelStatus.Text = _statusMsgs[(int)_state];
					labelStatus.ForeColor = _colors[(int)_state];
					panelBorder.Invalidate();
					StateChanged?.Invoke(this, new TimerStateChangedEventArgs(oldState, _state));
				}
			}
		}

		public Core.ProcessState ProcessState
		{
			get { return _processState; }
			set
			{
				_processState = value;
				var notNull = _processState != null;
				panelTimer.Visible = notNull;
				label2.Visible = notNull;
				label3.Visible = notNull;
				buttonAdd.Enabled = !notNull;
				buttonRemove.Enabled = notNull;
				UpdateProgress();
			}
		}

		public Func<ITimerView, Core.ProcessState> GetState { get; set; }
		
		public event EventHandler<TimerStateChangedEventArgs> StateChanged;

		public TimerView()
		{
			InitializeComponent();
			ProcessState = ProcessState;
			//DoubleBuffered = true;
			typeof(Panel).InvokeMember("DoubleBuffered",
				System.Reflection.BindingFlags.SetProperty | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic,
				null, panelBorder, new object[] { true });
		}

		public static Color GetTimerStateColor(TimerState state)
		{
			var index = (int)state;
			index = index < 0 ? 0 : index >= _colors.Length ? 0 : index;
			return _colors[index];
		}

		public void AddProgress(long milliseconds)
		{
			progressBar1.Value = Math.Min(progressBar1.Value + 10, progressBar1.Maximum);
			//var progress = milliseconds / (double)_durationMillis;
			//progressBar1.Value = Math.Min((int)Math.Round(progressBar1.Maximum * progress), progressBar1.Maximum);
		}

		public void UpdateProgress()
		{
			if (ProcessState == null)
			{
				return;
			}
			dateTimePicker1.Value = ProcessState.StepStart;
			labelProgress.Text = string.Format("{0} / {1}    ( {2:f2}% )", 
				ToString(ProcessState.StepDuration), 
				ToString(ProcessState.CurrentStep.Duration), 
				ProcessState.StepProgress * 100);
			labelTitle.Text = string.Format("Процесс: {0}", ProcessState.Timeline.Title);
			labelStepNumber.Text = string.Format("Этап {0}/{1} : {2}", ProcessState.StepIndex + 1, ProcessState.StepsCount, ProcessState.CurrentStep.Title);
			textBoxDescription.Text = ProcessState.CurrentStep.Description;
			label2.Text = string.Format("{0} | {1}", ProcessState.Timeline.Product.Title, ProcessState.Timeline.Category.Title);
			label3.Text = ProcessState.ChamberTitle;

			if (State != TimerState.Checked)
			{
				State = GetTimerState();
			}
			progressBar1.Value = Math.Min((int)Math.Round(progressBar1.Maximum * ProcessState.StepProgress), progressBar1.Maximum);
			UpdateButtonStartText();
			UpdateButtonResetText();
		}

		protected TimerState GetTimerState()
		{
			if (ProcessState == null)
			{
				return TimerState.None;
			}
			if (ProcessState.IsStepFinished)
			{
				return TimerState.Finished;
			}
			if (ProcessState.IsPaused)
			{
				return TimerState.Paused;
			}
			if (ProcessState.IsCheckRequiered)
			{
				return TimerState.NeedChecking;
			}
			return TimerState.Running;
		}

		protected void buttonStart_Click(object sender, EventArgs e)
		{
			if (ProcessState != null)
			{
				if (!ProcessState.IsStepStarted)
				{
					ProcessState.StartStep();
				}
				else if (!ProcessState.IsStepFinished)
				{
					ProcessState.IsPaused = !ProcessState.IsPaused;
				}
				UpdateButtonStartText();
			}
		}

		protected void buttonReset_Click(object sender, EventArgs e)
		{
			if (ProcessState != null)
			{
				if (ProcessState.IsPaused)
				{
					ProcessState.ResetStep();
				}
				else if (ProcessState.IsStepFinished)
				{
					State = TimerState.Checked;
				}
				else if (State == TimerState.NeedChecking)
				{
					ProcessState.Check();
				}
				UpdateButtonResetText();
			}
		}

		protected void buttonNext_Click(object sender, EventArgs e)
		{
			if (ProcessState != null)
			{
				ProcessState.StepIndex++;
				State = GetTimerState();
			}
		}

		protected void buttonPrev_Click(object sender, EventArgs e)
		{
			if (ProcessState != null)
			{
				ProcessState.StepIndex--;
				State = GetTimerState();
			}
		}

		protected void UpdateButtonStartText()
		{
			if (ProcessState != null)
			{
				buttonStart.Text = !ProcessState.IsStepStarted ? "Начать"
					: ProcessState.IsPaused ? "Возобновить" : "Остановить";
			}
		}

		protected void UpdateButtonResetText()
		{
			if (ProcessState != null)
			{
				buttonReset.Text = ProcessState.IsPaused ? "Сбросить"
					: State == TimerState.Finished || State == TimerState.NeedChecking ? "Проверено" : "Проверено";
			}
		}

		protected static string ToString(TimeSpan ts)
		{
			var format = ts.Days > 0 ? "{0} дн. {1:00}:{2:00}:{3:00}" : "{1:00}:{2:00}:{3:00}";
			return string.Format(format, ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
		}

		private void panel1_Paint(object sender, PaintEventArgs e)
		{
			var color = _colors[(int)State];
			var width = 2;
			var style = ButtonBorderStyle.Solid;
			ControlPaint.DrawBorder(e.Graphics, panelBorder.ClientRectangle,
					   color, width, style, // left
					   color, width, style, // top
					   color, width, style, // right
					   color, width, style);// bottom
		}

		protected void buttonAdd_Click(object sender, EventArgs e)
		{
			State = TimerState.None;
			ProcessState = GetState?.Invoke(this);
		}

		protected void buttonRemove_Click(object sender, EventArgs e)
		{
			State = TimerState.None;
			ProcessState = null;
		}
	}
}
