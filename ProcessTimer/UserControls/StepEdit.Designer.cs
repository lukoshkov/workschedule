﻿namespace ProcessTimer.UserControls
{
	partial class StepEdit
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbTitle = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.numericHours = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.dtMinutes = new System.Windows.Forms.DateTimePicker();
			this.label4 = new System.Windows.Forms.Label();
			this.tbDuration = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tbDescription = new System.Windows.Forms.TextBox();
			this.tbDurationPeriod = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.dtMinutesPeriod = new System.Windows.Forms.DateTimePicker();
			this.label7 = new System.Windows.Forms.Label();
			this.numericHoursPeriod = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.numericHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericHoursPeriod)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 3);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Название";
			// 
			// tbTitle
			// 
			this.tbTitle.Location = new System.Drawing.Point(101, 0);
			this.tbTitle.Name = "tbTitle";
			this.tbTitle.Size = new System.Drawing.Size(265, 20);
			this.tbTitle.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Длительность";
			// 
			// numericHours
			// 
			this.numericHours.Location = new System.Drawing.Point(101, 26);
			this.numericHours.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.numericHours.Name = "numericHours";
			this.numericHours.Size = new System.Drawing.Size(60, 20);
			this.numericHours.TabIndex = 3;
			this.numericHours.ValueChanged += new System.EventHandler(this.numericHours_ValueChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(167, 28);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(15, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "ч.";
			// 
			// dtMinutes
			// 
			this.dtMinutes.CustomFormat = "mm";
			this.dtMinutes.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtMinutes.Location = new System.Drawing.Point(188, 26);
			this.dtMinutes.Name = "dtMinutes";
			this.dtMinutes.ShowUpDown = true;
			this.dtMinutes.Size = new System.Drawing.Size(36, 20);
			this.dtMinutes.TabIndex = 5;
			this.dtMinutes.Value = new System.DateTime(2020, 2, 5, 0, 0, 0, 0);
			this.dtMinutes.ValueChanged += new System.EventHandler(this.dtMinutes_ValueChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(230, 28);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(18, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "м.";
			// 
			// tbDuration
			// 
			this.tbDuration.BackColor = System.Drawing.SystemColors.Window;
			this.tbDuration.Location = new System.Drawing.Point(263, 25);
			this.tbDuration.Name = "tbDuration";
			this.tbDuration.ReadOnly = true;
			this.tbDuration.Size = new System.Drawing.Size(103, 20);
			this.tbDuration.TabIndex = 7;
			this.tbDuration.TabStop = false;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(4, 81);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(57, 13);
			this.label5.TabIndex = 8;
			this.label5.Text = "Описание";
			// 
			// tbDescription
			// 
			this.tbDescription.Location = new System.Drawing.Point(101, 81);
			this.tbDescription.Multiline = true;
			this.tbDescription.Name = "tbDescription";
			this.tbDescription.Size = new System.Drawing.Size(265, 69);
			this.tbDescription.TabIndex = 9;
			// 
			// tbDurationPeriod
			// 
			this.tbDurationPeriod.BackColor = System.Drawing.SystemColors.Window;
			this.tbDurationPeriod.Location = new System.Drawing.Point(263, 52);
			this.tbDurationPeriod.Name = "tbDurationPeriod";
			this.tbDurationPeriod.ReadOnly = true;
			this.tbDurationPeriod.Size = new System.Drawing.Size(103, 20);
			this.tbDurationPeriod.TabIndex = 15;
			this.tbDurationPeriod.TabStop = false;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(230, 55);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(18, 13);
			this.label6.TabIndex = 14;
			this.label6.Text = "м.";
			// 
			// dtMinutesPeriod
			// 
			this.dtMinutesPeriod.CustomFormat = "mm";
			this.dtMinutesPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtMinutesPeriod.Location = new System.Drawing.Point(188, 53);
			this.dtMinutesPeriod.Name = "dtMinutesPeriod";
			this.dtMinutesPeriod.ShowUpDown = true;
			this.dtMinutesPeriod.Size = new System.Drawing.Size(36, 20);
			this.dtMinutesPeriod.TabIndex = 13;
			this.dtMinutesPeriod.Value = new System.DateTime(2020, 2, 5, 0, 0, 0, 0);
			this.dtMinutesPeriod.ValueChanged += new System.EventHandler(this.dtMinutes_ValueChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(167, 55);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(15, 13);
			this.label7.TabIndex = 12;
			this.label7.Text = "ч.";
			// 
			// numericHoursPeriod
			// 
			this.numericHoursPeriod.Location = new System.Drawing.Point(101, 53);
			this.numericHoursPeriod.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.numericHoursPeriod.Name = "numericHoursPeriod";
			this.numericHoursPeriod.Size = new System.Drawing.Size(60, 20);
			this.numericHoursPeriod.TabIndex = 11;
			this.numericHoursPeriod.ValueChanged += new System.EventHandler(this.numericHours_ValueChanged);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(3, 55);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(96, 13);
			this.label8.TabIndex = 10;
			this.label8.Text = "Период проверки";
			// 
			// StepEdit
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tbDurationPeriod);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.dtMinutesPeriod);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.numericHoursPeriod);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.tbDescription);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.tbDuration);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.dtMinutes);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.numericHours);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tbTitle);
			this.Controls.Add(this.label1);
			this.Name = "StepEdit";
			this.Size = new System.Drawing.Size(368, 153);
			((System.ComponentModel.ISupportInitialize)(this.numericHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericHoursPeriod)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbTitle;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numericHours;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DateTimePicker dtMinutes;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbDuration;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbDescription;
		private System.Windows.Forms.TextBox tbDurationPeriod;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.DateTimePicker dtMinutesPeriod;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown numericHoursPeriod;
		private System.Windows.Forms.Label label8;
	}
}
