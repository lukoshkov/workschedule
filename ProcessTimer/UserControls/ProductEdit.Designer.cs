﻿namespace ProcessTimer.UserControls
{
	partial class ProductEdit
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.tbProductTitle = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.listBoxCategories = new System.Windows.Forms.ListBox();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.buttonAddCat = new System.Windows.Forms.Button();
			this.buttonEditCat = new System.Windows.Forms.Button();
			this.buttonDeleteCat = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.tbCategoryTitle = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tbProductTitle, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(374, 330);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(128, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Наименование изделия";
			// 
			// tbProductTitle
			// 
			this.tbProductTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tbProductTitle.Location = new System.Drawing.Point(3, 16);
			this.tbProductTitle.Name = "tbProductTitle";
			this.tbProductTitle.Size = new System.Drawing.Size(368, 20);
			this.tbProductTitle.TabIndex = 1;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tableLayoutPanel2);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 42);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(368, 285);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Категории испытаний";
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.listBoxCategories, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.tbCategoryTitle, 1, 2);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 3;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.Size = new System.Drawing.Size(362, 266);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// listBoxCategories
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.listBoxCategories, 2);
			this.listBoxCategories.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBoxCategories.FormattingEnabled = true;
			this.listBoxCategories.Location = new System.Drawing.Point(3, 3);
			this.listBoxCategories.Name = "listBoxCategories";
			this.listBoxCategories.Size = new System.Drawing.Size(356, 199);
			this.listBoxCategories.TabIndex = 0;
			this.listBoxCategories.SelectedIndexChanged += new System.EventHandler(this.listBoxCategories_SelectedIndexChanged);
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.flowLayoutPanel1.AutoSize = true;
			this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel1, 2);
			this.flowLayoutPanel1.Controls.Add(this.buttonAddCat);
			this.flowLayoutPanel1.Controls.Add(this.buttonEditCat);
			this.flowLayoutPanel1.Controls.Add(this.buttonDeleteCat);
			this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 208);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(356, 29);
			this.flowLayoutPanel1.TabIndex = 1;
			// 
			// buttonAddCat
			// 
			this.buttonAddCat.Location = new System.Drawing.Point(3, 3);
			this.buttonAddCat.Name = "buttonAddCat";
			this.buttonAddCat.Size = new System.Drawing.Size(75, 23);
			this.buttonAddCat.TabIndex = 0;
			this.buttonAddCat.Text = "Добавить";
			this.buttonAddCat.UseVisualStyleBackColor = true;
			this.buttonAddCat.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// buttonEditCat
			// 
			this.buttonEditCat.Location = new System.Drawing.Point(84, 3);
			this.buttonEditCat.Name = "buttonEditCat";
			this.buttonEditCat.Size = new System.Drawing.Size(75, 23);
			this.buttonEditCat.TabIndex = 1;
			this.buttonEditCat.Text = "Изменить";
			this.buttonEditCat.UseVisualStyleBackColor = true;
			this.buttonEditCat.Click += new System.EventHandler(this.buttonEdit_Click);
			// 
			// buttonDeleteCat
			// 
			this.buttonDeleteCat.Location = new System.Drawing.Point(165, 3);
			this.buttonDeleteCat.Name = "buttonDeleteCat";
			this.buttonDeleteCat.Size = new System.Drawing.Size(75, 23);
			this.buttonDeleteCat.TabIndex = 2;
			this.buttonDeleteCat.Text = "Удалить";
			this.buttonDeleteCat.UseVisualStyleBackColor = true;
			this.buttonDeleteCat.Click += new System.EventHandler(this.buttonDelete_Click);
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 246);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Название категории";
			// 
			// tbCategoryTitle
			// 
			this.tbCategoryTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tbCategoryTitle.Location = new System.Drawing.Point(121, 243);
			this.tbCategoryTitle.Name = "tbCategoryTitle";
			this.tbCategoryTitle.Size = new System.Drawing.Size(238, 20);
			this.tbCategoryTitle.TabIndex = 3;
			// 
			// ProductEdit
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "ProductEdit";
			this.Size = new System.Drawing.Size(374, 330);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbProductTitle;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.ListBox listBoxCategories;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Button buttonAddCat;
		private System.Windows.Forms.Button buttonEditCat;
		private System.Windows.Forms.Button buttonDeleteCat;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbCategoryTitle;
	}
}
