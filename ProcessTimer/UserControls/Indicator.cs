﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ProcessTimer.UserControls
{
	public partial class Indicator : UserControl
	{
		private Color _color;

		public Color Color
		{
			get { return _color; }
			set
			{
				_color = value;
				labelText.ForeColor = _color;
				borderPanel1.BorderColor = _color;
			}
		}

		public override string Text
		{
			get { return labelText.Text; }
			set
			{
				labelText.Text = value;
				RepositionText();
			}
		}

		public Indicator()
		{
			InitializeComponent();
			Color = Color.Green;
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			RepositionText();
		}

		protected void RepositionText()
		{
			var x = (borderPanel1.Width - labelText.Width) / 2;
			var y = (borderPanel1.Height - labelText.Height) / 2;
			labelText.Location = new Point(x, y);
		}

		protected void Control_Click(object sender, EventArgs e)
		{
			OnClick(e);
		}
	}
}
