﻿using ProcessTimer.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessTimer.UserControls.TimerViews
{
	public interface ITimerView
	{
		int Number { get; set; }
		TimerState State { get; }
		Core.ProcessState ProcessState { get; set; }

		Func<ITimerView, Core.ProcessState> GetState { get; set; }

		event EventHandler<TimerStateChangedEventArgs> StateChanged;

		void UpdateProgress();
	}
}
