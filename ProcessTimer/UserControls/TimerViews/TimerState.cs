﻿namespace ProcessTimer.UserControls.TimerViews
{
	public enum TimerState
	{
		None,
		Running,
		Finished,
		Paused,
		NeedChecking,
		Checked,
		Unstarted
	}
}
