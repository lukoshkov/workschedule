﻿using System;

namespace ProcessTimer.UserControls.TimerViews
{
	public class TimerStateChangedEventArgs : EventArgs
	{
		public TimerState OldState { get; set; }
		public TimerState NewState { get; set; }

		public TimerStateChangedEventArgs(TimerState oldState, TimerState newState)
		{
			OldState = oldState;
			NewState = newState;
		}
	}
}
