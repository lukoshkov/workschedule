﻿namespace ProcessTimer.UserControls
{
	partial class TimerView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.labelProgress = new System.Windows.Forms.Label();
			this.labelStepNumber = new System.Windows.Forms.Label();
			this.textBoxDescription = new System.Windows.Forms.TextBox();
			this.buttonStart = new System.Windows.Forms.Button();
			this.buttonReset = new System.Windows.Forms.Button();
			this.buttonNext = new System.Windows.Forms.Button();
			this.labelTitle = new System.Windows.Forms.Label();
			this.labelStatus = new System.Windows.Forms.Label();
			this.buttonPrev = new System.Windows.Forms.Button();
			this.panelTimer = new System.Windows.Forms.Panel();
			this.panelBorder = new System.Windows.Forms.Panel();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonRemove = new System.Windows.Forms.Button();
			this.buttonAdd = new System.Windows.Forms.Button();
			this.panelTimer.SuspendLayout();
			this.panelBorder.SuspendLayout();
			this.SuspendLayout();
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.CustomFormat = "Начало:   HH:mm   dd.MM.yyyy";
			this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePicker1.Location = new System.Drawing.Point(12, 122);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
			this.dateTimePicker1.TabIndex = 0;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(12, 164);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(200, 23);
			this.progressBar1.TabIndex = 1;
			// 
			// labelProgress
			// 
			this.labelProgress.AutoSize = true;
			this.labelProgress.Location = new System.Drawing.Point(9, 147);
			this.labelProgress.Name = "labelProgress";
			this.labelProgress.Size = new System.Drawing.Size(110, 13);
			this.labelProgress.TabIndex = 2;
			this.labelProgress.Text = "00:00 / 00:00 (0,00%)";
			// 
			// labelStepNumber
			// 
			this.labelStepNumber.AutoSize = true;
			this.labelStepNumber.Location = new System.Drawing.Point(12, 27);
			this.labelStepNumber.Name = "labelStepNumber";
			this.labelStepNumber.Size = new System.Drawing.Size(94, 13);
			this.labelStepNumber.TabIndex = 3;
			this.labelStepNumber.Text = "Этап 1/1 : \"Тест\"";
			// 
			// textBoxDescription
			// 
			this.textBoxDescription.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxDescription.Location = new System.Drawing.Point(12, 43);
			this.textBoxDescription.Multiline = true;
			this.textBoxDescription.Name = "textBoxDescription";
			this.textBoxDescription.ReadOnly = true;
			this.textBoxDescription.Size = new System.Drawing.Size(200, 36);
			this.textBoxDescription.TabIndex = 4;
			this.textBoxDescription.TabStop = false;
			// 
			// buttonStart
			// 
			this.buttonStart.Location = new System.Drawing.Point(12, 217);
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size(96, 23);
			this.buttonStart.TabIndex = 5;
			this.buttonStart.Text = "Начать";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
			// 
			// buttonReset
			// 
			this.buttonReset.Location = new System.Drawing.Point(116, 217);
			this.buttonReset.Name = "buttonReset";
			this.buttonReset.Size = new System.Drawing.Size(96, 23);
			this.buttonReset.TabIndex = 6;
			this.buttonReset.Text = "Сбросить";
			this.buttonReset.UseVisualStyleBackColor = true;
			this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
			// 
			// buttonNext
			// 
			this.buttonNext.Location = new System.Drawing.Point(12, 85);
			this.buttonNext.Name = "buttonNext";
			this.buttonNext.Size = new System.Drawing.Size(96, 23);
			this.buttonNext.TabIndex = 7;
			this.buttonNext.Text = "Следующий";
			this.buttonNext.UseVisualStyleBackColor = true;
			this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
			// 
			// labelTitle
			// 
			this.labelTitle.AutoSize = true;
			this.labelTitle.Location = new System.Drawing.Point(12, 6);
			this.labelTitle.Name = "labelTitle";
			this.labelTitle.Size = new System.Drawing.Size(51, 13);
			this.labelTitle.TabIndex = 8;
			this.labelTitle.Text = "Процесс";
			// 
			// labelStatus
			// 
			this.labelStatus.AutoSize = true;
			this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelStatus.Location = new System.Drawing.Point(8, 192);
			this.labelStatus.Name = "labelStatus";
			this.labelStatus.Size = new System.Drawing.Size(68, 20);
			this.labelStatus.TabIndex = 9;
			this.labelStatus.Text = "Статус";
			// 
			// buttonPrev
			// 
			this.buttonPrev.Location = new System.Drawing.Point(116, 85);
			this.buttonPrev.Name = "buttonPrev";
			this.buttonPrev.Size = new System.Drawing.Size(96, 23);
			this.buttonPrev.TabIndex = 10;
			this.buttonPrev.Text = "Предыдущий";
			this.buttonPrev.UseVisualStyleBackColor = true;
			this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
			// 
			// panelTimer
			// 
			this.panelTimer.Controls.Add(this.textBoxDescription);
			this.panelTimer.Controls.Add(this.buttonPrev);
			this.panelTimer.Controls.Add(this.dateTimePicker1);
			this.panelTimer.Controls.Add(this.labelStatus);
			this.panelTimer.Controls.Add(this.progressBar1);
			this.panelTimer.Controls.Add(this.labelTitle);
			this.panelTimer.Controls.Add(this.labelProgress);
			this.panelTimer.Controls.Add(this.buttonNext);
			this.panelTimer.Controls.Add(this.labelStepNumber);
			this.panelTimer.Controls.Add(this.buttonReset);
			this.panelTimer.Controls.Add(this.buttonStart);
			this.panelTimer.Location = new System.Drawing.Point(3, 59);
			this.panelTimer.Name = "panelTimer";
			this.panelTimer.Size = new System.Drawing.Size(226, 250);
			this.panelTimer.TabIndex = 11;
			// 
			// panelBorder
			// 
			this.panelBorder.Controls.Add(this.label3);
			this.panelBorder.Controls.Add(this.label2);
			this.panelBorder.Controls.Add(this.label1);
			this.panelBorder.Controls.Add(this.buttonRemove);
			this.panelBorder.Controls.Add(this.buttonAdd);
			this.panelBorder.Controls.Add(this.panelTimer);
			this.panelBorder.Location = new System.Drawing.Point(3, 3);
			this.panelBorder.Name = "panelBorder";
			this.panelBorder.Size = new System.Drawing.Size(233, 313);
			this.panelBorder.TabIndex = 12;
			this.panelBorder.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(59, 1);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 13);
			this.label3.TabIndex = 16;
			this.label3.Text = "Камера";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(59, 15);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 15;
			this.label2.Text = "Заголовок";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(15, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(27, 20);
			this.label1.TabIndex = 14;
			this.label1.Text = "#1";
			// 
			// buttonRemove
			// 
			this.buttonRemove.Location = new System.Drawing.Point(119, 30);
			this.buttonRemove.Name = "buttonRemove";
			this.buttonRemove.Size = new System.Drawing.Size(96, 23);
			this.buttonRemove.TabIndex = 13;
			this.buttonRemove.Text = "Удалить";
			this.buttonRemove.UseVisualStyleBackColor = true;
			this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
			// 
			// buttonAdd
			// 
			this.buttonAdd.Location = new System.Drawing.Point(15, 30);
			this.buttonAdd.Name = "buttonAdd";
			this.buttonAdd.Size = new System.Drawing.Size(96, 23);
			this.buttonAdd.TabIndex = 12;
			this.buttonAdd.Text = "Добавить";
			this.buttonAdd.UseVisualStyleBackColor = true;
			this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// TimerView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panelBorder);
			this.Name = "TimerView";
			this.Size = new System.Drawing.Size(239, 319);
			this.panelTimer.ResumeLayout(false);
			this.panelTimer.PerformLayout();
			this.panelBorder.ResumeLayout(false);
			this.panelBorder.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label labelProgress;
		private System.Windows.Forms.Label labelStepNumber;
		private System.Windows.Forms.TextBox textBoxDescription;
		private System.Windows.Forms.Button buttonStart;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.Button buttonNext;
		private System.Windows.Forms.Label labelTitle;
		private System.Windows.Forms.Label labelStatus;
		private System.Windows.Forms.Button buttonPrev;
		private System.Windows.Forms.Button buttonRemove;
		private System.Windows.Forms.Button buttonAdd;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		protected System.Windows.Forms.Panel panelTimer;
		protected System.Windows.Forms.Panel panelBorder;
	}
}
