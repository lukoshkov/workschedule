﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProcessTimer.UserControls
{
	public class BorderPanel : Panel
	{
		private Color _color;
		private int _borderWidth;

		public Color BorderColor
		{
			get
			{
				return _color;
			}
			set
			{
				_color = value;
				Invalidate();
			}
		}

		public int BorderWidth
		{
			get
			{
				return _borderWidth;
			}
			set
			{
				_borderWidth = value;
				Invalidate();
			}
		}

		public BorderPanel()
			: base()
		{
			DoubleBuffered = true;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			var color = BorderColor;
			var width = BorderWidth;
			var style = ButtonBorderStyle.Solid;
			ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
					   color, width, style, // left
					   color, width, style, // top
					   color, width, style, // right
					   color, width, style);// bottom
		}
	}
}
