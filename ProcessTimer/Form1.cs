﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ProcessTimer.Core;
using ProcessTimer.Core.Storing;
using ProcessTimer.UserControls;
using ProcessTimer.UserControls.TimerViews;

using TV = ProcessTimer.UserControls.TimerViewHorizontal;
using TVOther = ProcessTimer.UserControls.TimerView;

namespace ProcessTimer
{
	public partial class Form1 : Form
	{
		private static readonly string TimelinesDir = "Timelines";
		private static readonly string ProductsDir = "Products";
		private static readonly string StateFile = "ProcessTimer.state";
		private static readonly string LogProgramEventType = "СОБЫТИЕ";
		private static readonly string LogUserActionType = "ДЕЙСТВИЕ";

		private Windows.TimelineSelectForm _timelineSelect;

		private List<Product> _products;
		private List<ProcessTimeline> _timelines;
		private List<ProcessState> _states;
		private List<TV> _timerViews;
		private List<TVOther> _timerViewsH;
		private List<Indicator> _indicators;
		private Timer _timerMaster;
		private Timer _timerStateWrite;
		private Timer _timerLogWrite;
		private Dictionary<TimerState, int> _stateCounts;
		private Dictionary<TimerState, string> _stateTitles;
		private Dictionary<TimerState, string> _localizedStates;
		private System.Media.SoundPlayer _player;
		private TimerState _playerState;

		private Logging.Log _eventLog;

		public Form1()
		{
			InitializeComponent();
			_timelineSelect = new Windows.TimelineSelectForm();
			_stateCounts = new Dictionary<TimerState, int>();
			_stateTitles = new Dictionary<TimerState, string>();
			_stateTitles.Add(TimerState.Finished, "Завершено");
			_stateTitles.Add(TimerState.NeedChecking, "Требуется проверка");
			_stateTitles.Add(TimerState.Paused, "Остановлено");

			_localizedStates = new Dictionary<TimerState, string>();
			_localizedStates.Add(TimerState.Checked, "Проверено");
			_localizedStates.Add(TimerState.Finished, "Завершено");
			_localizedStates.Add(TimerState.NeedChecking, "Требуется проверка");
			_localizedStates.Add(TimerState.None, "");
			_localizedStates.Add(TimerState.Paused, "Остановлено");
			_localizedStates.Add(TimerState.Running, "Запущено");
			_localizedStates.Add(TimerState.Unstarted, "Ожидание");

			_products = ProductStorage.RetrieveAll(ProductsDir);
			if (_products == null) _products = new List<Product>();
			UpdateProductsList();
			
			_timelines = TimelineStorage.RetrieveAll(TimelinesDir, _products);
			if (_timelines == null) _timelines = new List<ProcessTimeline>();
			var old = _timelines.FindAll(t => t.Product == null || t.Category == null);
			if (old.Count > 0)
			{
				var product = new Product("Без изделия");
				var category = new TestCategory("Без категории");
				product.TestCategories.Add(category);
				_products.Add(product);
				ProductStorage.Store(product, ProductsDir);
				SetProductAndCategory(old, product, category);
				UpdateProductsList();
			}
			//_timelines = CreateRandomTimelines(11, _products[0]);

			//_timelines = new List<ProcessTimeline>();
			//var timeline = new ProcessTimeline("Test");
			//timeline.Steps.Add(new ProcessStep(new TimeSpan(0, 0, 10), "Title", "Description", new TimeSpan(0, 0, 5)));
			//_timelines.Add(timeline);

			LoadStates();
			CreateTimerViews();
			
			////ResizeEnd += (s, e) => FlowLayoutPannel_SizeChanged(flowLayoutPanel1, e);
			////FlowLayoutPannel_SizeChanged(flowLayoutPanel1, new EventArgs());
			//flowLayoutPanel1.WrapContents = true;
			//flowLayoutPanel1.FlowDirection = FlowDirection.LeftToRight;
			////flowLayoutPanel1.Resize += FlowLayoutPannel_SizeChanged;
			tabControl1.TabPages.Remove(tabPage4);
			SetControlsAutoSize(flowPanelTimers, true);

			SetProductButtonsEnabled();
			SetTimelineButtonsEnabled();
			UpdateTimelinesList();

			CountTimerStatuses();
			UpdateTimersStatus();
			CreateIndicators();

			_timerMaster = new Timer();
			_timerMaster.Interval = 1000;
			_timerMaster.Tick += (s, e) =>
			{
				foreach (var tv in _timerViews)
				{
					tv.UpdateProgress();
				}
			};
			_timerMaster.Start();

			_timerStateWrite = new Timer();
			_timerStateWrite.Interval = 10 * 1000;
			_timerStateWrite.Tick += (s, e) => StateStorage.Store(_states, StateFile);
			_timerStateWrite.Start();

			_player = new System.Media.SoundPlayer();

			_eventLog = new Logging.Log(string.Format(@"Logs\{0}-{1:00}-{2:00}.csv", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day), Encoding.GetEncoding(1251));
			AddEventLogLine(LogProgramEventType, "Приложение запущено");

			_timerLogWrite = new Timer();
			_timerLogWrite.Interval = 10 * 1000;
			_timerLogWrite.Tick += (s, e) => System.Threading.ThreadPool.QueueUserWorkItem(obj => _eventLog.TryFlush());
			_timerLogWrite.Start();
		}

		private void AddEventLogLine(string eventType, string message)
		{
			_eventLog.AddLine("{0};{1};{2}", DateTime.Now, eventType, message);
		}

		private string ProcessStateToString(ProcessState state)
		{
			return string.Format("(Изделие: {0} | Категория: {1} | Процесс: {2} | Камера: {3})",
				state.Timeline.Product.Title,
				state.Timeline.Category.Title,
				state.Timeline.Title, state.ChamberTitle);
		}

		private string ProcessStateToCsvString(ProcessState state)
		{
			return string.Format("\"{0}\";\"{1}\";\"{2}\";\"{3}\"",
				state.Timeline.Product.Title, 
				state.Timeline.Category.Title,
				state.Timeline.Title, 
				state.ChamberTitle);
		}

		private void SetControlsAutoSize(FlowLayoutPanel flp, bool value)
		{
			if (value)
			{
				flp.WrapContents = false;
				flp.FlowDirection = FlowDirection.TopDown;
				flp.Resize += FlowLayoutPannel_SizeChanged;
			}
			else
			{
				flp.WrapContents = true;
				flp.FlowDirection = FlowDirection.LeftToRight;
				flp.Resize -= FlowLayoutPannel_SizeChanged;
			}
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);
			_timerMaster.Stop();
			_timerStateWrite.Stop();
			_timerLogWrite.Stop();
			StateStorage.Store(_states, StateFile);
			_eventLog.TryFlush();
		}

		private void FlowLayoutPannel_SizeChanged(object sender, EventArgs e)
		{
			var flp = sender as FlowLayoutPanel;
			flp.SuspendLayout();
			var scrollBarWidth = flp.VerticalScroll.Visible ? 30 : 30;
			foreach (Control ctrl in flp.Controls)
			{
				ctrl.Width = flp.ClientSize.Width - scrollBarWidth;
			}
			flp.ResumeLayout();
		}

		private ProcessState CreateProcessState(ITimerView tv)
		{
			ProcessState state = null;
			if (_timelineSelect.ShowDialog() == DialogResult.OK)
			{
				var timeline = _timelineSelect.SelectedTimeline;
				if (timeline != null)
				{
					state = new ProcessState(timeline);
					state.ChamberTitle = _timelineSelect.ChamberTitle;
					var index = _timerViews.FindIndex(t => ReferenceEquals(t, tv));
					_states[index] = state;
					AddEventLogLine(LogUserActionType, string.Format("Добавлен таймер;{0}", ProcessStateToCsvString(state)));

					_states.Add(null);
					var lastTV = CreateTimerView(_states.Count - 1);
					lastTV.Width = _timerViews[0].Width;
					_timerViews.Add(lastTV);
					flowPanelTimers.Controls.Add(lastTV);
					var lastInd = CreateIndicator(_states.Count - 1);
					_indicators.Add(lastInd);
					flpIndicators.Controls.Add(lastInd);
					CountTimerStatuses();
					UpdateTimersStatus();
				}
			}
			return state;
		}

		private void LoadStates()
		{
			try
			{
				_states = StateStorage.Retrieve(StateFile, _timelines);
			}
			catch (Exception ex1)
			{
				try
				{
					_states = StateStorage.RetrieveBackup(StateFile, _timelines);
				}
				catch (Exception ex2)
				{
				}
			}
			if (_states == null)
			{
				_states = new List<ProcessState>();
			}
			if (_states.Count == 0)
			{
				_states.Add(null);
			}
		}

		private void CreateTimerViews()
		{
			_timerViews = new List<TV>();
			for (int i = 0; i < _states.Count; i++)
			{
				var tv = CreateTimerView(i);
				_timerViews.Add(tv);
			}
			flowPanelTimers.Controls.Clear();
			flowPanelTimers.Controls.AddRange(_timerViews.ToArray());
		}

		private TV CreateTimerView(int index)
		{
			var tv = new TV();
			tv.ProcessState = _states[index];
			tv.GetState = CreateProcessState;
			tv.StateChanged += OnTimerStateChanged;
			tv.StepIndexChanged += OnTimerStepChanged;
			tv.StepResetted += OnTimerStepResetted;
			tv.Number = index + 1;
			return tv;
		}

		private void CreateIndicators()
		{
			_indicators = new List<Indicator>();
			for (int i = 0; i < _timerViews.Count; i++)
			{
				var indicator = CreateIndicator(i);
				_indicators.Add(indicator);
			}
			flpIndicators.Controls.Clear();
			flpIndicators.Controls.AddRange(_indicators.ToArray());
		}

		private Indicator CreateIndicator(int index)
		{
			var indicator = new Indicator();
			indicator.Size = new Size(30, 30);
			indicator.Color = TimerView.GetTimerStateColor(_timerViews[index].State);
			indicator.Text = _timerViews[index].Number.ToString();
			indicator.Click += (s, e) => ScrollPanelTo(flowPanelTimers, int.Parse(indicator.Text) - 1);
			return indicator;
		}

		private void OnTimerStateChanged(object sender, TimerStateChangedEventArgs e)
		{
			var tv = sender as TV;
			var index = _timerViews.FindIndex(t => ReferenceEquals(t, tv));
			_indicators[index].Color = TimerView.GetTimerStateColor(e.NewState);
			if (_stateCounts.ContainsKey(e.OldState))
			{
				_stateCounts[e.OldState] = Math.Max(0, _stateCounts[e.OldState] - 1);
			}
			if (_stateCounts.ContainsKey(e.NewState))
			{
				_stateCounts[e.NewState]++;
			}
			else
			{
				_stateCounts.Add(e.NewState, 1);
			}
			if (tv.State == TimerState.None)
			{
				RemoveTimerView(index);
			}
			var state = tv.ProcessState;
			if (state != null)
			{
				var type = e.NewState == TimerState.NeedChecking || e.NewState == TimerState.Finished ? LogProgramEventType : LogUserActionType;
				var message = string.Format("Состояние таймера изменилось с {1} на {2};{0}", 
					ProcessStateToCsvString(state), _localizedStates[e.OldState], _localizedStates[e.NewState]);
				AddEventLogLine(type, message);
			}
			UpdateTimersStatus();
			UpdateSoundPlayer();
		}

		private void OnTimerStepChanged(object sender, EventArgs e)
		{
			var tv = sender as TV;
			if (tv == null) return;
			var state = tv.ProcessState;
			if (state == null) return;
			var message = string.Format("Выбран шаг процесса {1} из {2};{0}",
				ProcessStateToCsvString(state), state.StepIndex + 1, state.StepsCount);
			AddEventLogLine(LogUserActionType, message);
		}

		private void OnTimerStepResetted(object sender, EventArgs e)
		{
			var tv = sender as TV;
			if (tv == null) return;
			var state = tv.ProcessState;
			if (state == null) return;
			var message = string.Format("Таймер сброшен;{0}", ProcessStateToCsvString(state));
			AddEventLogLine(LogUserActionType, message);
		}

		private void RemoveTimerView(int index)
		{
			_states.RemoveAt(index);
			flowPanelTimers.Controls.RemoveAt(index);
			flpIndicators.Controls.RemoveAt(index);
			_timerViews.RemoveAt(index);
			_indicators.RemoveAt(index);
			for (int i = 0; i < _timerViews.Count; i++)
			{
				_timerViews[i].Number = i + 1;
				_indicators[i].Text = (i + 1).ToString();
			}
			CountTimerStatuses();
			UpdateTimersStatus();
		}

		private void UpdateTimersStatus()
		{
			var sb = new StringBuilder();
			foreach (var item in _stateTitles)
			{
				var count = _stateCounts.ContainsKey(item.Key) ? _stateCounts[item.Key] : 0;
				sb.AppendFormat("|  {0}: {1}  ", item.Value, count);
			}
			sb.Append("|");
			labelTimersStatus.Text = sb.ToString();
			Text = "Таймеры      " + sb.ToString();
		}

		private void CountTimerStatuses()
		{
			_stateCounts.Clear();
			foreach (var tv in _timerViews)
			{
				var state = tv.State;
				if (_stateCounts.ContainsKey(state))
				{
					_stateCounts[state]++;
				}
				else
				{
					_stateCounts.Add(state, 1);
				}
			}
		}

		private void UpdateSoundPlayer()
		{
			var states = new TimerState[] { TimerState.Finished, TimerState.NeedChecking };
			var sounds = new string[] { @"Sounds\1.wav", @"Sounds\2.wav" };
			var hasSound = false;
			for (int i = 0; i < states.Length; i++)
			{
				if (_stateCounts.ContainsKey(states[i]) && _stateCounts[states[i]] > 0)
				{
					hasSound = true;
					if (states[i] == _playerState)
					{
						break;
					}
					else
					{
						try
						{
							_playerState = states[i];
							_player.SoundLocation = sounds[i];
							_player.PlayLooping();
						}
						catch (Exception ex)
						{
							System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show(ex.Message));
						}
						var message = string.Format("Одна или несколько камер перешли в состояние {0}", _stateTitles[states[i]]);
						System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show(message));
					}
				}
			}
			if (!hasSound)
			{
				_playerState = TimerState.None;
				_player.Stop();
			}
		}

		private void ScrollPanelTo(FlowLayoutPanel panel, int index)
		{
			var control = panel.Controls[index];
			var loc = control.Location - new Size(panel.AutoScrollPosition);
			loc -= new Size(control.Margin.Left, control.Margin.Top);
			panel.AutoScrollPosition = loc;
			control.Focus();
		}

		//protected override void OnResize(EventArgs e)
		//{
		//	base.OnResize(e);
		//	var tv = new TimerView();
		//	flowPanelTimers.Dock = DockStyle.None;
		//	flowPanelTimers.AutoSize = false;
		//	flowPanelTimers.Height = tabControl1.ClientSize.Height;
		//	var w = tv.Width + tv.Margin.Left + tv.Margin.Right + 10;
		//	flowPanelTimers.Width = (tabControl1.ClientSize.Width / w) * w;
		//	var x = (tabControl1.ClientSize.Width - flowPanelTimers.Width) / 2;
		//	flowPanelTimers.Location = new Point(x, flowPanelTimers.Location.Y);
		//}

		private static List<ProcessTimeline> CreateRandomTimelines(int count, Product product)
		{
			var list = new List<ProcessTimeline>();
			var random = new Random();
			for (int i = 0; i < count; i++)
			{
				var timeline = new ProcessTimeline(random.Next().ToString());
				timeline.Product = product;
				timeline.Category = product.TestCategories[0];
				for (int j = 0; j < 3; j++)
				{
					var duration = new TimeSpan(0, random.Next(0, 3), random.Next(0, 60));
					timeline.Steps.Add(new ProcessStep(duration, random.Next().ToString(), random.Next().ToString()));
				}
				list.Add(timeline);
			}
			return list;
		}

		private static void SetProductAndCategory(List<ProcessTimeline> timelines, Product product, TestCategory category)
		{
			foreach (var tl in timelines)
			{
				tl.Product = product;
				tl.Category = category;
				TimelineStorage.Update(tl, TimelinesDir);
			}
		}

		#region Timeline edit tab

		private bool _timelinesUpdating = false;

		private void comboBoxProducts_SelectedIndexChanged(object sender, EventArgs e)
		{
			var product = comboBoxProducts.SelectedItem as Product;
			comboBoxCategories.DataSource = null;
			if (product != null)
			{
				comboBoxCategories.DataSource = product.TestCategories;
				comboBoxCategories.DisplayMember = nameof(TestCategory.Title);
				comboBoxCategories.SelectedIndex = -1;
			}
		}

		private void comboBoxCategories_SelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateTimelinesList();
			SetTimelineButtonsEnabled();
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetTimelineButtonsEnabled();
			if (_timelinesUpdating) return;
			timelineEdit1.Timeline = listBox1.SelectedItem as ProcessTimeline;
		}

		private void SetTimelineButtonsEnabled()
		{
			buttonTimelineAdd.Enabled = comboBoxCategories.SelectedIndex > -1;
			buttonTimelineEdit.Enabled = listBox1.SelectedIndex > -1;
			buttonTimelineDelete.Enabled = buttonTimelineEdit.Enabled;
		}

		private void buttonTimelineAdd_Click(object sender, EventArgs e)
		{
			var timeline = timelineEdit1.GetNew();
			var product = comboBoxProducts.SelectedItem as Product;
			var category = comboBoxCategories.SelectedItem as TestCategory;
			if (product == null || category == null)
			{
				System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show("Необходимо выбрать Изделие и Категорию испытаний"));
				return;
			}
			if (!string.IsNullOrEmpty(timeline.Title))
			{
				timeline.Product = product;
				timeline.Category = category;
				try
				{
					TimelineStorage.Store(timeline, TimelinesDir);
					_timelines.Add(timeline);
					UpdateTimelinesList();
				}
				catch (Exception ex)
				{
					var message = string.Format("{0}\n{1}", ex.Message, ex.StackTrace);
					System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show(message));
				}
			}
			else
			{
				System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show("Поле Название не может быть пустым"));
			}
		}

		private void buttonTimelineEdit_Click(object sender, EventArgs e)
		{
			var timeline = listBox1.SelectedItem as ProcessTimeline;
			var product = comboBoxProducts.SelectedItem as Product;
			var category = comboBoxCategories.SelectedItem as TestCategory;
			if (product == null || category == null)
			{
				System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show("Необходимо выбрать Изделие и Категорию испытаний"));
				return;
			}
			if (timeline != null && timeline == timelineEdit1.Timeline)
			{
				timelineEdit1.ApplyChanges();
				timeline.Product = product;
				timeline.Category = category;
				try
				{
					TimelineStorage.Update(timeline, TimelinesDir);
					UpdateTimelinesList();
				}
				catch (Exception ex)
				{
					var message = string.Format("{0}\n{1}", ex.Message, ex.StackTrace);
					System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show(message));
				}
			}
		}

		private void buttonTimelineDelete_Click(object sender, EventArgs e)
		{
			var timeline = listBox1.SelectedItem as ProcessTimeline;
			if (timeline != null)
			{
				var stateI = _states.FindIndex(s => s != null && ReferenceEquals(s.Timeline, timeline));
				if (stateI > -1)
				{
					var message = string.Format("Процесс используется таймером № {0}, удаление невозможно.", _timerViews[stateI].Number);
					System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show(message));
					return;
				}
				try
				{
					TimelineStorage.Delete(timeline, TimelinesDir);
					_timelines.Remove(timeline);
					UpdateTimelinesList();
				}
				catch (Exception ex)
				{
					var message = string.Format("{0}\n{1}", ex.Message, ex.StackTrace);
					System.Threading.ThreadPool.QueueUserWorkItem(o => MessageBox.Show(message));
				}
			}
		}

		private void buttonLoad_Click(object sender, EventArgs e)
		{
			var dialog = new OpenFileDialog();
			dialog.Filter = "CSV (с разделителями)|*.csv";
			dialog.CheckFileExists = true;
			dialog.Multiselect = false;
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				ProcessTimeline timeline;
				if (TimelineCsvParser.TryParse(dialog.FileName, out timeline))
				{
					timeline.Title = System.IO.Path.GetFileNameWithoutExtension(dialog.SafeFileName);
					timelineEdit1.UpdateFields(timeline);
				}
			}
		}

		#endregion

		#region Product edit tab

		private void listBoxProducts_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetProductButtonsEnabled();
			productEdit1.Product = listBoxProducts.SelectedItem as Product;
		}

		private void SetProductButtonsEnabled()
		{
			buttonProductEdit.Enabled = listBoxProducts.SelectedIndex > -1;
			buttonProductDelete.Enabled = buttonProductEdit.Enabled;
		}

		private void buttonProductAdd_Click(object sender, EventArgs e)
		{
			var product = productEdit1.GetNew();
			if (!string.IsNullOrEmpty(product.Title))
			{
				ProductStorage.Store(product, ProductsDir);
				_products.Add(product);
				UpdateProductsList();
			}
		}

		private void buttonProductEdit_Click(object sender, EventArgs e)
		{
			var product = listBoxProducts.SelectedItem as Product;
			if (product != null && product == productEdit1.Product)
			{
				productEdit1.ApplyChanges();
				ProductStorage.Update(product, ProductsDir);
				UpdateProductsList();
			}
		}

		private void buttonProductDelete_Click(object sender, EventArgs e)
		{
			var product = listBoxProducts.SelectedItem as Product;
			if (product != null)
			{
				ProductStorage.Delete(product, ProductsDir);
				_products.Remove(product);
				UpdateProductsList();
			}
		}

		#endregion

		private void UpdateTimelinesList()
		{
			_timelinesUpdating = true;
			_timelineSelect.Timelines = _timelines;
			var product = comboBoxProducts.SelectedItem as Product;
			var category = comboBoxCategories.SelectedItem as TestCategory;
			listBox1.DataSource = null;
			if (product == null || category == null) return;
			var timelines = _timelines.FindAll(t => t.Product == product && t.Category == category);
			listBox1.DataSource = timelines;
			listBox1.SelectedIndex = -1;
			_timelinesUpdating = false;
		}

		private void UpdateProductsList()
		{
			listBoxProducts.DataSource = null;
			listBoxProducts.DataSource = _products;
			listBoxProducts.DisplayMember = nameof(Product.Title);
			listBoxProducts.SelectedIndex = -1;
			comboBoxProducts.DataSource = null;
			comboBoxProducts.DataSource = new List<Product>(_products);
			comboBoxProducts.DisplayMember = nameof(Product.Title);
			comboBoxProducts.SelectedIndex = -1;
			_timelineSelect.Products = _products;
		}
	}
}
