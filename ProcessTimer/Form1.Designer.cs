﻿namespace ProcessTimer
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.flowPanelTimers = new System.Windows.Forms.FlowLayoutPanel();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.buttonTimelineAdd = new System.Windows.Forms.Button();
			this.buttonTimelineEdit = new System.Windows.Forms.Button();
			this.buttonTimelineDelete = new System.Windows.Forms.Button();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.timelineEdit1 = new ProcessTimer.UserControls.TimelineEdit();
			this.comboBoxProducts = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxCategories = new System.Windows.Forms.ComboBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.listBoxProducts = new System.Windows.Forms.ListBox();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.buttonProductAdd = new System.Windows.Forms.Button();
			this.buttonProductEdit = new System.Windows.Forms.Button();
			this.buttonProductDelete = new System.Windows.Forms.Button();
			this.productEdit1 = new ProcessTimer.UserControls.ProductEdit();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.labelTimersStatus = new System.Windows.Forms.Label();
			this.flpIndicators = new System.Windows.Forms.FlowLayoutPanel();
			this.indicator1 = new ProcessTimer.UserControls.Indicator();
			this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
			this.buttonLoad = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.flowLayoutPanel3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.flpIndicators.SuspendLayout();
			this.tableLayoutMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(5, 5);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(5);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(774, 486);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.flowPanelTimers);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(766, 460);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Таймеры";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// flowPanelTimers
			// 
			this.flowPanelTimers.AutoScroll = true;
			this.flowPanelTimers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowPanelTimers.Location = new System.Drawing.Point(3, 3);
			this.flowPanelTimers.Name = "flowPanelTimers";
			this.flowPanelTimers.Size = new System.Drawing.Size(760, 454);
			this.flowPanelTimers.TabIndex = 0;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.tableLayoutPanel2);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(766, 460);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Процессы";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 3;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 2, 3);
			this.tableLayoutPanel2.Controls.Add(this.listBox1, 0, 3);
			this.tableLayoutPanel2.Controls.Add(this.timelineEdit1, 2, 4);
			this.tableLayoutPanel2.Controls.Add(this.comboBoxProducts, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.comboBoxCategories, 1, 1);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 5;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(760, 454);
			this.tableLayoutPanel2.TabIndex = 2;
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.flowLayoutPanel2.AutoSize = true;
			this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.flowLayoutPanel2.Controls.Add(this.buttonTimelineAdd);
			this.flowLayoutPanel2.Controls.Add(this.buttonTimelineEdit);
			this.flowLayoutPanel2.Controls.Add(this.buttonTimelineDelete);
			this.flowLayoutPanel2.Controls.Add(this.buttonLoad);
			this.flowLayoutPanel2.Location = new System.Drawing.Point(346, 67);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(324, 29);
			this.flowLayoutPanel2.TabIndex = 1;
			// 
			// buttonTimelineAdd
			// 
			this.buttonTimelineAdd.Location = new System.Drawing.Point(3, 3);
			this.buttonTimelineAdd.Name = "buttonTimelineAdd";
			this.buttonTimelineAdd.Size = new System.Drawing.Size(75, 23);
			this.buttonTimelineAdd.TabIndex = 0;
			this.buttonTimelineAdd.Text = "Добавить";
			this.buttonTimelineAdd.UseVisualStyleBackColor = true;
			this.buttonTimelineAdd.Click += new System.EventHandler(this.buttonTimelineAdd_Click);
			// 
			// buttonTimelineEdit
			// 
			this.buttonTimelineEdit.Location = new System.Drawing.Point(84, 3);
			this.buttonTimelineEdit.Name = "buttonTimelineEdit";
			this.buttonTimelineEdit.Size = new System.Drawing.Size(75, 23);
			this.buttonTimelineEdit.TabIndex = 1;
			this.buttonTimelineEdit.Text = "Изменить";
			this.buttonTimelineEdit.UseVisualStyleBackColor = true;
			this.buttonTimelineEdit.Click += new System.EventHandler(this.buttonTimelineEdit_Click);
			// 
			// buttonTimelineDelete
			// 
			this.buttonTimelineDelete.Location = new System.Drawing.Point(165, 3);
			this.buttonTimelineDelete.Name = "buttonTimelineDelete";
			this.buttonTimelineDelete.Size = new System.Drawing.Size(75, 23);
			this.buttonTimelineDelete.TabIndex = 2;
			this.buttonTimelineDelete.Text = "Удалить";
			this.buttonTimelineDelete.UseVisualStyleBackColor = true;
			this.buttonTimelineDelete.Click += new System.EventHandler(this.buttonTimelineDelete_Click);
			// 
			// listBox1
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.listBox1, 2);
			this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(3, 67);
			this.listBox1.Name = "listBox1";
			this.tableLayoutPanel2.SetRowSpan(this.listBox1, 2);
			this.listBox1.Size = new System.Drawing.Size(337, 384);
			this.listBox1.TabIndex = 0;
			this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
			// 
			// timelineEdit1
			// 
			this.timelineEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.timelineEdit1.Location = new System.Drawing.Point(346, 102);
			this.timelineEdit1.Name = "timelineEdit1";
			this.timelineEdit1.Size = new System.Drawing.Size(411, 349);
			this.timelineEdit1.TabIndex = 0;
			// 
			// comboBoxProducts
			// 
			this.comboBoxProducts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxProducts.FormattingEnabled = true;
			this.comboBoxProducts.Location = new System.Drawing.Point(69, 3);
			this.comboBoxProducts.Name = "comboBoxProducts";
			this.comboBoxProducts.Size = new System.Drawing.Size(271, 21);
			this.comboBoxProducts.TabIndex = 2;
			this.comboBoxProducts.SelectedIndexChanged += new System.EventHandler(this.comboBoxProducts_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Изделие";
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 34);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Категория";
			// 
			// comboBoxCategories
			// 
			this.comboBoxCategories.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxCategories.FormattingEnabled = true;
			this.comboBoxCategories.Location = new System.Drawing.Point(69, 30);
			this.comboBoxCategories.Name = "comboBoxCategories";
			this.comboBoxCategories.Size = new System.Drawing.Size(271, 21);
			this.comboBoxCategories.TabIndex = 3;
			this.comboBoxCategories.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategories_SelectedIndexChanged);
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.tableLayoutPanel1);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(766, 460);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Изделия";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.listBoxProducts, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel3, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.productEdit1, 1, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(766, 460);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// listBoxProducts
			// 
			this.listBoxProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBoxProducts.FormattingEnabled = true;
			this.listBoxProducts.Location = new System.Drawing.Point(3, 3);
			this.listBoxProducts.Name = "listBoxProducts";
			this.tableLayoutPanel1.SetRowSpan(this.listBoxProducts, 2);
			this.listBoxProducts.Size = new System.Drawing.Size(377, 454);
			this.listBoxProducts.TabIndex = 0;
			this.listBoxProducts.SelectedIndexChanged += new System.EventHandler(this.listBoxProducts_SelectedIndexChanged);
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.flowLayoutPanel3.AutoSize = true;
			this.flowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.flowLayoutPanel3.Controls.Add(this.buttonProductAdd);
			this.flowLayoutPanel3.Controls.Add(this.buttonProductEdit);
			this.flowLayoutPanel3.Controls.Add(this.buttonProductDelete);
			this.flowLayoutPanel3.Location = new System.Drawing.Point(386, 3);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(377, 29);
			this.flowLayoutPanel3.TabIndex = 1;
			// 
			// buttonProductAdd
			// 
			this.buttonProductAdd.Location = new System.Drawing.Point(3, 3);
			this.buttonProductAdd.Name = "buttonProductAdd";
			this.buttonProductAdd.Size = new System.Drawing.Size(75, 23);
			this.buttonProductAdd.TabIndex = 0;
			this.buttonProductAdd.Text = "Добавить";
			this.buttonProductAdd.UseVisualStyleBackColor = true;
			this.buttonProductAdd.Click += new System.EventHandler(this.buttonProductAdd_Click);
			// 
			// buttonProductEdit
			// 
			this.buttonProductEdit.Location = new System.Drawing.Point(84, 3);
			this.buttonProductEdit.Name = "buttonProductEdit";
			this.buttonProductEdit.Size = new System.Drawing.Size(75, 23);
			this.buttonProductEdit.TabIndex = 1;
			this.buttonProductEdit.Text = "Изменить";
			this.buttonProductEdit.UseVisualStyleBackColor = true;
			this.buttonProductEdit.Click += new System.EventHandler(this.buttonProductEdit_Click);
			// 
			// buttonProductDelete
			// 
			this.buttonProductDelete.Location = new System.Drawing.Point(165, 3);
			this.buttonProductDelete.Name = "buttonProductDelete";
			this.buttonProductDelete.Size = new System.Drawing.Size(75, 23);
			this.buttonProductDelete.TabIndex = 2;
			this.buttonProductDelete.Text = "Удалить";
			this.buttonProductDelete.UseVisualStyleBackColor = true;
			this.buttonProductDelete.Click += new System.EventHandler(this.buttonProductDelete_Click);
			// 
			// productEdit1
			// 
			this.productEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.productEdit1.Location = new System.Drawing.Point(386, 38);
			this.productEdit1.Name = "productEdit1";
			this.productEdit1.Size = new System.Drawing.Size(377, 262);
			this.productEdit1.TabIndex = 2;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.flowLayoutPanel1);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(766, 460);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.AutoScroll = true;
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(760, 454);
			this.flowLayoutPanel1.TabIndex = 0;
			this.flowLayoutPanel1.WrapContents = false;
			// 
			// labelTimersStatus
			// 
			this.labelTimersStatus.AutoSize = true;
			this.labelTimersStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelTimersStatus.Location = new System.Drawing.Point(3, 496);
			this.labelTimersStatus.Name = "labelTimersStatus";
			this.labelTimersStatus.Size = new System.Drawing.Size(72, 24);
			this.labelTimersStatus.TabIndex = 0;
			this.labelTimersStatus.Text = "Статус";
			// 
			// flpIndicators
			// 
			this.flpIndicators.AutoSize = true;
			this.flpIndicators.Controls.Add(this.indicator1);
			this.flpIndicators.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flpIndicators.Location = new System.Drawing.Point(3, 523);
			this.flpIndicators.Name = "flpIndicators";
			this.flpIndicators.Size = new System.Drawing.Size(778, 56);
			this.flpIndicators.TabIndex = 1;
			// 
			// indicator1
			// 
			this.indicator1.Color = System.Drawing.Color.Green;
			this.indicator1.Location = new System.Drawing.Point(3, 3);
			this.indicator1.Name = "indicator1";
			this.indicator1.Size = new System.Drawing.Size(50, 50);
			this.indicator1.TabIndex = 0;
			// 
			// tableLayoutMain
			// 
			this.tableLayoutMain.BackColor = System.Drawing.Color.White;
			this.tableLayoutMain.ColumnCount = 1;
			this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutMain.Controls.Add(this.flpIndicators, 0, 2);
			this.tableLayoutMain.Controls.Add(this.labelTimersStatus, 0, 1);
			this.tableLayoutMain.Controls.Add(this.tabControl1, 0, 0);
			this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutMain.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutMain.Name = "tableLayoutMain";
			this.tableLayoutMain.RowCount = 3;
			this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutMain.Size = new System.Drawing.Size(784, 582);
			this.tableLayoutMain.TabIndex = 0;
			// 
			// buttonLoad
			// 
			this.buttonLoad.Location = new System.Drawing.Point(246, 3);
			this.buttonLoad.Name = "buttonLoad";
			this.buttonLoad.Size = new System.Drawing.Size(75, 23);
			this.buttonLoad.TabIndex = 3;
			this.buttonLoad.Text = "Загрузить";
			this.buttonLoad.UseVisualStyleBackColor = true;
			this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 582);
			this.Controls.Add(this.tableLayoutMain);
			this.MinimumSize = new System.Drawing.Size(800, 620);
			this.Name = "Form1";
			this.Text = "Таймеры";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.flowLayoutPanel2.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.flowLayoutPanel3.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.flpIndicators.ResumeLayout(false);
			this.tableLayoutMain.ResumeLayout(false);
			this.tableLayoutMain.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.ListBox listBox1;
		private UserControls.TimelineEdit timelineEdit1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.Button buttonTimelineAdd;
		private System.Windows.Forms.Button buttonTimelineEdit;
		private System.Windows.Forms.Button buttonTimelineDelete;
		private System.Windows.Forms.FlowLayoutPanel flowPanelTimers;
		private System.Windows.Forms.Label labelTimersStatus;
		private System.Windows.Forms.FlowLayoutPanel flpIndicators;
		private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
		private UserControls.Indicator indicator1;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.ListBox listBoxProducts;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.Button buttonProductAdd;
		private System.Windows.Forms.Button buttonProductEdit;
		private System.Windows.Forms.Button buttonProductDelete;
		private UserControls.ProductEdit productEdit1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.ComboBox comboBoxProducts;
		private System.Windows.Forms.ComboBox comboBoxCategories;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Button buttonLoad;
	}
}

