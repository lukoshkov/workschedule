﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ProcessTimer.Core;

namespace ProcessTimer.Windows
{
	public partial class TimelineSelectForm : Form
	{
		private List<Product> _products;
		private List<ProcessTimeline> _timelines;

		public List<Product> Products
		{
			get
			{
				return _products;
			}
			set
			{
				_products = value;
				cbProducts.DataSource = null;
				cbProducts.DataSource = _products;
				cbProducts.DisplayMember = nameof(Product.Title);
				cbProducts.SelectedIndex = -1;
			}
		}

		public List<ProcessTimeline> Timelines
		{
			get
			{
				return _timelines;
			}
			set
			{
				_timelines = value;
				UpdateTimelines();
			}
		}

		public Product SelectedProduct
		{
			get
			{
				return cbProducts.SelectedItem as Product;
			}
		}

		public TestCategory SelectedCategory
		{
			get
			{
				return cbCategories.SelectedItem as TestCategory;
			}
		}

		public ProcessTimeline SelectedTimeline
		{
			get
			{
				return cbTimelines.SelectedItem as ProcessTimeline;
			}
		}

		public string ChamberTitle
		{
			get
			{
				return textBoxChamber.Text;
			}
		}

		public TimelineSelectForm()
		{
			InitializeComponent();
		}

		private void cbProducts_SelectedIndexChanged(object sender, EventArgs e)
		{
			var product = SelectedProduct;
			cbCategories.DataSource = null;
			if (product != null)
			{
				cbCategories.DataSource = product.TestCategories;
				cbCategories.DisplayMember = nameof(TestCategory.Title);
				cbCategories.SelectedIndex = -1;
			}
		}

		private void cbCategories_SelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateTimelines();
		}

		private void cbTimelines_SelectedIndexChanged(object sender, EventArgs e)
		{
			var timeline = SelectedTimeline;
			textBoxSteps.Text = "";
			if (timeline != null)
			{
				var sb = new StringBuilder();
				foreach (var step in timeline.Steps)
				{
					sb.AppendLine(step.ToString());
				}
				textBoxSteps.Text = sb.ToString();
			}
		}

		private void UpdateTimelines()
		{
			var product = SelectedProduct;
			var category = SelectedCategory;
			cbTimelines.DataSource = null;
			if (product != null && category != null)
			{
				cbTimelines.DataSource = _timelines.FindAll(t => t.Product == product && t.Category == category);
				cbTimelines.DisplayMember = nameof(ProcessTimeline.Title);
				cbTimelines.SelectedIndex = -1;
			}
		}
	}
}
