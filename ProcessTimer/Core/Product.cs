﻿using System;
using System.Collections.Generic;

namespace ProcessTimer.Core
{
	public class Product
	{
		public Guid Guid { get; set; }
		public string Title { get; set; }
		public List<TestCategory> TestCategories { get; private set; }

		public Product(string title)
			: this(title, Guid.NewGuid())
		{ }

		public Product(string title, Guid guid)
		{
			Guid = guid;
			Title = title;
			TestCategories = new List<TestCategory>();
		}
	}
}
