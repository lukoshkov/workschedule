﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessTimer.Core
{
	public class ProcessState
	{
		private TimeSpan _pausedTime;
		private DateTime _pauseStart;
		private bool _isStarted;
		private bool _isPaused;
		private int _stepIndex;
		private int _checkIntervals;
		private int _checks;

		public string ChamberTitle { get; set; }
		public ProcessTimeline Timeline { get; set; }
		public DateTime StepStart { get; set; }
		public bool IsStepFinishedChecked { get; set; }

		public DateTime StepEnd
		{
			get { return StepStart + CurrentStep.Duration + StepTotalPausedTime; }
		}

		public int StepIndex
		{
			get { return _stepIndex; }
			set
			{
				_stepIndex = Math.Min(Math.Max(0, value), StepsCount - 1);
				ResetStep();
			}
		}

		public int StepsCount
		{
			get
			{
				return Timeline.Steps.Count;
			}
		}

		public ProcessStep CurrentStep
		{
			get
			{
				return Timeline.Steps[StepIndex];
			}
		}

		public TimeSpan StepDuration
		{
			get
			{
				if (!_isStarted) return TimeSpan.Zero;
				return Min(DateTime.Now - StepStart - StepTotalPausedTime, CurrentStep.Duration);
			}
		}

		public double StepProgress
		{
			get
			{
				if (CurrentStep.Duration.TotalMilliseconds == 0) return 0;
				return StepDuration.TotalMilliseconds / CurrentStep.Duration.TotalMilliseconds;
			}
		}

		public bool IsStepStarted
		{
			get { return _isStarted; }
		}

		public bool IsStepFinished
		{
			get
			{
				return _isStarted && CurrentStep.Duration == StepDuration;
			}
		}

		public bool IsCheckRequiered
		{
			get
			{
				if (CurrentStep.CheckInterval > TimeSpan.Zero)
				{
					_checkIntervals = (int)Math.Floor(StepDuration.TotalMilliseconds / CurrentStep.CheckInterval.TotalMilliseconds);
					return _checkIntervals > _checks;
				}
				return false;
			}
		}

		public bool IsPaused
		{
			get { return _isPaused; }
			set
			{
				if (_isPaused != value)
				{
					if (value)
					{
						_pauseStart = DateTime.Now;
					}
					else
					{
						_pausedTime += StepCurrentPausedTime;
					}
					_isPaused = value;
				}
			}
		}

		public TimeSpan StepTotalPausedTime
		{
			get
			{
				return _pausedTime + StepCurrentPausedTime;
			}
		}

		public TimeSpan StepCurrentPausedTime
		{
			get
			{
				return IsPaused ? (DateTime.Now - _pauseStart) : TimeSpan.Zero;
			}
		}

		public ProcessState(ProcessTimeline timeline)
			: this(timeline, string.Empty)
		{ }

		public ProcessState(ProcessTimeline timeline, string chamberTitle)
			: this(timeline, 0, DateTime.Now, false, false, false, false, chamberTitle, TimeSpan.Zero, TimeSpan.Zero, DateTime.Now)
		{ }

		public ProcessState(ProcessTimeline timeline, int stepIndex, DateTime stepStart, 
			bool isStepStarted, bool isPaused, bool isCheckRequired, bool isFinishedChecked, string chamberTitle, 
			TimeSpan stepTotalPausedTime, TimeSpan stepCurrentPausedTime, DateTime recordTime)
		{
			Timeline = timeline;
			StepIndex = stepIndex;
			StepStart = stepStart;
			ChamberTitle = chamberTitle;
			IsStepFinishedChecked = isFinishedChecked;
			_isStarted = isStepStarted;
			_isPaused = isPaused;
			_checks = isCheckRequired ? _checkIntervals - 1 : _checkIntervals;
			_pausedTime = stepTotalPausedTime - stepCurrentPausedTime;
			if (isPaused)
			{
				_pauseStart = recordTime - stepCurrentPausedTime;
			}
		}

		public void StartStep()
		{
			if (!_isStarted)
			{
				StepStart = DateTime.Now;
				_isStarted = true;
			}
		}

		public void ResetStep()
		{
			_pausedTime = TimeSpan.Zero;
			_isStarted = false;
			_isPaused = false;
			_checkIntervals = 0;
			_checks = 0;
			IsStepFinishedChecked = false;
		}

		public void Check()
		{
			_checks = _checkIntervals;
		}

		private static TimeSpan Min(TimeSpan a, TimeSpan b)
		{
			return a < b ? a : b;
		}
	}
}
