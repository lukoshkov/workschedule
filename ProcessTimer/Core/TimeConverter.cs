﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessTimer.Core
{
	public class TimeConverter
	{
		public enum Format
		{
			Dhms,
			Hms
		}

		public static string ToString(TimeSpan ts, Format format = Format.Hms)
		{
			switch (format)
			{
				case Format.Dhms:
					var f = ts.Days > 0 ? "{0} сут. {1:00}:{2:00}:{3:00}" : "{1:00}:{2:00}:{3:00}";
					return string.Format(f, ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
				case Format.Hms:
					var hours = (int)Math.Floor(ts.TotalHours);
					return string.Format("{0:00}:{1:00}:{2:00}", hours, ts.Minutes, ts.Seconds);
				default:
					return ts.ToString();
			}
		}
	}
}
