﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProcessTimer.Core.Storing
{
	public class StateStorage
	{
		public static readonly int Version = 1;

		public static string GetBackupPath(string path)
		{
			return path + ".bak";
		}

		public static void Store(List<ProcessState> states, string path)
		{
			if (states == null)
			{
				return;
			}

			if (File.Exists(path))
			{
				var backup = GetBackupPath(path);
				StorageHelper.MoveFile(path, backup, true);
			}
			using (var fs = new FileStream(path, FileMode.Create))
			using (var writer = new BinaryWriter(fs))
			{
				writer.Write(Version);
				writer.Write(DateTime.Now.Ticks);
				writer.Write(states.Count);
				foreach (var state in states)
				{
					writer.Write(state != null);
					if (state != null)
					{
						writer.Write(state.StepIndex);
						writer.Write(state.IsStepStarted);
						writer.Write(state.IsPaused);
						writer.Write(state.IsCheckRequiered);
						writer.Write(state.IsStepFinishedChecked);
						writer.Write(state.StepStart.Ticks);
						writer.Write(state.StepTotalPausedTime.Ticks);
						writer.Write(state.StepCurrentPausedTime.Ticks);
						StorageHelper.WriteGuid(writer, state.Timeline.Guid);
						StorageHelper.WriteString(writer, state.ChamberTitle);
					}
				}
			}
		}

		public static List<ProcessState> RetrieveBackup(string path, List<ProcessTimeline> timelines)
		{
			try
			{
				var backup = GetBackupPath(path);
				var states = Retrieve(backup, timelines);
				File.Copy(backup, path, true);
				return states;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static List<ProcessState> Retrieve(string path, List<ProcessTimeline> timelines)
		{
			var states = new List<ProcessState>();
			using (var fs = new FileStream(path, FileMode.Open))
			using (var reader = new BinaryReader(fs))
			{
				var version = reader.ReadInt32();
				var time = new DateTime(reader.ReadInt64());
				var count = reader.ReadInt32();
				for (int i = 0; i < count; i++)
				{
					ProcessState state = null;
					if (reader.ReadBoolean())
					{
						var index = reader.ReadInt32();
						var isStarted = reader.ReadBoolean();
						var isPaused = reader.ReadBoolean();
						var isCheckRequired = reader.ReadBoolean();
						var isFinishedChecked = reader.ReadBoolean();
						var start = new DateTime(reader.ReadInt64());
						var pausedTotal = new TimeSpan(reader.ReadInt64());
						var pausedCurrent = new TimeSpan(reader.ReadInt64());
						var guid = StorageHelper.ReadGuid(reader);
						var chamber = StorageHelper.ReadString(reader);

						var timeline = timelines.Find(t => t.Guid == guid);
						if (timeline != null)
						{
							state = new ProcessState(timeline, index, start, isStarted, isPaused, isCheckRequired, isFinishedChecked, chamber, pausedTotal, pausedCurrent, time);
						}
					}
					states.Add(state);
				}
			}
			return states;
		}
	}
}
