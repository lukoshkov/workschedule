﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProcessTimer.Core.Storing
{
	public static class ProductStorage
	{
		public static readonly int Version = 1;
		public static readonly string FileExtension = ".pdt";

		public static void Store(Product product, string directory)
		{
			if (product == null)
			{
				return;
			}
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			var path = GetPath(product, directory);
			if (File.Exists(path))
			{
				return;
			}
			WriteProduct(product, path);
		}

		public static void Update(Product product, string directory)
		{
			if (product == null)
			{
				return;
			}
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			var path = GetPath(product, directory);
			UpdateSafe(product, path);
		}

		public static List<Product> RetrieveAll(string directory)
		{
			if (!Directory.Exists(directory))
			{
				return null;
			}
			var products = new List<Product>();
			foreach (var path in Directory.GetFiles(directory))
			{
				if (Path.GetExtension(path).Equals(FileExtension))
				{
					products.Add(Retrieve(path));
				}
			}
			return products;
		}

		public static Product Retrieve(string path)
		{
			if (!File.Exists(path))
			{
				return null;
			}

			var version = Version;
			var product = new Product(Path.GetFileNameWithoutExtension(path));
			using (var fs = new FileStream(path, FileMode.Open))
			using (var reader = new BinaryReader(fs))
			{
				version = reader.ReadInt32();
				product.Guid = StorageHelper.ReadGuid(reader);
				product.Title = StorageHelper.ReadString(reader);
				var count = reader.ReadInt32();
				for (int i = 0; i < count; i++)
				{
					var guid = StorageHelper.ReadGuid(reader);
					var title = StorageHelper.ReadString(reader);
					var category = new TestCategory(title, guid);
					product.TestCategories.Add(category);
				}
			}
			if (version != Version)
			{
				UpdateSafe(product, path);
			}
			if (!path.Contains(product.Guid.ToString()))
			{
				var dir = Path.GetDirectoryName(path);
				Store(product, dir);
				File.Delete(path);
			}
			return product;
		}

		public static void Delete(Product product, string directory)
		{
			if (product == null)
			{
				return;
			}
			var path = GetPath(product, directory);
			if (File.Exists(path))
			{
				File.Delete(path);
			}
		}

		public static string GetPath(Product product, string directory)
		{
			return product != null ? GetPath(product.Guid.ToString(), directory) : null;
			//return product != null ? GetPath(product.Title, directory) : null;
		}

		public static string GetPath(string productTitle, string directory)
		{
			return productTitle != null
				? Path.Combine(directory, StorageHelper.ToValidFileName(productTitle) + FileExtension)
				: null;
		}

		private static void WriteProduct(Product product, string path)
		{
			using (var fs = new FileStream(path, FileMode.Create))
			using (var writer = new BinaryWriter(fs))
			{
				writer.Write(Version);
				StorageHelper.WriteGuid(writer, product.Guid);
				StorageHelper.WriteString(writer, product.Title);
				writer.Write(product.TestCategories.Count);
				foreach (var category in product.TestCategories)
				{
					StorageHelper.WriteGuid(writer, category.Guid);
					StorageHelper.WriteString(writer, category.Title);
				}
			}
		}

		private static void UpdateSafe(Product product, string path)
		{
			var tmp = path + ".tmp";
			WriteProduct(product, tmp);
			File.Copy(tmp, path, true);
			File.Delete(tmp);
		}
	}
}
