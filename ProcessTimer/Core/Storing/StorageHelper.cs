﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProcessTimer.Core.Storing
{
	public static class StorageHelper
	{
		public static readonly Encoding StringEncoding = Encoding.UTF8;
		public static readonly int GuidByteLength = 16;

		public static string ToValidFileName(string value)
		{
			var invalid = Path.GetInvalidFileNameChars();
			if (value.IndexOfAny(invalid) >= 0)
			{
				var str = new List<char>();
				foreach (var c in value)
				{
					var valid = true;
					foreach (var ic in invalid)
					{
						if (ic == c)
						{
							valid = false;
							break;
						}
					}
					if (valid)
					{
						str.Add(c);
					}
				}
				value = new string(str.ToArray());
			}
			return value;
		}

		public static bool MoveFile(string src, string dst, bool rewrite)
		{
			var exists = File.Exists(dst);
			if (rewrite && exists || !exists)
			{
				File.Delete(dst);
				File.Move(src, dst);
				return true;
			}
			return false;
		}

		public static void CreateDirectoryIfNotExists(string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		public static void WriteString(BinaryWriter writer, string value)
		{
			var bytes = StringEncoding.GetBytes(value);
			writer.Write(bytes.Length);
			writer.Write(bytes);
		}

		public static string ReadString(BinaryReader reader)
		{
			var length = reader.ReadInt32();
			return StringEncoding.GetString(reader.ReadBytes(length));
		}

		public static void WriteGuid(BinaryWriter writer, Guid value)
		{
			writer.Write(value.ToByteArray());
		}

		public static Guid ReadGuid(BinaryReader reader)
		{
			return new Guid(reader.ReadBytes(GuidByteLength));
		}
	}
}
