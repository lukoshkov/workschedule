﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ProcessTimer.Core.Storing
{
	public class TimelineStorage
	{
		public static readonly int Version = 3;
		public static readonly string FileExtension = ".ptl";

		public static void Store(ProcessTimeline timeline, string directory)
		{
			if (timeline == null)
			{
				return;
			}
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			var path = GetPath(timeline, directory);
			if (File.Exists(path))
			{
				return;
			}
			WriteTimeline(timeline, path);
		}

		public static void Update(ProcessTimeline timeline, string directory)
		{
			if (timeline == null)
			{
				return;
			}
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			var path = GetPath(timeline, directory);
			UpdateSafe(timeline, path);
		}

		public static List<ProcessTimeline> RetrieveAll(string directory, List<Product> products)
		{
			if (!Directory.Exists(directory))
			{
				return null;
			}
			var timelines = new List<ProcessTimeline>();
			foreach (var path in Directory.GetFiles(directory))
			{
				if (Path.GetExtension(path).Equals(FileExtension))
				{
					timelines.Add(Retrieve(path, products));
				}
			}
			return timelines;
		}

		public static ProcessTimeline Retrieve(string path, List<Product> products)
		{
			if (!File.Exists(path))
			{
				return null;
			}

			var version = Version;
			var timeline = new ProcessTimeline(Path.GetFileNameWithoutExtension(path));
			using (var fs = new FileStream(path, FileMode.Open))
			using (var reader = new BinaryReader(fs))
			{
				version = reader.ReadInt32();
				timeline.Guid = version > 1 ? StorageHelper.ReadGuid(reader) : Guid.NewGuid();
				var guidP = version > 2 ? StorageHelper.ReadGuid(reader) : Guid.Empty;
				var guidC = version > 2 ? StorageHelper.ReadGuid(reader) : Guid.Empty;
				var product = products.Find(p => p.Guid == guidP);
				timeline.Product = product;
				timeline.Category = product == null ? null : product.TestCategories.Find(c => c.Guid == guidC);
				timeline.Title = StorageHelper.ReadString(reader);
				var count = reader.ReadInt32();
				for (int i = 0; i < count; i++)
				{
					var duration = new TimeSpan(reader.ReadInt64());
					var interval = new TimeSpan(reader.ReadInt64());
					var title = StorageHelper.ReadString(reader);
					var description = StorageHelper.ReadString(reader);
					var step = new ProcessStep(duration, title, description);
					timeline.Steps.Add(step);
				}
			}
			if (version != Version)
			{
				UpdateSafe(timeline, path);
			}
			if (!path.Contains(timeline.Guid.ToString()))
			{
				var dir = Path.GetDirectoryName(path);
				Store(timeline, dir);
				File.Delete(path);
			}
			return timeline;
		}

		public static void Delete(ProcessTimeline timeline, string directory)
		{
			if (timeline == null)
			{
				return;
			}
			var path = GetPath(timeline, directory);
			if (File.Exists(path))
			{
				File.Delete(path);
			}
		}

		public static string GetPath(ProcessTimeline timeline, string directory)
		{
			return timeline != null ? GetPath(timeline.Guid.ToString(), directory) : null;
			//return timeline != null ? GetPath(timeline.Title, directory) : null;
		}

		public static string GetPath(string timelineTitle, string directory)
		{
			return timelineTitle != null
				? Path.Combine(directory, StorageHelper.ToValidFileName(timelineTitle) + FileExtension)
				: null;
		}

		private static void WriteTimeline(ProcessTimeline timeline, string path)
		{
			using (var fs = new FileStream(path, FileMode.Create))
			using (var writer = new BinaryWriter(fs))
			{
				writer.Write(Version);
				StorageHelper.WriteGuid(writer, timeline.Guid);
				StorageHelper.WriteGuid(writer, timeline.Product == null ? Guid.Empty : timeline.Product.Guid);
				StorageHelper.WriteGuid(writer, timeline.Category == null ? Guid.Empty : timeline.Category.Guid);
				StorageHelper.WriteString(writer, timeline.Title);
				writer.Write(timeline.Steps.Count);
				foreach (var step in timeline.Steps)
				{
					writer.Write(step.Duration.Ticks);
					writer.Write(step.CheckInterval.Ticks);
					StorageHelper.WriteString(writer, step.Title);
					StorageHelper.WriteString(writer, step.Description);
				}
			}
		}

		private static void UpdateSafe(ProcessTimeline timeline, string path)
		{
			var tmp = path + ".tmp";
			WriteTimeline(timeline, tmp);
			File.Copy(tmp, path, true);
			File.Delete(tmp);
		}
	}
}
