﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProcessTimer.Core.Storing
{
	public class TimelineCsvParser
	{
		public static readonly Encoding ExcelCsvEncoding = Encoding.GetEncoding(1251);

		public static bool TryParse(string path, out ProcessTimeline timeline)
		{
			timeline = null;
			if (File.Exists(path))
			{
				var fields = GetCsvFields(path);
				TryParse(fields, out timeline);
			}
			return timeline != null;
		}

		public static List<string[]> GetCsvFields(string path)
		{
			var lines = new List<string[]>();
			using (var parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(path, ExcelCsvEncoding, true))
			{
				parser.SetDelimiters(";");
				parser.HasFieldsEnclosedInQuotes = true;
				parser.TrimWhiteSpace = true;
				while (!parser.EndOfData)
				{
					lines.Add(parser.ReadFields());
				}
			}
			return lines;
		}

		protected static bool TryParse(List<string[]> lines, out ProcessTimeline timeline)
		{
			var steps = new List<ProcessStep>();
			ProcessStep step;
			foreach (var fields in lines)
			{
				if (TryParse(fields, out step))
				{
					steps.Add(step);
				}
			}
			timeline = steps.Count > 0 ? new ProcessTimeline("") : null;
			if (timeline != null) timeline.Steps.AddRange(steps);
			return timeline != null;
		}

		protected static bool TryParse(string[] fields, out ProcessStep step)
		{
			step = new ProcessStep();
			if (fields.Length > 1)
			{
				TimeSpan duration;
				if (TryParse(fields[1], out duration))
				{
					step = new ProcessStep(duration, "", fields[0]);
					return true;
				}
			}
			return false;
		}

		protected static bool TryParse(string text, out TimeSpan value)
		{
			int h = 0;
			int min = 0;
			var spaces = new char[] { ' ', (char)160 };
			var parts = text.Split(spaces, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < parts.Length; i++)
			{
				parts[i] = parts[i].Trim("().,".ToCharArray());
			}
			for (int i = 1; i < parts.Length; i++)
			{
				if (parts[i] == "ч")
				{
					if (int.TryParse(parts[i - 1], out h))
					{
					}
				}
				if (parts[i] == "мин")
				{
					if (int.TryParse(parts[i - 1], out min))
					{
					}
				}
			}
			value = new TimeSpan(h, min, 0);
			return true;
		}
	}
}
