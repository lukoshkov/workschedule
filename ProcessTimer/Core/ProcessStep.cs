﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessTimer.Core
{
	public class ProcessStep
	{
		public TimeSpan Duration { get; set; }
		public TimeSpan CheckInterval { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }

		public ProcessStep()
		{

		}

		public ProcessStep(TimeSpan duration, string title, string description)
			: this(duration, title, description, TimeSpan.Zero)
		{ }

		public ProcessStep(TimeSpan duration, string title, string description, TimeSpan checkInterval)
		{
			Duration = duration;
			Title = title;
			Description = description;
			CheckInterval = checkInterval;
		}

		public override string ToString()
		{
			return string.Format("{0} - {1} ({2})", TimeConverter.ToString(Duration), Title, Description);
		}
	}
}
