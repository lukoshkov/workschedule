﻿using System;

namespace ProcessTimer.Core
{
	public class TestCategory
	{
		public Guid Guid { get; set; }
		public string Title { get; set; }

		public TestCategory(string title)
			: this(title, Guid.NewGuid())
		{
		}

		public TestCategory(string title, Guid guid)
		{
			Guid = guid;
			Title = title;
		}
	}
}
