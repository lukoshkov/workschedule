﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessTimer.Core
{
	public class ProcessTimeline
	{
		public Guid Guid { get; set; }
		public string Title { get; set; }
		public Product Product { get; set; }
		public TestCategory Category { get; set; }
		public List<ProcessStep> Steps { get; private set; }

		public ProcessTimeline(string title)
			: this(title, Guid.NewGuid())
		{
		}

		public ProcessTimeline(string title, Guid guid)
		{
			Title = title;
			Guid = guid;
			Steps = new List<ProcessStep>();
		}

		public override string ToString()
		{
			return Title;
		}
	}
}
