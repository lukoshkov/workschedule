﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace ProcessTimer
{
	static class Program
	{
		static Mutex mutex = new Mutex(true, "{9B163F8A-EC66-4044-8FE5-16A3C1049EF3}");
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			if (mutex.WaitOne(TimeSpan.Zero, true))
			{
				try
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
					Application.Run(new Form1());
				}
				catch (Exception ex)
				{
					MessageBox.Show(string.Format("{0}\n{1}", ex.Message, ex.StackTrace), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				finally
				{
					mutex.ReleaseMutex();
				}
			}
			else
			{
				MessageBox.Show("Приложение уже запущено");
			}
		}
	}
}
