﻿namespace ProcessTimer.Logging
{
	public class LogLine
	{
		public string Format;
		public object[] Args;

		public LogLine(string format, params object[] args)
		{
			Format = format;
			Args = args;
		}

		public override string ToString()
		{
			return string.Format(Format, Args);
		}
	}
}
