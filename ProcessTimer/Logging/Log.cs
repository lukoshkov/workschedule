﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProcessTimer.Logging
{
	public class Log : IDisposable
	{
		public static readonly Encoding DefaultEncoding = Encoding.UTF8;

		private bool _isAsync;
		private string _path;
		private Stack<LogLine> _lines;
		private System.Threading.Thread _worker;

		public Encoding Encoding { get; set; }

		public bool IsWriteAsync
		{
			get { return _isAsync; }
			set
			{
				if (_isAsync != value)
				{
					_isAsync = value;
					if (_isAsync == true)
					{
						_worker = new System.Threading.Thread(WorkerLoop);
						_worker.Start();
					}
				}
			}
		}

		public string FilePath
		{
			get { return _path; }
			set
			{
				_path = value;
				var dir = Path.GetDirectoryName(value);
				if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
				{
					Directory.CreateDirectory(dir);
				}
			}
		}

		public Log(string filepath)
			: this(filepath, DefaultEncoding)
		{ }

		public Log(string filepath, Encoding encoding)
		{
			FilePath = filepath;
			Encoding = encoding;
			_lines = new Stack<LogLine>();
		}

		public void WriteLine(string format, params object[] args)
		{
			var encoding = Encoding == null ? DefaultEncoding : Encoding;
			using (var writer = new StreamWriter(FilePath, true, encoding))
			{
				writer.WriteLine(format, args);
			}
		}

		public void WriteLines(params LogLine[] lines)
		{
			if (lines == null) return;
			var encoding = Encoding == null ? DefaultEncoding : Encoding;
			using (var writer = new StreamWriter(FilePath, true, encoding))
			{
				foreach (var line in lines)
				{
					writer.WriteLine(line.Format, line.Args);
				}
			}
		}

		public void AddLine(string format, params object[] args)
		{
			var line = new LogLine(format, args);
			lock (_lines)
			{
				_lines.Push(line);
			}
		}

		public void AddLines(params LogLine[] lines)
		{
			lock (_lines)
			{
				foreach (var line in lines)
				{
					_lines.Push(line);
				}
			}
		}

		public bool TryFlush()
		{
			try
			{
				Flush();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public void Flush()
		{
			if (_lines.Count > 0)
			{
				LogLine[] lines = null;
				lock (_lines)
				{
					lines = new LogLine[_lines.Count];
					for (int i = lines.Length - 1; i > -1; i--)
					{
						lines[i] = _lines.Pop();
					}
				}
				WriteLines(lines);
			}
		}

		private void WorkerLoop()
		{
			while (_isAsync)
			{
				Flush();
				//System.Threading.Thread.Sleep(10000);
			}
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
					_isAsync = false;
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~Log() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
