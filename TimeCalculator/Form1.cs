﻿using System;
using System.Text;
using System.Windows.Forms;

namespace TimeCalculator
{
	public partial class Form1 : Form
	{
		private static readonly string DateTimeFormat = "HH:mm   ddd, dd  MMMM  yyyy г.";

		public Form1()
		{
			InitializeComponent();
		}

		private void numericLengthHours_ValueChanged(object sender, EventArgs e)
		{
			UpdateProcessLength();
			UpdateStartAndEnd();
		}

		private void dtpLengthMinutes_ValueChanged(object sender, EventArgs e)
		{
			UpdateProcessLength();
			UpdateStartAndEnd();
		}

		private void dtpStartTime_ValueChanged(object sender, EventArgs e)
		{
			UpdateStartAndEnd();
		}

		private void dtpStartDate_ValueChanged(object sender, EventArgs e)
		{
			UpdateStartAndEnd();
		}

		private void dtpEndTime_ValueChanged(object sender, EventArgs e)
		{
			UpdateStartAndEnd();
		}

		private void dtpEndDate_ValueChanged(object sender, EventArgs e)
		{
			UpdateStartAndEnd();
		}

		private void UpdateStartAndEnd()
		{
			var length = GetProcessLength();
			var start = GetDateTime(dtpStartDate, dtpStartTime);
			var end = GetDateTime(dtpEndDate, dtpEndTime);
			var calculatedEnd = start.Add(length);
			var calculatedStart = end.Subtract(length);
			textBoxEnd.Text = calculatedEnd.ToString(DateTimeFormat);
			textBoxStart.Text = calculatedStart.ToString(DateTimeFormat);
		}

		private void UpdateProcessLength()
		{
			var length = GetProcessLength();
			var sb = new StringBuilder();
			if (length.TotalDays >= 1)
			{
				sb.AppendFormat("{0} сут. ", (int)length.TotalDays);
			}
			sb.AppendFormat("{0} ч. {1} м.", length.Hours, length.Minutes);
			textBoxLength.Text = sb.ToString();
		}

		private TimeSpan GetProcessLength()
		{
			return new TimeSpan((int)numericLengthHours.Value, dtpLengthMinutes.Value.Minute, 0);
		}

		private DateTime GetDateTime(DateTimePicker date, DateTimePicker time)
		{
			return new DateTime(date.Value.Year, date.Value.Month, date.Value.Day, time.Value.Hour, time.Value.Minute, 0);
		}
	}
}
