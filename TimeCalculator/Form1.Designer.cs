﻿namespace TimeCalculator
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
			this.numericLengthHours = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.dtpLengthMinutes = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
			this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
			this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxEnd = new System.Windows.Forms.TextBox();
			this.textBoxStart = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.textBoxLength = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.numericLengthHours)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(37, 33);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(131, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Длительность процесса";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(37, 79);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(95, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Начало процесса";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(36, 169);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(113, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Окончание процесса";
			// 
			// dtpStartTime
			// 
			this.dtpStartTime.CustomFormat = "HH:mm";
			this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpStartTime.Location = new System.Drawing.Point(174, 77);
			this.dtpStartTime.Name = "dtpStartTime";
			this.dtpStartTime.ShowUpDown = true;
			this.dtpStartTime.Size = new System.Drawing.Size(52, 20);
			this.dtpStartTime.TabIndex = 3;
			this.dtpStartTime.ValueChanged += new System.EventHandler(this.dtpStartTime_ValueChanged);
			// 
			// numericLengthHours
			// 
			this.numericLengthHours.Location = new System.Drawing.Point(174, 31);
			this.numericLengthHours.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.numericLengthHours.Name = "numericLengthHours";
			this.numericLengthHours.Size = new System.Drawing.Size(52, 20);
			this.numericLengthHours.TabIndex = 1;
			this.numericLengthHours.ValueChanged += new System.EventHandler(this.numericLengthHours_ValueChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(229, 33);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(15, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "ч.";
			// 
			// dtpLengthMinutes
			// 
			this.dtpLengthMinutes.CustomFormat = "mm";
			this.dtpLengthMinutes.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpLengthMinutes.Location = new System.Drawing.Point(250, 31);
			this.dtpLengthMinutes.Name = "dtpLengthMinutes";
			this.dtpLengthMinutes.ShowUpDown = true;
			this.dtpLengthMinutes.Size = new System.Drawing.Size(36, 20);
			this.dtpLengthMinutes.TabIndex = 2;
			this.dtpLengthMinutes.Value = new System.DateTime(2019, 11, 10, 0, 0, 0, 0);
			this.dtpLengthMinutes.ValueChanged += new System.EventHandler(this.dtpLengthMinutes_ValueChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(290, 33);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(18, 13);
			this.label5.TabIndex = 7;
			this.label5.Text = "м.";
			// 
			// dtpStartDate
			// 
			this.dtpStartDate.CustomFormat = "ddd,  dd  MMMM  yyyy г.";
			this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpStartDate.Location = new System.Drawing.Point(250, 77);
			this.dtpStartDate.Name = "dtpStartDate";
			this.dtpStartDate.ShowUpDown = true;
			this.dtpStartDate.Size = new System.Drawing.Size(159, 20);
			this.dtpStartDate.TabIndex = 4;
			this.dtpStartDate.ValueChanged += new System.EventHandler(this.dtpStartDate_ValueChanged);
			// 
			// dtpEndDate
			// 
			this.dtpEndDate.CustomFormat = "ddd,  dd  MMMM  yyyy г.";
			this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpEndDate.Location = new System.Drawing.Point(250, 166);
			this.dtpEndDate.Name = "dtpEndDate";
			this.dtpEndDate.ShowUpDown = true;
			this.dtpEndDate.Size = new System.Drawing.Size(159, 20);
			this.dtpEndDate.TabIndex = 6;
			this.dtpEndDate.ValueChanged += new System.EventHandler(this.dtpEndDate_ValueChanged);
			// 
			// dtpEndTime
			// 
			this.dtpEndTime.CustomFormat = "HH:mm";
			this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpEndTime.Location = new System.Drawing.Point(174, 166);
			this.dtpEndTime.Name = "dtpEndTime";
			this.dtpEndTime.ShowUpDown = true;
			this.dtpEndTime.Size = new System.Drawing.Size(52, 20);
			this.dtpEndTime.TabIndex = 5;
			this.dtpEndTime.ValueChanged += new System.EventHandler(this.dtpEndTime_ValueChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(37, 106);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(113, 13);
			this.label6.TabIndex = 8;
			this.label6.Text = "Окончание процесса";
			// 
			// textBoxEnd
			// 
			this.textBoxEnd.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxEnd.Location = new System.Drawing.Point(174, 106);
			this.textBoxEnd.Name = "textBoxEnd";
			this.textBoxEnd.ReadOnly = true;
			this.textBoxEnd.Size = new System.Drawing.Size(235, 20);
			this.textBoxEnd.TabIndex = 10;
			this.textBoxEnd.TabStop = false;
			// 
			// textBoxStart
			// 
			this.textBoxStart.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxStart.Location = new System.Drawing.Point(174, 194);
			this.textBoxStart.Name = "textBoxStart";
			this.textBoxStart.ReadOnly = true;
			this.textBoxStart.Size = new System.Drawing.Size(235, 20);
			this.textBoxStart.TabIndex = 12;
			this.textBoxStart.TabStop = false;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(36, 194);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(95, 13);
			this.label7.TabIndex = 11;
			this.label7.Text = "Начало процесса";
			// 
			// textBoxLength
			// 
			this.textBoxLength.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxLength.Location = new System.Drawing.Point(314, 30);
			this.textBoxLength.Name = "textBoxLength";
			this.textBoxLength.ReadOnly = true;
			this.textBoxLength.Size = new System.Drawing.Size(95, 20);
			this.textBoxLength.TabIndex = 13;
			this.textBoxLength.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(456, 246);
			this.Controls.Add(this.textBoxLength);
			this.Controls.Add(this.textBoxStart);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.textBoxEnd);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.dtpEndDate);
			this.Controls.Add(this.dtpEndTime);
			this.Controls.Add(this.dtpStartDate);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.dtpLengthMinutes);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.numericLengthHours);
			this.Controls.Add(this.dtpStartTime);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Калькулятор";
			((System.ComponentModel.ISupportInitialize)(this.numericLengthHours)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DateTimePicker dtpStartTime;
		private System.Windows.Forms.NumericUpDown numericLengthHours;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.DateTimePicker dtpLengthMinutes;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.DateTimePicker dtpStartDate;
		private System.Windows.Forms.DateTimePicker dtpEndDate;
		private System.Windows.Forms.DateTimePicker dtpEndTime;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBoxEnd;
		private System.Windows.Forms.TextBox textBoxStart;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxLength;
	}
}

